USE [SPS]
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE IF EXISTS  [dbo].[MetadataGet] 
GO


-- =============================================
-- Author:		Audrey
-- Create date: 06/02/2018
-- Description:	Add the meta data
-- =============================================
CREATE PROCEDURE [dbo].[MetadataGet] 
		@DataStrategyId INT,
		@Result INT OUTPUT,
		@ResultMessage NVARCHAR(100) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SET @Result = -1


	SET @Result = ISNULL((SELECT TOP 1 VersionId FROM [dbo].[Metadata] WHERE DataStrategyId = @DataStrategyId ORDER BY EntryUpdateDate DESC), -1)

	IF (@Result = -1)
	BEGIN
		SET @ResultMessage = ' Data Strategy Id: ' + CAST(@DataStrategyId AS VARCHAR(20)) + ' does not appear to exist'
	END


END
GO

GRANT EXECUTE ON [dbo].[MetadataGet] TO etluser 

