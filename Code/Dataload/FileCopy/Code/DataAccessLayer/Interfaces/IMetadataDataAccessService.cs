﻿using Models.Models;

namespace DataAccessLayer.Interfaces
{
    public interface IMetadataDataAccessService
    {
        ExecuteResult GetMetaData(Metadata metdata, string schemeType);
       
    }
}
