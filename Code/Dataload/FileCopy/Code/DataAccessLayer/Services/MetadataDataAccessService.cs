﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Interfaces;
using Models.Models;

namespace DataAccessLayer.Services
{
    public class MetadataDataAccessService : IMetadataDataAccessService
    {
        public ExecuteResult GetMetaData(Metadata metdata, string schemeType)
        {
            var execResult = new ExecuteResult();

            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[MetadataGet]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@DataStrategyId", SqlDbType.Int).Value = metdata.DataStrategyId;

                    var result = command.Parameters.Add("@Result", SqlDbType.Int);
                    result.Direction = ParameterDirection.Output;

                    var resultMessage = command.Parameters.Add("@ResultMessage", SqlDbType.VarChar, 100);
                    resultMessage.Direction = ParameterDirection.Output;

                    con.Open();
                    command.ExecuteNonQuery();

                    execResult.VersionId = (int)(result.Value ?? -1);
                    execResult.Message = Convert.IsDBNull(resultMessage.Value) ? string.Empty  : (string)resultMessage.Value ;
                    execResult.Success = execResult.VersionId != -1;
                }
            }

            return execResult;
        }

     
    }
}
