﻿using System;
using Services.Interfaces;
using Services.Services;

namespace SpsFileCopy
{
    public class Program
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args)
        {
            try
            {
                Log.Info("Start");

                IFileCopyService importService = new FileCopyService(Log);
                importService.StartFileCopy();

                Log.Info("Finished");
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal Error Occured", ex);

                var mailerService = new MailerService();
                mailerService.SendErrorEmail(ex);
            }
        }
    }
}
