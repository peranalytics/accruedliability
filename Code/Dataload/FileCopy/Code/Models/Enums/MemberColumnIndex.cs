﻿namespace Models.Enums
{
    public enum MemberColumnIndex
    {
        PayrollNumber = 19,
        AdditionalPayrollNumber = 20,
        PrsiClass = 21,
        PensionableEmploymentStartDate = 22,
        PensionableEmploymentLeaveDate = 23,
        MemberStatus = 24,
        NormalRetirementDate = 25,
        MembershipCategory = 26,
        EmployeeContributionsPaid = 27,
        TotalActualGrossPensionableRemunerationPaid = 28,
        PensionReferableAmount = 29,
        LumpSumReferableAmount = 30,
        GrossRefundAmountPaid = 31,
        RefundDate = 32,
        RefundMembershipCategory = 33,
        RefundPensionableEmploymentStartDate = 34,
        RefundPensionableEmploymentLeaveDate = 35,
        RefundPensionReferableAmount = 36,
        RefundLumpsumReferableAmount = 37,
        RrOriginalRaNumber = 38,
        RrOriginalRaName = 39,
        RrFinalDateOfPayment = 40,
        RrPensionableEmploymentStartDate = 41,
        RrPensionableEmploymentLeaveDate = 42,
        RrGrossContributeAmount = 43,
        RrCompoundInterestmount = 44,
        RrMembershipCategory = 45,
        RrPensionReferableAmount = 46,
        RrLumpsumReferableAmount = 47
    }
}
