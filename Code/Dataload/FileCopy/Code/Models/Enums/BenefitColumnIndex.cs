﻿namespace Models.Enums
{
    public enum BenefitColumnIndex
    {
        PrsiClass = 19,
        BeneficiaryPpsn = 20,
        BeneficiaryDateofBirth = 21,
        BeneficiaryFirstName = 22,
        BeneficiarySurname = 23,
        BeneficiaryMaidenName = 24,
        BeneficiarySalutationTitle = 25,
        BeneficiaryGender = 26,
        BenefitType = 27,
        BenefitEffectiveDate = 28,
        PensionPaymentStartDate = 29,
        PensionPaymentEndDate = 30,
        GrossAnnualPensionValue = 31,
        GrossRetirementLumpSum = 32,
        ShortServiceGratuity = 33,
        GrossDeathGratuity = 34,
        GratuityPaymentDate = 35
    }
}
