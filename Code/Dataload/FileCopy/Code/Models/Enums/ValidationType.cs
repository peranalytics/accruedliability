﻿using System.ComponentModel;

namespace Models.Enums
{
    public enum ValidationType
    {
        [Description("Validation Successful")]
        Success = 0,
        [Description("The expected number of records did not match the number of records in the file")]
        RecordCountMismatch = 1,
        [Description("The expected year of records did not match the year property in the file")]
        YearMismatch = 2,
        [Description("The files are converted into an empty dataset")]
        DataSetEmpty = 3,
        [Description("The file contains data for more than one year")]
        MultipleYearsInFile = 4,
        [Description("Invalid SPS Year")]
        InvalidSpsYear = 5,
        [Description("Invalid data file type")]
        InvalidDataFileType = 6,
        [Description("Invalid member data in the file")]
        InvalidMemberData = 7,
        [Description("Invalid benefit data in the file")]
        InvalidBenefitData = 8
    }
}
