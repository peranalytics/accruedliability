﻿namespace Models.Enums
{
    public enum SchemeType
    {
        Members = 1,
        Benefits = 2
    }
}
