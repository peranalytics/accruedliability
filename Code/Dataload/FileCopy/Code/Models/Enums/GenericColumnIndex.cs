﻿namespace Models.Enums
{
    public enum GenericColumnIndex
    {
        Year = 0,
        Ppsn = 1,
        DateofBirth = 2,
        FirstName = 3,
        Surname = 4,
        MaidenName = 5,
        SalutationTitle = 6,
        Gender = 7,
        CivilStatus = 8,
        Pao = 9,
        DateofDeath = 10,
        HomeAddress1 = 11,
        HomeAddress2 = 12,
        HomeAddress3 = 13,
        HomeAddress4 = 14,
        HomeAddress5 = 15,
        Eircode = 16,
        RaNumber = 17,
        RaName = 18
    }
}
