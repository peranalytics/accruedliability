﻿namespace Models.Enums
{
    public enum FileType
    {
        Excel = 1,
        Csv = 2
    }
}
