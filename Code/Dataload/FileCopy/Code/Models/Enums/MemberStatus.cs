﻿using System.ComponentModel;

namespace Models.Enums
{
    public enum MemberStatus
    {
        [Description("Active Member")]
        ActiveMember = 0,
        [Description("Leaver - not vested and contributions refunded")]
        LeaverNotVestedAndContributionsRefunded = 1,
        [Description("Leaver - not vested and contributions not refunded")]
        LeaverNotVestedAndContributionsNotRefunded = 2,
        [Description("Leaver - deferred benefit (vested)")]
        LeaverDeferredBenefitVested =  3,
        [Description("Leaver - unknown")]
        LeaverUnknown = 4,
        [Description("Suspended Member")]
        SuspendedMember = 5,
        [Description("Retirement - Normal")]
        RetirementNormal = 6,
        [Description("Retirement - Ill Health")]
        RetirementIllHealth = 7,
        [Description("Retirement - Cost Neutral")]
        RetirementCostNeutral = 8,
        [Description("Death in Service")]
        DeathinService = 9,
        [Description("Death of Deferred Member")]
        DeathofDeferredMember = 10,
    }
}
