﻿using Newtonsoft.Json;

namespace Models.Models
{
    public class Metadata
    {
        [JsonProperty(PropertyName = "id")]
        public int DataStrategyId { get; set; }

        public int VersionId { get; set; }

        [JsonProperty(PropertyName = "fields")]
        public MetadataDetails Details { get; set; }

        public string MetadataFileName { get; set; }
    }
}
