﻿using Models.Enums;

namespace Models.Models
{
    public class ValidationResult
    {
        public ValidationType ValidationType { get; set; }

        public Metadata Metadata { get; set; }
    }
}
