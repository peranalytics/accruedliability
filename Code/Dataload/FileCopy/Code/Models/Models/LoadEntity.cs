﻿using System.Data;

namespace Models.Models
{
    public class LoadEntity
    {
        public Metadata Metadata { get; set; }

        public DataSet FileData { get; set; }
    }
}
