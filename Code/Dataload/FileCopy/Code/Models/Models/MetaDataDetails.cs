﻿using System;
using Models.Enums;
using Newtonsoft.Json;

namespace Models.Models
{
    public class MetadataDetails
    {
        [JsonProperty(PropertyName = "psb_number")]
        public int RelevantAuthority { get; set; }

        [JsonProperty(PropertyName = "file_name")]
        public string FileName { get; set; }

        public int Year { get; set; }

        [JsonProperty(PropertyName = "uploaded_by")]
        public string UploadedBy { get; set; }

        public TransactionType TransactionType => TransactionType.Insert;

        [JsonProperty(PropertyName = "type")]
        public FileType FileType { get; set; }

        [JsonProperty(PropertyName = "num_records")]
        public int RecordCount { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public bool Validated { get; set; }
    }
}
