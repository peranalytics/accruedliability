﻿using System;
using System.Data;
using Models.Enums;
using Models.Models;
using Services.Interfaces;
using Services.Services;

namespace Services.Abstract
{
    public abstract class FileProcessingService : IFileProcessingService
    {

        public ValidationResult ProcessFile(Metadata metadata, SchemeType schemeType, string dataPath, log4net.ILog log)
        {
            log.Info("ProcessFile - File Processing Service");
            var fileData = GetFileAsDataSet(dataPath);
            log.Info("ProcessFile - GetFileAsDataSet Succeeeded");

            if (fileData == null || fileData.Tables.Count == 0 || fileData.Tables[0].Rows.Count == 0)
            {
                log.Info($"ProcessFile -{schemeType.ToString()} - Data Empty - All data for relevant authority Id:  {metadata.Details.RelevantAuthority} and Year: {metadata.Details.Year} will be deleted");

                return new ValidationResult
                {
                    ValidationType = ValidationType.Success,
                    Metadata = metadata
                };
            }

            var validationService = new ValidationService(metadata, fileData);
            var valResult = validationService.Validate();
            log.Info("ProcessFile - Validation Complete");

            return valResult;
        }

        public virtual DataSet GetFileAsDataSet(string path)
        {
            throw new NotImplementedException();
        }
    }
}
