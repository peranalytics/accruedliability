﻿using System;

namespace Services.Interfaces
{
    public interface IMailerService
    {
        void SendSuccessEmail();

        void SendErrorEmail(Exception exception);
    }
}
