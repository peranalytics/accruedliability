﻿using System.Collections.Generic;
using Models.Enums;
using Models.Models;

namespace Services.Interfaces
{
    public interface IMetadataService
    {
        IEnumerable<Metadata> ReadMetaFiles(string schemePath, SchemeType schemeType);
    }
}
