﻿using System.Data;
using System.IO;
using System.Linq;
using Services.Abstract;

namespace Services.Services
{
    public class CsvFileProcessingService : FileProcessingService
    {
        public override DataSet GetFileAsDataSet(string filePath)
        {
            var ds = new DataSet();
            var dt = new DataTable();
            ds.Tables.Add(dt);

            var allLines = File.ReadAllLines(filePath);
            var header = allLines.First();
            var cols = header.Split(',');

            for (var i = 0; i < cols.Length; i++)
            {
                var col = cols[i];
                var colName = dt.Columns.Contains(col) ? $"{col}_{i}" : col;
                dt.Columns.Add(colName);
            }

            foreach (var line in allLines.Skip(1))
            {
                var items = line.Split(',');
                var newRow = dt.NewRow();

                for (var i = 0; i < cols.Length; i++)
                {
                    newRow[i] = items[i];
                }

                dt.Rows.Add(newRow);
            }

            return ds;
        }
    }
}
