﻿using System;
using System.Data;
using System.Linq;
using Models.Enums;
using Models.Models;
using Services.Interfaces;

namespace Services.Services
{
    public class ValidationService : IValidationService
    {
        private readonly Metadata _meta;
        private readonly DataSet _ds;

        public ValidationService(Metadata meta, DataSet ds)
        {
            _meta = meta;
            _ds = ds;
        }

        public ValidationResult Validate()
        {
            var result = MetadataValidation();

            if (result.ValidationType != ValidationType.Success)
            {
                return result;
            }

            result = YearValidation();

            if (result.ValidationType != ValidationType.Success)
            {
                return result;
            }

            return result;
        }

        public static ValidationResult ReturnSuccess(Metadata meta)
        {
            return new ValidationResult
            {
                ValidationType = ValidationType.Success,
                Metadata = meta
            };
        }

        private ValidationResult MetadataValidation()
        {
            var dt = _ds.Tables[0];

            if (_meta.Details.RecordCount != dt.Rows.Count)
            {
                return new ValidationResult
                {
                    ValidationType = ValidationType.RecordCountMismatch,
                    Metadata = _meta
                };
            }

            return ReturnSuccess(_meta);
        }

        private ValidationResult YearValidation()
        {
            var dt = _ds.Tables[0];
            var distinctYears = dt.AsEnumerable().Select(row => row[(int)GenericColumnIndex.Year]).Distinct().ToList();

            if (distinctYears.Count > 1)
            {
                return new ValidationResult
                {
                    ValidationType = ValidationType.MultipleYearsInFile,
                    Metadata = _meta
                };
            }

            var fileYear = Convert.ToInt32(distinctYears.First());

            if (fileYear != _meta.Details.Year)
            {
                return new ValidationResult
                {
                    ValidationType = ValidationType.YearMismatch,
                    Metadata = _meta
                };
            }

            if (fileYear < (int)ValidationStatics.MinYear)
            {
                return new ValidationResult
                {
                    ValidationType = ValidationType.InvalidSpsYear,
                    Metadata = _meta
                };
            }

            return ReturnSuccess(_meta);
        }
    }
}
