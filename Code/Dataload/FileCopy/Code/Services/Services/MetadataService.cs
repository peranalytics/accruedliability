﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Services;
using Models.Enums;
using Models.Models;
using Newtonsoft.Json;
using Services.Extensions;
using Services.Interfaces;

namespace Services.Services
{
    public class MetadataService : IMetadataService
    {
        private readonly IMetadataDataAccessService _metadataDataAccess;
        private readonly log4net.ILog _log;

        public MetadataService(log4net.ILog log)
        {
            _metadataDataAccess = new MetadataDataAccessService();
            _log = log;
        }

        public IEnumerable<Metadata> ReadMetaFiles(string schemePath, SchemeType schemeType)
        {
            _log.Info($"SchemePath: {schemePath}");

            var info = new DirectoryInfo(schemePath);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            if (!files.Any())
            {
                return new List<Metadata>();
            }

            var results = (from file in files where file.Extension == ".json" select ProcessMetaFile(file, schemeType)).ToList();
            return results;
        }

        private Metadata ProcessMetaFile(FileSystemInfo file, SchemeType schemeType)
        {
            var stream = File.ReadAllText(file.FullName);

            var result = JsonConvert.DeserializeObject<Metadata>(stream);
            var resultDetails = JsonConvert.DeserializeObject<MetadataDetails>(stream);

            if (result.Details == null)
            {
                result.Details = resultDetails;
            }

            result.MetadataFileName = file.FullName;

            var execResult = _metadataDataAccess.GetMetaData(result, schemeType.GetDescription());
            _log.Info($"ProcessMetaFile: {execResult.Success} - {execResult.VersionId} - {execResult.Message}");

            result.VersionId = execResult.VersionId;
            return result;
        }
    }
}
