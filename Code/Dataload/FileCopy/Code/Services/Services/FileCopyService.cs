﻿using System;
using System.IO;
using System.Linq;
using Models.Enums;
using Models.Models;
using Services.Extensions;
using Services.Interfaces;

namespace Services.Services
{
    public class FileCopyService : IFileCopyService
    {
        private readonly log4net.ILog _log;
        private readonly IMetadataService _metadataService;
        private IFileProcessingService _fileProcessingService;
        private readonly IMailerService _mailerService;

        private string _fileFolder;

        public FileCopyService(log4net.ILog log)
        {
            _metadataService = new MetadataService(log);
            _mailerService = new MailerService();
            _log = log;
        }

        public void StartFileCopy()
        {
            ReadMetaData();
            _log.Info("Finish");

            if (GlobalSettings.SendEmail)
            {
                _mailerService.SendSuccessEmail();
            }

            ArchiveLogFiles();
        }

        public void ReadMetaData()
        {
            _fileFolder = GlobalSettings.FileFolder;
            _log.Info($"FolderPath: {_fileFolder}");

            _log.Info("Membership Scheme Start");
            LoadMemberMetadata();
            _log.Info("Membership Scheme Finish");
            _log.Info("Benefits Scheme Start");

            LoadBenefitMetadata();
            _log.Info("Benefits Scheme Finish");
        }

        private void LoadMemberMetadata()
        {
            var schemePath = $"{_fileFolder}{GlobalSettings.MembershipScheme}";
            LoadFiles(schemePath, SchemeType.Members);
        }

        public void LoadBenefitMetadata()
        {
            var schemePath = $"{_fileFolder}{GlobalSettings.BenefitsScheme}";
            LoadFiles(schemePath, SchemeType.Benefits);
        }

        private bool LoadFiles(string schemePath, SchemeType schemeType)
        {
            var metas = _metadataService.ReadMetaFiles(schemePath, schemeType).ToList();

            if (!metas.Any())
            {
                _log.Info($"No files exist in {schemePath}");
                return false; // to signify empty files
            }

            var orderedMetas = metas.OrderBy(x => x.Details.Modified);

            foreach (var meta in orderedMetas)
            {
                var dataPath = $"{schemePath}{meta.Details.FileName}";
                ValidationResult result;

                switch (meta.Details.FileType)
                {
                    case FileType.Excel:
                        _fileProcessingService = new ExcelFileProcessingService();
                        _log.Info($"Processing {dataPath} as File Type Excel");
                        result = _fileProcessingService.ProcessFile(meta, schemeType, dataPath, _log);
                        break;
                    case FileType.Csv:
                        _fileProcessingService = new CsvFileProcessingService();
                        _log.Info($"Processing {dataPath} as File Type CSV");
                        result = _fileProcessingService.ProcessFile(meta, schemeType, dataPath, _log);
                        break;
                    default:
                        result = new ValidationResult
                        {
                            ValidationType = ValidationType.InvalidDataFileType,
                            Metadata = meta
                        };
                        _log.Fatal($"Invalid file type in meta data file { meta.MetadataFileName }");
                        break;
                }

                if (result.ValidationType == ValidationType.Success)
                {
                    var localFileName = ArchiveFile(meta, dataPath);
                    _log.Info($"File archive as {localFileName}");
                }
                else
                {
                    _log.Fatal($"Invalid data in the file - does not match validation description - '{result.ValidationType.GetDescription()}' filename - '{ meta.MetadataFileName }'");
                }
            }

            return true;
        }


        private static void ArchiveLogFiles()
        {
            var info = new DirectoryInfo(GlobalSettings.LogFileFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            var deleteDate = DateTime.Now.AddDays(-GlobalSettings.LogDaysKeep);

            if (!files.Any())
            {
                return;
            }

            var filesToDelete = files.Where(x => x.CreationTime < deleteDate);

            foreach (var logFile in filesToDelete)
            {
                logFile.Delete();
            }
        }

        private string ArchiveFile(Metadata metadata, string filePath)
        {
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var extension = Path.GetExtension(filePath);
            var name = ExcludeNonValidChars(fileName);

            var archiveName = $"Year_{metadata.Details.Year}_RA_{metadata.Details.RelevantAuthority}_Version_{metadata.VersionId}_{name}{extension}";
            var archivePath = $"{GlobalSettings.ArchiveFileFolder}{archiveName}";
            File.Copy(filePath, archivePath);

            var file = new FileInfo(filePath);
            file.Delete();

            file = new FileInfo(metadata.MetadataFileName);
            file.Delete();

            return archiveName;
        }

        public string ExcludeNonValidChars(string fileName)
        {
            fileName = fileName.ToLower();
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"[^a-z0-9\s-]", ""); // Remove all non valid chars          
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s+", " ").Trim(); // convert multiple spaces into one space  
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s", "-"); // //Replace spaces by dashes
            return fileName;
        }
    }
}



