﻿namespace Models.Enums
{
    public enum TransactionType
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
