﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Enums
{
    public class SchemeListModel
    {
        public List<string> BeneficiaryTypes { get; set; }

        public List<string> BenefitTypes { get; set; }

        public List<string> Genders { get; set; }

        public List<string> CivilStatus { get; set; }

        public List<string> PensionBasis { get; set; }

        public List<string> PensionScheme { get; set; }

        public List<string> PrsiClass { get; set; }
    }
}
