﻿using System.Data;

namespace Models.Models
{
    public class ExecuteResult
    {
        public int VersionId { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; }

        public DataTable Result { get; set; }

        public int RowsAffected { get; set; }
    }
}
