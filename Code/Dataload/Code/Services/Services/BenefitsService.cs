﻿using System;
using System.Data;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Services;
using Models.Models;
using Services.Interfaces;

namespace Services.Services
{
    public class BenefitsService : IBenefitsService
    {
        private readonly IBenefitsDataAccessService _benefitsDataAccessService;

        public BenefitsService()
        {
            _benefitsDataAccessService = new BenefitsDataAccessService();
        }

        public void TruncateStagingTables()
        {
            _benefitsDataAccessService.TruncateStagingTables();
        }

        public void LoadBenefitsToStaging(DataTable dt)
        {
            _benefitsDataAccessService.LoadBenefitsToStaging(dt);
        }

        public DataTable GetBenefitsDataTable(DataSet ds, int versionId)
        {
            var dt = CreateTargetTable();

            if (ds == null || ds.Tables.Count <= 0)
            {
                return null;
            }

            var fileData = ds.Tables[0];

            foreach (DataRow row in fileData.Rows)
            {
                var newRow = dt.NewRow();

                newRow["VersionId"] = versionId;

                var origYearString = row[0].ToString() == "" ? null : row[0].ToString();
                var origYearDecimal = origYearString != null ? Convert.ToDecimal(origYearString) : 0;
                var origYearInt = origYearDecimal != 0 ? (int?)(origYearDecimal) : 0;

                newRow["Year"] = origYearInt;

                newRow["PPSN"] = row[1];
                newRow["Dob"] = row[2];
                newRow["FirstName"] = row[3];
                newRow["Surname"] = row[4];
                newRow["MaidenName"] = row[5];
                newRow["Title"] = null;
                newRow["Gender"] = row[6];
                newRow["CivilStatus"] = row[7];
                newRow["PensionAdjustmentOrder"] = row[8];
                newRow["DateOfDeath"] = row[9].ToString() == "" ? DBNull.Value : row[9];
                newRow["HomeAddress1"] = null;
                newRow["HomeAddress2"] = null;
                newRow["HomeAddress3"] = null;
                newRow["HomeAddress4"] = null;
                newRow["HomeAddress5"] = null;
                newRow["Eircode"] = null;


                var raNumberString = row[10].ToString() == "" ? null : row[10].ToString();
                var raNumberDecimal = raNumberString != null ? Convert.ToDecimal(raNumberString) : 0;
                var raNumberInt = raNumberDecimal != 0 ? (int?)(raNumberDecimal) : 0;

                newRow["RaNumber"] = raNumberInt;
                newRow["RaName"] = row[11];
                newRow["PRSIClass"] = row[12];
                newRow["BenPPSN"] = row[13];
                newRow["BenDob"] = row[14].ToString() == "" ? DBNull.Value : row[14];
                newRow["BenFirstName"] = row[15];
                newRow["BenSurname"] = row[16];
                newRow["BenMaidenName"] = row[17];
                newRow["BenTitle"] = null;
                newRow["BenGender"] = row[18];
                newRow["BenType"] = row[19];
                newRow["BenEffectiveDate"] = row[20].ToString() == "" ? DBNull.Value : row[20];
                newRow["PensionPaymentStartDate"] = row[21].ToString() == "" ? DBNull.Value : row[21];
                newRow["PensionPaymentEndDate"] = row[22].ToString() == "" ? DBNull.Value : row[22];
                newRow["GrossAnnualPensionValue"] = row[23].ToString() == "" ? DBNull.Value : row[23];
                newRow["GrossRetirementLumpSum"] = row[24].ToString() == "" ? DBNull.Value : row[24];
                newRow["ShortServiceGratuity"] = row[25].ToString() == "" ? DBNull.Value : row[25];
                newRow["GrossDeathGratuity"] = row[26].ToString() == "" ? DBNull.Value : row[26];
                newRow["GratuityPaymentDate"] = row[27].ToString() == "" ? DBNull.Value : row[27];

                dt.Rows.Add(newRow);
            }

            return dt;
        }

        public ExecuteResult SaveBenefits(int versionId)
        {
            return _benefitsDataAccessService.SaveBenefits(versionId);
        }

        private static DataTable CreateTargetTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("VersionId", typeof(int));
            dt.Columns.Add("Year", typeof(int));
            dt.Columns.Add("PPSN", typeof(string));
            dt.Columns.Add("Dob", typeof(DateTime));
            dt.Columns.Add("FirstName", typeof(string));
            dt.Columns.Add("Surname", typeof(string));
            dt.Columns.Add("MaidenName", typeof(string));
            dt.Columns.Add("Title", typeof(string));
            dt.Columns.Add("Gender", typeof(string));
            dt.Columns.Add("CivilStatus", typeof(string));
            dt.Columns.Add("PensionAdjustmentOrder", typeof(string));
            dt.Columns.Add("DateOfDeath", typeof(DateTime));
            dt.Columns.Add("HomeAddress1", typeof(string));
            dt.Columns.Add("HomeAddress2", typeof(string));
            dt.Columns.Add("HomeAddress3", typeof(string));
            dt.Columns.Add("HomeAddress4", typeof(string));
            dt.Columns.Add("HomeAddress5", typeof(string));
            dt.Columns.Add("Eircode", typeof(string));
            dt.Columns.Add("RaNumber", typeof(int));
            dt.Columns.Add("RaName", typeof(string));
            dt.Columns.Add("PRSIClass", typeof(string));
            dt.Columns.Add("BenPPSN", typeof(string));
            dt.Columns.Add("BenDob", typeof(DateTime));
            dt.Columns.Add("BenFirstName", typeof(string));
            dt.Columns.Add("BenSurname", typeof(string));
            dt.Columns.Add("BenMaidenName", typeof(string));
            dt.Columns.Add("BenTitle", typeof(string));
            dt.Columns.Add("BenGender", typeof(string));
            dt.Columns.Add("BenType", typeof(string));
            dt.Columns.Add("BenEffectiveDate", typeof(DateTime));
            dt.Columns.Add("PensionPaymentStartDate", typeof(DateTime)).AllowDBNull = true;
            dt.Columns.Add("PensionPaymentEndDate", typeof(DateTime)).AllowDBNull = true;
            dt.Columns.Add("GrossAnnualPensionValue", typeof(decimal));
            dt.Columns.Add("GrossRetirementLumpSum", typeof(decimal));
            dt.Columns.Add("ShortServiceGratuity", typeof(decimal));
            dt.Columns.Add("GrossDeathGratuity", typeof(decimal));
            dt.Columns.Add("GratuityPaymentDate", typeof(DateTime));

            return dt;
        }

    }
}
