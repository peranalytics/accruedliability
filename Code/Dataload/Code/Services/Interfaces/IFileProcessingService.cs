﻿using System.Data;
using Models.Enums;
using Models.Models;

namespace Services.Interfaces
{
    public interface IFileProcessingService
    {
        DataSet GetFileAsDataSet(string path);

        ValidationResult ProcessFile(Metadata metadata, SchemeType schemeType, string dataPath, log4net.ILog log);
    }
}
