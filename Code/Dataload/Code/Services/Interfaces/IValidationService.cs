﻿using Models.Models;

namespace Services.Interfaces
{
    public interface IValidationService
    {
        ValidationResult Validate();
    }
}
