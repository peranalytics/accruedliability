﻿using System;
using System.Configuration;

namespace Load.Services
{
    public static class GlobalSettings
    {
        public static string FileFolder => ConfigurationManager.AppSettings["FileFolder"];

        public static string ArchiveFileFolder => ConfigurationManager.AppSettings["ArchiveFileFolder"];

        public static string OriginalsFileFolder => ConfigurationManager.AppSettings["OriginalsFileFolder"];

        public static string ErrorEmailToAddress => ConfigurationManager.AppSettings["ErrorEmailToAddress"];

        public static string ReportEmailToAddress => ConfigurationManager.AppSettings["ReportEmailToAddress"];

        public static string EmailFromAddress => ConfigurationManager.AppSettings["EmailFromAddress"];

        public static string BusinessClientName => ConfigurationManager.AppSettings["BusinessClientName"];

        public static string SmtpServer => ConfigurationManager.AppSettings["SmtpServer"];

        public static int SmtpPortNumber => Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPortNumber"]);

        public static bool SendEmail => Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]);

        public static string EmailSubject => ConfigurationManager.AppSettings["EmailSubject"];

        public static string LogFileFolder => ConfigurationManager.AppSettings["LogFileFolder"];

        public static int LogDaysKeep => Convert.ToInt32(ConfigurationManager.AppSettings["LogDaysKeep"]);
    }
}
