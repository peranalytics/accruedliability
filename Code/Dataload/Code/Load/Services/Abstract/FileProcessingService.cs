﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Load.DataAccessLayer;
using Load.Services.Interfaces;
using Load.Services.Services;
using Models.Enums;
using Models.Models;

namespace Load.Services.Abstract
{
    public class FileProcessingService
    {
        private readonly SchemeDataAccess _schemeDataAccess;
        private readonly SchemeService _schemeService;
        private readonly MetaDataService _metaDataService;

        private readonly log4net.ILog _log;

        public FileProcessingService(log4net.ILog log)
        {
            _schemeService = new SchemeService();
            _schemeDataAccess = new SchemeDataAccess();
            _metaDataService = new MetaDataService(log);
            _log = log;
        }

        public ValidationResult ProcessFile(MetaData metadata)
        {
            _log.Info("ProcessFile - File Processing Service");
            var fileData = GetFileAsDataSet(metadata);
            _log.Info("ProcessFile - GetFileAsDataSet Succeeeded");

            if (fileData == null || fileData.Tables.Count == 0 || fileData.Tables[0].Rows.Count == 0)
            {
                _log.Info($"ProcessFile -{metadata.Details.SchemeType.ToString()} - Data Empty - All data for relevant authority Id:  {metadata.Details.RelevantAuthority} and Year: {metadata.Details.Year} will be deleted");
            }

            SchemeListModel schemeListModel = GetSchemeListModel();

            switch (metadata.Details.SchemeType)
            {
                case SchemeType.Actives:
                    _log.Info("ProcessFile - Scheme Type Actives");
                    _schemeService.LoadActivesData(fileData, metadata, schemeListModel);
                    break;
                case SchemeType.Deferreds:
                    _log.Info("ProcessFile - Scheme Type Deferreds");
                    _schemeService.LoadDeferredData(fileData, metadata, schemeListModel);
                    break;
                case SchemeType.Payments:
                    _log.Info("ProcessFile - Scheme Type Payments");
                    _schemeService.LoadPaymentsData(fileData, metadata, schemeListModel);
                    break;
                case SchemeType.Unknown:
                    _log.Error("Unknown Scheme Type");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(metadata.Details.SchemeType), metadata.Details.SchemeType, null);
            }

            var archiveName = ArchiveFile(metadata);
            _metaDataService.MarkMetaDataVersionAsSuccess(metadata.VersionId, archiveName);

            return new ValidationResult
            {
                ValidationType = ValidationType.Success,
                MetaData = metadata
            };
        }

        public DataSet GetFileAsDataSet(MetaData metaData)
        {
            DataSet ds = null;
            var filePath = $"{GlobalSettings.FileFolder}{metaData.Details.FileName}";

            switch (metaData.Details.FileType)
            {
                case FileType.Excel:
                    var excelProcessingService = new ExcelFileProcessingService();
                    _log.Info($"Processing {filePath} as File Type CSV");
                    ds = excelProcessingService.GetFileAsDataSet(filePath);
                    break;
                case FileType.Csv:
                    var csvFileProcessingService = new CsvFileProcessingService();
                    _log.Info($"Processing {filePath} as File Type CSV");
                    ds = csvFileProcessingService.GetFileAsDataSet(filePath);
                    break;
                default:
                    _log.Fatal($"Invalid file type in meta data file {filePath}");
                    break;
            }

            return ds;
        }

        private SchemeListModel GetSchemeListModel()
        {
            var schemeListModel = new SchemeListModel
            {
                BeneficiaryTypes = _schemeDataAccess.GetAllBeneficiaryTypes(),
                BenefitTypes = _schemeDataAccess.GetAllBenefitTypes(),
                CivilStatus = _schemeDataAccess.GetAllCivilStatus(),
                Genders = _schemeDataAccess.GetAllGender(),
                PensionBasis = _schemeDataAccess.GetAllPensionBasis(),
                PensionScheme = _schemeDataAccess.GetAllPensionScheme(),
                PrsiClass = _schemeDataAccess.GetAllPrsiClass()
            };

            return schemeListModel;
        }

        
        private string ArchiveFile(MetaData md)
        {
            var filePath = $"{GlobalSettings.FileFolder}{md.Details.FileName}";
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var extension = Path.GetExtension(filePath);
            var name = ExcludeNonValidChars(fileName);

            var archiveName = $"Version_{md.VersionId}_RA_{md.Details.RelevantAuthority}_Year_{md.Details.Year}_{name}{extension}";
            var archivePath = $"{GlobalSettings.ArchiveFileFolder}{archiveName}";
            File.Copy(filePath, archivePath);

            var originalNewFileLocation = $"{GlobalSettings.OriginalsFileFolder}{fileName}{extension}";
            File.Copy(filePath, originalNewFileLocation, true);

            var metaFilePath = md.MetadataFileName;
            var metafileName = Path.GetFileNameWithoutExtension(metaFilePath);
            var metafileExtension = Path.GetExtension(metaFilePath);
            var metaDataNewFileLocation = $"{GlobalSettings.OriginalsFileFolder}{metafileName}{metafileExtension}";
            File.Copy(metaFilePath, metaDataNewFileLocation, true);

            var file = new FileInfo(filePath);
            file.Delete();

            var metafile = new FileInfo(md.MetadataFileName);
            metafile.Delete();

            return archiveName;
        }

        public string ExcludeNonValidChars(string fileName)
        {
            fileName = fileName.ToLower();
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"[^a-z0-9\s-]", ""); // Remove all non valid chars          
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s+", " ").Trim(); // convert multiple spaces into one space  
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s", "-"); // //Replace spaces by dashes
            return fileName;
        }
    }
}
