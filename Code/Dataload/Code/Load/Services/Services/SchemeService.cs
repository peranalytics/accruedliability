﻿using System;
using System.Data;
using Load.DataAccessLayer;
using Models.Enums;
using Models.Models;

namespace Load.Services.Services
{
    public class SchemeService
    {
        private readonly SchemeDataAccess _schemeDataAccess;

        public SchemeService()
        {
            _schemeDataAccess = new SchemeDataAccess();
        }

        public void LoadActivesData(DataSet ds, MetaData metaData, SchemeListModel schemeList)
        {
            // Get table as DS
            // Validate it against list
            // Load into staging - should be fine
            // Try load into real table
            // If it doesn't work then put it into unvalidated - log errors
            // If crash move file to processed with error messages
        }

        public void LoadDeferredData(DataSet ds, MetaData metaData, SchemeListModel schemeList)
        {

        }

        public void LoadPaymentsData(DataSet ds, MetaData metaData, SchemeListModel schemeList)
        {

        }

        private static DataTable CreateActivesTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("VersionId", typeof(int));
            dt.Columns.Add("RelevantAuthorityId", typeof(int));
            dt.Columns.Add("Year", typeof(int));
            dt.Columns.Add("PPSN", typeof(string));

            dt.Columns.Add("DateOfBirth", typeof(DateTime));
            dt.Columns.Add("Gender", typeof(string));
            dt.Columns.Add("CivilStatus", typeof(string));
            dt.Columns.Add("PensionScheme", typeof(string));
            dt.Columns.Add("DateOfEntryIntoScheme", typeof(DateTime));
            dt.Columns.Add("MinimumNormalRetirementAge", typeof(int));
            dt.Columns.Add("PrsiClass", typeof(string));
            dt.Columns.Add("AnnualPensionablePayFTE", typeof(float));
            dt.Columns.Add("LengthOfService", typeof(float));
            dt.Columns.Add("FTE", typeof(float));
            dt.Columns.Add("Grade", typeof(string));
            dt.Columns.Add("ScalePoint", typeof(string));
            dt.Columns.Add("IncrementDate", typeof(DateTime));

            return dt;
        }
    }
}
