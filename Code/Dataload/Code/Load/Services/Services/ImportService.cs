﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Load.DataAccessLayer;
using Load.Services.Abstract;
using Load.Services.Interfaces;
using Models.Enums;
using Models.Models;

namespace Load.Services.Services
{
    public class ImportService
    {
        private readonly log4net.ILog _log;
        private readonly FileProcessingService _fileProcessingService;
        private readonly DataAccessService _dataAccessService;
        private readonly MetaDataService _metadataService;
        private readonly IMailerService _mailerService;

        private string _fileFolder;

        public ImportService(log4net.ILog log)
        {
            _dataAccessService = new DataAccessService();
            _mailerService = new MailerService();
            _metadataService = new MetaDataService(log);
            _fileProcessingService = new FileProcessingService(log);
            _log = log;
        }

        public void StartDataload()
        {
            LoadData();
            _log.Info("Finish");
            ArchiveLogFiles();
        }

        public void LoadData()
        {
            _fileFolder = GlobalSettings.FileFolder;
            _log.Info($"FolderPath: {_fileFolder}");

            var info = new DirectoryInfo(_fileFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            if (!files.Any())
            {
                _log.Info($"Meta Data Summary Create Start");
                _metadataService.CreateMetaDataSummary();
                _log.Info($"Meta Data Summary Create End");

                return; // DO NOT SEND EMAIL FOR NO FILES
            }

            LoadFiles();

            if (GlobalSettings.SendEmail)
            {
                _mailerService.SendSuccessEmail();
            }

            _log.Info($"Meta Data Summary Create Start");
            _metadataService.CreateMetaDataSummary();
            _log.Info($"Meta Data Summary Create End");

        }

        private bool LoadFiles()
        {
            var metas = _metadataService.LoadMetaFiles(_fileFolder).ToList();

            if (!metas.Any())
            {
                _log.Info($"No files exist in {_fileFolder}");
                return false;
            }

            var activeMetas = metas.Where(x => x.Details.SchemeType == SchemeType.Actives);
            var activesMetasEnumerable = activeMetas as IList<MetaData> ?? activeMetas.ToList();

            if (activesMetasEnumerable.Any())
            {
                _log.Info("Active Scheme Start");
                LoadFiles(activesMetasEnumerable);
                _log.Info("Active Scheme Finish");
            }

            var deferredMetas = metas.Where(x => x.Details.SchemeType == SchemeType.Deferreds);
            var deferredMetasEnumerable = deferredMetas as IList<MetaData> ?? deferredMetas.ToList();

            if (deferredMetasEnumerable.Any())
            {
                _log.Info("Deferred Scheme Start");
                LoadFiles(deferredMetasEnumerable);
                _log.Info("Deferred Scheme Finish");
            }

            var paymentMetas = metas.Where(x => x.Details.SchemeType == SchemeType.Payments);
            var paymentMetasEnumerable = paymentMetas as IList<MetaData> ?? paymentMetas.ToList();

            if (paymentMetasEnumerable.Any())
            {
                _log.Info("Payments Scheme Start");
                LoadFiles(paymentMetasEnumerable);
                _log.Info("Payments Scheme Finish");
            }

            return true;
        }

        public void LoadFiles(IEnumerable<MetaData> metas)
        {
            foreach (var metaData in metas)
            {
                _fileProcessingService.ProcessFile(metaData);
            }

        }

        private void TruncateStagingTables()
        {
            _dataAccessService.TruncateStagingTables();
        }

        private static void ArchiveLogFiles()
        {
            var info = new DirectoryInfo(GlobalSettings.LogFileFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            var deleteDate = DateTime.Now.AddDays(-GlobalSettings.LogDaysKeep);

            if (!files.Any())
            {
                return;
            }

            var filesToDelete = files.Where(x => x.CreationTime < deleteDate);

            foreach (var logFile in filesToDelete)
            {
                logFile.Delete();
            }
        }
    }
}



