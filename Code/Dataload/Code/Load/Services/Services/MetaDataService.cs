﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Load.DataAccessLayer;
using Load.Services.Extensions;
using Models.Enums;
using Models.Models;
using Newtonsoft.Json;

namespace Load.Services.Services
{
    public class MetaDataService
    {
        private readonly log4net.ILog _log;
        private readonly MetaDataAccessService _metadataDataAccess;


        public MetaDataService(log4net.ILog log)
        {
            _metadataDataAccess = new MetaDataAccessService();
            _log = log;
        }

        public IEnumerable<MetaData> LoadMetaFiles(string schemePath)
        {
            _log.Info($"SchemePath: {schemePath}");

            var info = new DirectoryInfo(schemePath);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            if (!files.Any())
            {
                return new List<MetaData>();
            }

            var results = (from file in files where file.Extension == ".json" select ProcessMetaFile(file)).ToList();
            return results;
        }

        public void MarkMetaDataVersionAsSuccess(int versionId, string localFileName)
        {
            _metadataDataAccess.UpdateFileHistory(versionId, localFileName, true);
        }

        public void CreateMetaDataSummary()
        {
            _metadataDataAccess.CreateFileHistorySummary();
        }

        private MetaData ProcessMetaFile(FileSystemInfo file)
        {
            var stream = File.ReadAllText(file.FullName);

            var result = JsonConvert.DeserializeObject<MetaData>(stream);
            var resultDetails = JsonConvert.DeserializeObject<MetadataDetails>(stream);

            if (result.Details == null)
            {
                result.Details = resultDetails;
            }

            result.MetadataFileName = file.FullName;

            switch (resultDetails.SchemeFile)
            {
                case "Accrued Liability - Actives":
                    resultDetails.SchemeType = SchemeType.Actives;
                    break;
                case "Accrued Liability - Deferreds":
                    resultDetails.SchemeType = SchemeType.Deferreds;
                    break;
                case "Accrued Liability - Payment":
                    resultDetails.SchemeType = SchemeType.Payments;
                    break;
                default:
                    resultDetails.SchemeType = SchemeType.Unknown;
                    break;
            }

            var execResult = _metadataDataAccess.SaveFileHistory(result, resultDetails.SchemeType.GetDescription());
            _log.Info($"ProcessMetaFile: {execResult.Success} - {execResult.VersionId} - {execResult.Message}");

            result.VersionId = execResult.VersionId;
            return result;
        }
    }
}
