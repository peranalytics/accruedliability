﻿using System.Data;
using System.IO;
using ExcelDataReader;
using Load.Services.Abstract;

namespace Load.Services.Services
{
    public class ExcelFileProcessingService
    {
        public DataSet GetFileAsDataSet(string filePath)
        {
            return LoadExcelFileIntoDataSet(filePath);
        }

        private static DataSet LoadExcelFileIntoDataSet(string dataPath)
        {
            DataSet ds;

            using (var fs = new FileStream(dataPath, FileMode.Open))
            {
                IExcelDataReader reader = null;
                var config = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = false,
                    }
                };

                if (dataPath.EndsWith(".xls"))
                {
                    reader = ExcelReaderFactory.CreateBinaryReader(fs);
                }
                else if (dataPath.EndsWith(".xlsx"))
                {
                    reader = ExcelReaderFactory.CreateOpenXmlReader(fs);
                }

                ds = reader.AsDataSet(config);
            }

            return ds;
        }
    }
}
