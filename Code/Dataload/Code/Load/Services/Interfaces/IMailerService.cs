﻿using System;

namespace Load.Services.Interfaces
{
    public interface IMailerService
    {
        void SendSuccessEmail();

        void SendErrorEmail(Exception exception);
    }
}
