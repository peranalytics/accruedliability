﻿using System.Data;
using System.IO;
using Models.Models;

namespace Load.Services.Interfaces
{
    public interface IFileProcessingService
    {
        DataSet GetFileAsDataSet(string path);

        ValidationResult ProcessFile(FileInfo file, log4net.ILog log);
    }
}
