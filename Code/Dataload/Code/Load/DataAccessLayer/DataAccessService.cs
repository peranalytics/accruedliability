﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Models.Models;

namespace Load.DataAccessLayer
{
    public class DataAccessService
    {
        public void LoadBudgetDataToStaging(DataTable table)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "[dbo].[StagingBudgetData]";
                    con.Open();
                    sqlBulkCopy.WriteToServer(table);

                    con.Close();
                }
            }
        }

        public void TruncateStagingTables()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[TruncateStagingTables]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
