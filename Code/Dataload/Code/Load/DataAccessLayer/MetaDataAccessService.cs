﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Models.Models;

namespace Load.DataAccessLayer
{
    public class MetaDataAccessService
    {
        public ExecuteResult SaveFileHistory(MetaData metdata, string schemeType)
        {
            var execResult = new ExecuteResult();

            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistoryAdd]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@DataStrategyId", SqlDbType.Int).Value = metdata.DataStrategyId;
                    command.Parameters.Add("@RelevantAuthorityId", SqlDbType.Int).Value = metdata.Details.RelevantAuthority;
                    command.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = metdata.Details.FileName;
                    command.Parameters.Add("@Year", SqlDbType.Int).Value = metdata.Details.Year;
                    command.Parameters.Add("@UploadedBy", SqlDbType.VarChar, 255).Value = metdata.Details.UploadedBy;
                    command.Parameters.Add("@TransactionTypeId", SqlDbType.Int).Value = metdata.Details.TransactionType;
                    command.Parameters.Add("@FileTypeId", SqlDbType.Int).Value = metdata.Details.FileType;
                    command.Parameters.Add("@RecordCount", SqlDbType.Int).Value = metdata.Details.RecordCount;
                    command.Parameters.Add("@Validated", SqlDbType.Bit).Value = metdata.Details.Validated;
                    command.Parameters.Add("@ModifyDate", SqlDbType.DateTime).Value = metdata.Details.Modified;
                    command.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = metdata.Details.Created;
                    command.Parameters.Add("@SchemeType", SqlDbType.VarChar, 50).Value = schemeType;

                    var result = command.Parameters.Add("@Result", SqlDbType.Int);
                    result.Direction = ParameterDirection.Output;

                    var resultMessage = command.Parameters.Add("@ResultMessage", SqlDbType.VarChar, 100);
                    resultMessage.Direction = ParameterDirection.Output;

                    con.Open();
                    command.ExecuteNonQuery();

                    execResult.VersionId = (int)(result.Value ?? -1);
                    execResult.Message = Convert.IsDBNull(resultMessage.Value) ? string.Empty : (string)resultMessage.Value;
                    execResult.Success = execResult.VersionId != -1;
                }
            }

            return execResult;
        }

        public void UpdateFileHistory(int versionId, string localFileName, bool success)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistoryUpdate]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@VersionId", SqlDbType.Int).Value = versionId;
                    command.Parameters.Add("@LocalFileName", SqlDbType.NVarChar).Value = localFileName;
                    command.Parameters.Add("@Success", SqlDbType.Bit).Value = success;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void CreateFileHistorySummary()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistorySummaryCreate]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
