﻿using Models.Models;

namespace DataAccessLayer.Interfaces
{
    public interface IMetadataDataAccessService
    {
        ExecuteResult SaveMetaData(Metadata metdata, string schemeType);

        void UpdateMetaData(int versionId, string localFileName, bool success);

        void CreateMetadataSummary();
    }
}
