﻿using System.Data;
using Models.Models;

namespace DataAccessLayer.Interfaces
{
    public interface IBenefitsDataAccessService
    {
        void TruncateStagingTables();

        void LoadBenefitsToStaging(DataTable table);

        ExecuteResult SaveBenefits(int versionId);
    }
}
