﻿using System.Data;

namespace DataAccessLayer.Interfaces
{
    public interface IRelevantAuthorityDataAccessService
    {
        DataSet GetRelevantAuthories();

        void UpdateRelavantAuthority(int id, double latitude, double longtitude);
    }
}
