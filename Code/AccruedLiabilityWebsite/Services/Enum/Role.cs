﻿
using System.ComponentModel;

namespace Services.Enum
{
    public enum Role
    {
        [Description("Administrator")]
        Administrator = 1,
        [Description("Pension Administrator")]
        PensionAdministrator = 2,
        [Description("Pension Viewer")]
        PensionViewer = 3,
        [Description("Policy Viewer")]
        PolicyViewer = 4
    }
}
