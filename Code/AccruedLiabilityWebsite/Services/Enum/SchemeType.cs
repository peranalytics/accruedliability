﻿using System.ComponentModel;

namespace Services.Enum
{
    public enum SchemeType
    {
        Actives = 1,
        Deferreds = 2,
        Payments = 3,
        Unknown = 4
    }
}
