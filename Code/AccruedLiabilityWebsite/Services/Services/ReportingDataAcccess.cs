﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Services.Interfaces;
using Services.Models;

namespace Services.Services
{
    public class ReportingDataAcccess : IReportingDataAccess
    {
        public IEnumerable<ReportItem> GetAllReports()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Report].[GetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDataSetToList(ds);
        }

        public IEnumerable<ReportItem> GetAllReportsByUserId(int userId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Report].[GetAllByUserId]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDataSetToList(ds);
        }

        public IEnumerable<ReportItem> GetAllReportsByPermissionId(int userId, int permissionId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Report].[GetAllByPermissionId]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@PermissionId", SqlDbType.Int).Value = permissionId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDataSetToList(ds);
        }

        public ReportItem GetReportById(int id, int userId)
        {
            var reports = GetAllReportsByUserId(userId);
            return reports.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<ReportItem> GetFrequentlyViewedReports(int currentUserId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Dashboard].[ReportViews]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@CurrentUserId", SqlDbType.Int).Value = currentUserId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDataSetToList(ds);
        }

        public IEnumerable<ReportItem> GetReportUsage(int currentUserId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Dashboard].[ReportUsage]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@CurrentUserId", SqlDbType.Int).Value = currentUserId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDataSetToList(ds);
        }

        private static IEnumerable<ReportItem> ConvertDataSetToList(DataSet ds)
        {
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    return ConvertDataTableToList(ds.Tables[0]);
                }

                if (ds.Tables.Count > 1 && ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    return ConvertDataTableToList(ds.Tables[1]);
                }
            }

            return new List<ReportItem>(); ;
        }

        private static IEnumerable<ReportItem> ConvertDataTableToList(DataTable dt)
        {
            var cols = dt.Columns;
            return (dt.Rows.Cast<DataRow>()
                .Select(row => new ReportItem
                {
                    Id = Convert.ToInt32(row["Id"]),
                    ShortReportName = row["ShortReportName"]?.ToString() ?? string.Empty,
                    ReportName = cols.Contains("ReportName")
                        ? (string) (row["ReportName"] == DBNull.Value ? null : row["ReportName"])
                        : string.Empty,
                    ReportDescription = cols.Contains("ReportDescription")
                        ? (string) (row["ReportDescription"] == DBNull.Value ? null : row["ReportDescription"])
                        : string.Empty,
                    ReportPath = row["ReportUrl"]?.ToString() ?? string.Empty,
                    LastViewDate = cols.Contains("ViewDate")
                        ? (DateTime?) (row["ViewDate"] == DBNull.Value ? null : row["ViewDate"])
                        : null,
                    NumberOfViews = cols.Contains("NumberOfViews")
                        ? (int?) (row["NumberOfViews"] == DBNull.Value ? null : row["NumberOfViews"])
                        : null,
                    ViewedBy = cols.Contains("UserName")
                        ? (string)(row["UserName"] == DBNull.Value ? null : row["UserName"])
                        : string.Empty,
                })).ToList();
        }

    }
}
