﻿using Services.Models;

namespace Services.Services
{
    public class AdminService
    {
        private readonly AdminDataAccess _adminDataAccess;

        public AdminService()
        {
            _adminDataAccess = new AdminDataAccess();
        }

        public RelevantAuthorityList GetRelevantAuthorityList(int page, int pageSize, string filterBy)
        {
            return _adminDataAccess.GetAllRelevantAuthorities(page, pageSize, filterBy);
        }

        public RelevantAuthority GetRelevantAuthorityById(int relevantAuthorityId)
        {
            return _adminDataAccess.GetRelevantAuthorityById(relevantAuthorityId);
        }
    }
}
