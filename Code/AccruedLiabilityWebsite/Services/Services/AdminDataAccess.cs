﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Services.Models;

namespace Services.Services
{
    public class AdminDataAccess
    {
        private readonly string _connectionString;

        public AdminDataAccess()
        {
            _connectionString = "SQLAuthConnection";
        }

        public RelevantAuthorityList GetAllRelevantAuthorities(int page, int pageSize, string filterBy)
        {
            var relevantAuthorityList = new RelevantAuthorityList();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[RelevantAuthorityGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {

                    command.Parameters.Add("@Page", SqlDbType.Int).Value = page;
                    command.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;

                    if (filterBy != null)
                    {
                        command.Parameters.Add("@FilterBy", SqlDbType.VarChar).Value = filterBy;
                    }

                    command.Parameters.Add("@TotalRaCount", SqlDbType.Int).Direction = ParameterDirection.Output;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    if (ds.Tables.Count == 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
                    {
                        relevantAuthorityList.RelevantAuthorities = new List<RelevantAuthority>();
                        relevantAuthorityList.TotalCount = 0;
                        return relevantAuthorityList;
                    }

                    var raCount = Convert.ToInt32(command.Parameters["@TotalRaCount"].Value);

                    relevantAuthorityList.RelevantAuthorities = GetAllRelevantAuthorities(ds);
                    relevantAuthorityList.TotalCount = raCount;

                    return relevantAuthorityList;
                }
            }
        }

        public RelevantAuthority GetRelevantAuthorityById(int relevantAuthorityId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[RelevantAuthorityGetById]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@RelevantAuthorityId", SqlDbType.Int).Value = relevantAuthorityId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }
            ;
            var ras = GetAllRelevantAuthorities(ds);
            return ras.FirstOrDefault();
        }

        private List<RelevantAuthority> GetAllRelevantAuthorities(DataSet ds)
        {
            var raList = new List<RelevantAuthority>();

            foreach (DataRow dataRow in ds.Tables[0].Rows)
            {
                var ra = new RelevantAuthority
                {
                    RelevantAuthorityId = Convert.ToInt32(dataRow["RelevantAuthorityId"].ToString()),
                    RelevantAuthorityName = dataRow["RelevantAuthorityName"].ToString()
                };


                raList.Add(ra);
            }

            return raList;
        }
    }
}
