﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Microsoft.AspNet.Identity;
using Services.Enum;
using Services.Models;

namespace Services.Services
{
    public class AccountService
    {
        private readonly AccountDataAccess _accountDataAccess;

        public AccountService()
        {
            _accountDataAccess = new AccountDataAccess();
        }

        public AccountResult LoginUser(string username, string password)
        {
            var result = CheckUser(username);

            if (result == null)
            {
                result = new AccountResult();
                result.Success = false;
                result.Message = "Login failed, please try again";

                return result;
            }

            IPasswordHasher pw = new PasswordHasher();
            var passwordCheck = pw.VerifyHashedPassword(result.HashedPassword, password);

            if (passwordCheck == PasswordVerificationResult.Success)
            {
                result.Success = true;
                return result;
            }

            result.Success = false;
            result.Message = "Login failed, please try again"; // Chnage this to have a specific log error message if password diff

            return result;
        }

        public AccountResult CheckResetPassword(string username, string password)
        {
            var result = CheckUser(username);

            if (result == null)
            {
                return new AccountResult
                {
                    Success = false,
                    Message = "Password reset failed, please contact your administrator"
                };
            }

            IPasswordHasher pw = new PasswordHasher();
            var passwordCheck = pw.VerifyHashedPassword(result.HashedPassword, password);

            if (passwordCheck == PasswordVerificationResult.Success)
            {
                result.Success = true;
                return result;
            }

            result.Success = false;
            result.Message = "Reset password failed, please check your password and try again";

            return result;
        }

        public AccountResult CheckUser(string username)
        {
            var ds = _accountDataAccess.GetUserByUserName(username);

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return null;
            }

            var dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
            {
                return null;
            }

            var dr = dt.Rows[0];

            return new AccountResult
            {
                UserId = dr["UserId"].ToString(),
                HashedPassword = dr["PasswordHash"].ToString()
            };
        }

        public RegisterResult RegisterUser(string username, string password, string firstName, string lastName, int roleId)
        {
            var checkUser = CheckUser(username);

            if (checkUser != null)
            {
                return new RegisterResult { Success = false, Message = $"{username} - username already exists" };
            }

            IPasswordHasher pw = new PasswordHasher();

            var hashedPassword = pw.HashPassword(password);
            var securityStamp = Guid.NewGuid().ToString("D");

            return _accountDataAccess.RegisterUser(hashedPassword, securityStamp, username, firstName, lastName, roleId);
        }

        public RegisterResult UpdatePassword(string userId, string password)
        {
            IPasswordHasher pw = new PasswordHasher();

            var hashedPassword = pw.HashPassword(password);
            var securityStamp = Guid.NewGuid().ToString("D");

            return _accountDataAccess.UpdatePassword(userId, hashedPassword, securityStamp);
        }

        public User GetUserDetails(int userId)
        {
            var ds = _accountDataAccess.GetUserDetails(userId);

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count != 1)
            {
                return null;
            }

            var dr = ds.Tables[0].Rows[0];

            return new User
            {
                Id = Convert.ToInt32(dr["UserId"].ToString()),
                Username = dr["Username"].ToString(),
                Role = (Role)Convert.ToInt32(dr["RoleId"].ToString())
            };
        }

        public Role GetUserRole(int userId)
        {
            var user = GetUserDetails(userId);
            return user.Role;
        }

        public IEnumerable<ReportItem> GetUserReports(int userId)
        {
            var reports = new List<ReportItem>();
            var ds = _accountDataAccess.GetUserReports(userId);

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return new List<ReportItem>();
            }

            var rootUrl = ConfigurationManager.AppSettings["SiteUrl"];

            foreach (var reportItem in reports)
            {
                var url = $"{rootUrl}Webforms/ReportViewer.aspx?id={reportItem.Id}";
                reportItem.ReportUrl = url;
            }

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                var reportId = Convert.ToInt32(dr["ReportId"].ToString());
                var ri = new ReportItem
                {
                    Id = reportId,
                    ReportName = dr["ReportName"].ToString(),
                    ReportDescription = dr["reportDescription"].ToString(),
                    ReportPath = dr["ReportUrl"].ToString(),
                };

                var url = $"{rootUrl}Webforms/ReportViewer.aspx?id={reportId}";
                ri.ReportUrl = url;

                reports.Add(ri);
            }

            return reports;
        }
    }
}
