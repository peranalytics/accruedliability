﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Chart.Mvc.ComplexChart;
using Services.Enum;
using Services.Extensions;
using Services.Models;

namespace Services.Services
{
    public class MetadataService
    {
        public IEnumerable<Metadata> GetMetadata(int? startYear = null, int? relevantAuthorityId = null)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistorySummaryGet]", con) { CommandType = CommandType.StoredProcedure })
                {
                    if (startYear.HasValue)
                    {
                        command.Parameters.Add("@StartYear", SqlDbType.Int).Value = startYear.Value;
                    }

                    if (relevantAuthorityId.HasValue)
                    {
                        command.Parameters.Add("@RelevantAuthorityId", SqlDbType.Int).Value = relevantAuthorityId.Value;
                    }

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDatasetToMetadata(ds);
        }

        public IEnumerable<Metadata> GetMetadataByRelevantAuthority(int relevantAuthorityId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistoryByRelevantAuthority]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@RelevantAuthorityId", SqlDbType.Int).Value = relevantAuthorityId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDatasetToMetadataDetails(ds);
        }

        public ChartModel GetFileStatsByScheme(int currentUserId, SchemeType schemeType, int relevantAuthorityId, int? startYear = null)
        {
            var metadata = GetMetadata(relevantAuthorityId: relevantAuthorityId).Where(x => x.SchemeType == schemeType.GetDescription()).ToList();
            var orderedMetaData = metadata.OrderBy(x => x.Year).ToList();

            var schemeVals = orderedMetaData.Select(x => Convert.ToDouble(x.RecordCount ?? 0)).ToList();

            var years = orderedMetaData.Select(x => x.Year).Distinct().ToList();
            var schemeYearDefaultCount = ConfigurationManager.AppSettings["SchemeYearDefaultCount"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["SchemeYearDefaultCount"]) : 8;

            if (years.Any() && years.Count < schemeYearDefaultCount)
            {
                var maxYear = years.Max();
                var variance = schemeYearDefaultCount - years.Count;

                for (var i = 1; i <= variance; i++)
                {
                    years.Add(maxYear + i);
                    schemeVals.Add(0);
                }
            }

            var labels = years.Select(x => x.ToString()).ToList();

            var complexDs = new List<ComplexDataset>
            {
                new ComplexDataset
                {
                    Data = schemeVals,
                    Label =  schemeType.GetDescription(),
                    FillColor = "rgba(121, 140, 155, 1)",
                    StrokeColor = "rgba(121, 140, 155, 1)",
                    PointColor = "#FFFFFF",
                    PointStrokeColor = "#FFFFFF",
                    PointHighlightFill = "#FFFFFF",
                    PointHighlightStroke = "rgba(121, 140, 155, 1)",

                },
            };

            return new ChartModel
            {
                ComplexDataset = complexDs,
                Labels = labels.ToArray()
            };
        }

        public IEnumerable<FileLog> ReadLogFileDownload(int relevantAuthorityId)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            var ds = new DataSet();

            using (var con = new SqlConnection(connectionString))
            {
                using (var command =
                    new SqlCommand("[Security].[ReadDownloadFileLog]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@RelevantAuthorityId", SqlDbType.Int).Value = relevantAuthorityId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDsToFileLogs(ds);
        }

        public Metadata GetMetadataByVersionId(int currentUserId, int versionId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistoryByVersionId]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@VersionId", SqlDbType.Int).Value = versionId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            var metadataDetails = ConvertDatasetToMetadataDetails(ds);
            return metadataDetails.First();
        }

        public IEnumerable<Metadata> GetRecentFileHistory()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistoryMostRecent]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ConvertDatasetToSimpleMetadatas(ds);
        }

        public void LogFileDownload(int versionId, int userId)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Security].[LogFileDownload]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@VersionId", SqlDbType.Int).Value = versionId;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        private IEnumerable<Metadata> ConvertDatasetToMetadata(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return new List<Metadata>();
            }

            var metadataList = new List<Metadata>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var metaData = new Metadata
                {
                    Year = Convert.ToInt32(row["YrId"].ToString()),
                    RelevantAuthority = new RelevantAuthority
                    {
                        RelevantAuthorityId = Convert.ToInt32(row["RelevantAuthorityId"].ToString()),
                        RelevantAuthorityName = row["RelevantAuthorityName"].ToString()
                    },
                    SchemeType = row["Scheme"].ToString(),
                    FileName = row["FileName"].ToString(),
                    RecordCount = (int?)(row["RecordCount"] == DBNull.Value ? null : row["RecordCount"]),
                    ModifyDate = (DateTime?)(row["ModifyDate"] == DBNull.Value ? null : row["ModifyDate"]),
                    Success = (bool?)(row["Success"] == DBNull.Value ? null : row["Success"]),
                    UploadedBy = row["UploadedBy"].ToString()
                };

                metadataList.Add(metaData);
            }

            return metadataList;
        }

        private IEnumerable<Metadata> ConvertDatasetToMetadataDetails(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return null;
            }

            return (from DataRow row in ds.Tables[0].Rows
                    select new Metadata
                    {
                        VersionId = Convert.ToInt32(row["VersionId"].ToString()),

                        Year = Convert.ToInt32(row["Year"].ToString()),
                        RelevantAuthority = new RelevantAuthority
                        {
                            RelevantAuthorityId = Convert.ToInt32(row["RelevantAuthorityId"].ToString()),
                            RelevantAuthorityName = row["RelevantAuthorityName"].ToString()
                        },

                        FileName = row["FileName"].ToString(),
                        UploadedBy = row["UploadedBy"].ToString(),
                        FileType = Convert.ToInt32(row["FileTypeId"].ToString()),
                        RecordCount = (int?)(row["RecordCount"] == DBNull.Value ? null : row["RecordCount"]),
                        CreateDate = (DateTime?)(row["CreateDate"] == DBNull.Value ? null : row["CreateDate"]),
                        ModifyDate = (DateTime?)(row["ModifyDate"] == DBNull.Value ? null : row["ModifyDate"]),
                        SchemeType = row["Scheme"].ToString(),
                        Success = (bool?)(row["Success"] == DBNull.Value ? null : row["Success"]),
                        DataloadedDate = (DateTime?)(row["ModifyDate"] == DBNull.Value ? null : row["EntryUpdateDate"]),
                        LocalFileName = row["LocalFileName"].ToString(),
                    }).ToList();
        }

        private IEnumerable<FileLog> ConvertDsToFileLogs(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return new List<FileLog>();
            }

            return (from DataRow row in ds.Tables[0].Rows
                select new FileLog
                {
                    
                    VersionId = (int)row["VersionId"],
                    UserName = row["UserName"].ToString(),
                    SchemeType = GetSchemeTypeFromString(row["Scheme"].ToString()),
                    FileName = row["FileName"].ToString(),
                    Period = row["Year"].ToString(),
                    LogDate = (DateTime)row["CreateDate"],
                }).ToList();
        }

        public SchemeType GetSchemeTypeFromString(string schemeType)
        {
            SchemeType schemeTypeEnum;
            TryParseEnum<SchemeType>(schemeType, out schemeTypeEnum);

            return schemeTypeEnum;
        }

        public static bool TryParseEnum<TEnum>(string aName, out TEnum aValue) where TEnum : struct
        {
            try
            {
                aValue = (TEnum)System.Enum.Parse(typeof(TEnum), aName);
                return true;
            }
            catch
            {
                aValue = default(TEnum);
                return false;
            }
        }

        private IEnumerable<Metadata> ConvertDatasetToSimpleMetadatas(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return null;
            }

            return (from DataRow row in ds.Tables[0].Rows
                select new Metadata
                {
                    Year = Convert.ToInt32(row["YrId"].ToString()),
                    RelevantAuthority = new RelevantAuthority
                    {
                        RelevantAuthorityId = Convert.ToInt32(row["RelevantAuthorityId"].ToString()),
                        RelevantAuthorityName = row["RelevantAuthorityName"].ToString()
                    },

                    FileName = row["FileName"].ToString(),
                    UploadedBy = row["UploadedBy"].ToString(),
                    RecordCount = (int?)(row["RecordCount"] == DBNull.Value ? null : row["RecordCount"]),
                    ModifyDate = (DateTime?)(row["ModifyDate"] == DBNull.Value ? null : row["ModifyDate"]),
                    SchemeType = row["Scheme"].ToString(),
                    Success = (bool?)(row["Success"] == DBNull.Value ? null : row["Success"]),
                }).ToList();
        }

    }
}
