﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Services.Models;

namespace Services.Services
{
    public class AccountDataAccess
    {
        private readonly string _ConnectionString;

        public AccountDataAccess()
        {
            _ConnectionString = "SQLAuthConnection";
        }

        public DataSet GetUserByUserName(string username)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_ConnectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Security].[CheckLogin]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@UserName", SqlDbType.Text).Value = username;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ds;
        }

        public RegisterResult RegisterUser(string hashedPassword, string securityStamp, string username, string firstName, string lastName, int roleId)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[_ConnectionString].ConnectionString;

            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    con.Open();

                    using (var command = new SqlCommand("[Security].[RegisterUser]", con) { CommandType = CommandType.StoredProcedure })
                    {
                        command.Parameters.Add("@HashedPassword", SqlDbType.Text).Value = hashedPassword;
                        command.Parameters.Add("@SecurityStamp", SqlDbType.Text).Value = securityStamp;
                        command.Parameters.Add("@UserName", SqlDbType.Text).Value = username;
                        command.Parameters.Add("@FirstName", SqlDbType.Text).Value = firstName;
                        command.Parameters.Add("@LastName", SqlDbType.Text).Value = lastName;
                        command.Parameters.Add("@RoleId", SqlDbType.Int).Value = roleId;

                        command.ExecuteNonQuery();
                    }

                    con.Close();
                }

                return new RegisterResult { Success = true };
            }
            catch (Exception e)
            {
                return new RegisterResult { Success = false, Message = "Create user has failed, please contact the administrator", Exception = e };
            }
        }

        public RegisterResult UpdatePassword(string userId, string hashedPassword, string securityStamp)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[_ConnectionString].ConnectionString;

            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    con.Open();

                    using (var command = new SqlCommand("[Security].[UpdatePassword]", con) { CommandType = CommandType.StoredProcedure })
                    {
                        command.Parameters.Add("@UserId", SqlDbType.Text).Value = userId;
                        command.Parameters.Add("@HashedPassword", SqlDbType.Text).Value = hashedPassword;
                        command.Parameters.Add("@SecurityStamp", SqlDbType.Text).Value = securityStamp;
                        command.ExecuteNonQuery();

                    }

                    con.Close();
                }

                return new RegisterResult { Success = true };
            }
            catch (Exception e)
            {
                return new RegisterResult { Success = false, Message = "Update password has failed, please contact the administrator", Exception = e };
            }
        }

        public DataSet GetUserDetails(int userId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_ConnectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Security].[GetUser]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ds;
        }

        public DataSet GetUserReports(int userId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_ConnectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Security].[GetUserReports]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ds;
        }
    }
}
