﻿using System;
using Services.Enum;

namespace Services.Models
{
    public class FileLog
    {
        public int VersionId { get; set; }

        public string FileName { get; set; }

        public string UserName { get; set; }

        public SchemeType SchemeType { get; set; }

        public DateTime LogDate { get; set; }

        public string Period { get; set; }
    }
}
