﻿namespace Services.Models
{
    public class ReportConfiguration
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public string Domain { get; set; }

        public string ReportServerUrl { get; set; }
    }
}
