﻿namespace Services.Models
{
    public class RelevantAuthority
    {
        public int RelevantAuthorityId { get; set; }

        public string RelevantAuthorityName { get; set; }

        public Department Department { get; set; }
    }
}
