﻿namespace Services.Models
{
    public class Menu
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string GroupName { get; set; }
    }
}
