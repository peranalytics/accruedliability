﻿namespace Services.Models
{
    public class DepartmentGroup
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
