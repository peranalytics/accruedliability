﻿
using System;

namespace Services.Models
{
    public class ReportItem
    {
        public int Id { get; set; }

        public string ReportName { get; set; }

        public string ShortReportName { get; set; }

        public string ReportPath { get; set; }

        public string ReportDescription { get; set; }

        public string ReportUrl { get; set; }

        public int? NumberOfViews { get; set; }

        public DateTime? LastViewDate { get; set; }

        public string ViewedBy { get; set; }
    }
}
