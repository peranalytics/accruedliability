﻿using System.Collections.Generic;
using Services.Enum;

namespace Services.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string PasswordHash { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Enabled { get; set; }

        public bool ResetRequired { get; set; }

        public string PhoneNumber { get; set; }

        public Role Role { get; set; }

       
    }
}
