﻿using System.Collections.Generic;
using Services.Models;

namespace Services.Interfaces
{
    public interface IReportingDataAccess
    {
        IEnumerable<ReportItem> GetAllReports();

        IEnumerable<ReportItem> GetAllReportsByUserId(int userId);

        IEnumerable<ReportItem> GetAllReportsByPermissionId(int userId, int permissionId);

        ReportItem GetReportById(int id, int userId);

        IEnumerable<ReportItem> GetFrequentlyViewedReports(int currentUserId);

        IEnumerable<ReportItem> GetReportUsage(int currentUserId);
    }
}
