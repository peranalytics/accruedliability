﻿using System.Web.Optimization;

namespace Databank
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.3.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/site.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                "~/Scripts/Chart.js"
                ));

            bundles.Add(new StyleBundle("~/Content/bundlecss").Include(
                    "~/Content/font-lato.css",
                    "~/Content/bootstrap.css",
                    "~/Content/simple-line-icons.css"));

            bundles.Add(new StyleBundle("~/Content/bundlespscss").Include(
                 "~/Content/site.css",
                 "~/Content/menu.css",
                 "~/Content/form.css",
                 "~/Content/header.css"));
        }
    }
}
