﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Databank
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie

            var cookieProvider = new CookieAuthenticationProvider();

            var applyRedirect = cookieProvider.OnApplyRedirect;
            cookieProvider.OnApplyRedirect = context =>
            {
                if (context.RedirectUri.StartsWith("http://" + context.Request.Host))
                {
                    context.RedirectUri = context.RedirectUri.Substring(
                        context.RedirectUri.IndexOf('/', "http://".Length));
                }
                applyRedirect(context);
            };

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                CookieHttpOnly = false,
                Provider = cookieProvider
            });
        }
    }
}