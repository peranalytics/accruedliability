﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chart.Mvc.ComplexChart;
using Chart.Mvc.SimpleChart;
using Databank.Models;
using Microsoft.AspNet.Identity;
using Services.Enum;
using Services.Services;

namespace Databank.Helpers
{
    public class ChartHelper
    {
        private readonly MetadataService _metadataService;

        private readonly int _userId = Convert.ToInt32(HttpContext.Current.User.Identity.GetUserId());

        public ChartHelper()
        {
            _metadataService = new MetadataService();
        }

        public ChartViewModel BuildFileStatsBySchemeChartViewModel(int startYear, SchemeType schemeType, int relevantAuthorityId, string width, string height)
        {
            var chartModel = _metadataService.GetFileStatsByScheme(_userId, schemeType, relevantAuthorityId, startYear);
            return chartModel.ComplexDataset != null ? BuildBarChart(chartModel.ComplexDataset, chartModel.Labels, "File Record", "FileStats", width, height) : null;
        }

        private ChartViewModel BuildBarChart(IEnumerable<ComplexDataset> ds, string[] labels, string name, string canvasId, string width, string height)
        {
            var vm = new ChartViewModel { BarChart = new BarChart(), ChartType = ChartType.BarChart, ChartName = name, CanvasId = canvasId, CanvasWidth = width, CanvasHeight = height, Labels = labels };
            vm.BarChart.ComplexData.Labels.AddRange(labels);
            vm.BarChart.ChartConfiguration.ShowTooltips = true;
            vm.BarChart.ComplexData.Datasets.AddRange(ds);

            return vm;
        }
        private ChartViewModel BuildPieChart(IEnumerable<SimpleData> ds, string name, string canvasId)
        {
            var vm = new ChartViewModel { PieChart = new PieChart(), ChartType = ChartType.PieChart, ChartName = name, CanvasId = canvasId };
            vm.PieChart.Data.AddRange(ds);
            vm.PieChart.ChartConfiguration.TooltipFontSize = 9;
            vm.PieChart.ChartConfiguration.LegendTemplate =
                "<ul class=\\\"<%=name.toLowerCase()%>-legend\\\"><% for (var i=0; i<segments.length; i++){%><li><span style=\\\"background-color:<%=segments[i].fillColor%>\\\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>";

            return vm;
        }
    }
}