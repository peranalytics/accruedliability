﻿using System;
using Microsoft.AspNet.Identity;
using Services.Enum;
using Services.Services;

namespace Databank.Helpers
{
    public class SecurityHelper
    {
        public static bool CanViewDetails()
        {
            var accountService = new AccountService();
            var role = accountService.GetUserRole(Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.GetUserId()));

            return (role == Role.PensionAdministrator) || (role == Role.Administrator) || (role == Role.PensionViewer);
        }

        public static bool IsAdmin()
        {
            var accountService = new AccountService();
            var role = accountService.GetUserRole(Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.GetUserId()));

            return (role == Role.PensionAdministrator) || (role == Role.Administrator);
        }
    }
}