﻿namespace Databank.Models
{
    public class ErrorViewModel
    {
        public int ErrorCode { get; set; }

        public string Description { get; set; }
    }
}