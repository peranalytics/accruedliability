﻿using System.Collections.Generic;

namespace Databank.Models
{
    public class AccountViewModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}