﻿using System.Collections.Generic;
using Services.Enum;
using Services.Models;

namespace Databank.Models
{
    public class SchemeDetailsViewModel
    {
        public FileHistoryViewModel FileHistoryViewModel { get; set; }

        public SchemeType SchemeType { get; set; }

        public ChartViewModel LargeChartViewModel { get; set; }

        public Dictionary<string, string> SchemeUsers { get; set; }

        public IEnumerable<FileLog> FileLogs { get; set; }
    }
}