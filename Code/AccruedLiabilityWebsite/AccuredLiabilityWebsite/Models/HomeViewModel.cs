﻿using System.Collections.Generic;
using Services.Models;

namespace Databank.Models
{
    public class HomeViewModel
    {
        public User User { get; set; }

        public string UserName { get; set; }

        public IEnumerable<Metadata> RecentFiles { get; set; }

    }
}