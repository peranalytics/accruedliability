﻿using System.Collections.Generic;
using Chart.Mvc.ComplexChart;
using Chart.Mvc.SimpleChart;
using Services.Enum;

namespace Databank.Models
{
    public class ChartViewModel
    {
        public ChartViewModel()
        {
            CanvasWidth = "300";
            CanvasHeight = "300";
        }

        public string ChartName { get; set; }

        public string CanvasId { get; set; }

        public BarChart BarChart { get; set; }

        public PieChart PieChart { get; set; }

        public IEnumerable<ComplexDataset> ChartDataset { get; set; }

        public IEnumerable<SimpleData> SimpleChartDataset { get; set; }

        public IEnumerable<string> Labels { get; set; }

        public ChartType ChartType { get; set; }

        public string CanvasWidth { get; set; }

        public string CanvasHeight { get; set; }
    }
}