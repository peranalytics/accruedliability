﻿using System.Collections.Generic;
using System.Web.Mvc;
using Services.Enum;

namespace Databank.Models
{
    public class AdminViewModel
    {
        public List<SelectListItem> Users { get; set; }

        public List<SelectListItem> SecurityGroups { get; set; }
    }
}