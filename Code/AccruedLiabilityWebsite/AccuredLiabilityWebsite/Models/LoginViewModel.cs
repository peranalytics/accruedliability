﻿using System.ComponentModel.DataAnnotations;

namespace Databank.Models
{
    public class LoginViewModel 
    {
        public int UserId { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Enabled { get; set; }

        public bool ResetRequired { get; set; }

        public string ReturnUrl { get; set; }

        public string PhoneNumber { get; set; }
    }
}