﻿using System.Collections.Generic;
using Services.Enum;
using Services.Models;

namespace Databank.Models
{
    public class FileManagerViewModel
    {
        public List<Metadata> MetadataList { get; set; }

        public SchemeType SchemeType { get; set; }

        public RelevantAuthority RelevantAuthority { get; set; }

        public int Year { get; set; }
    }
}