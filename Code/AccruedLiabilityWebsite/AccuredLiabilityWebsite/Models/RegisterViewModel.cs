﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Databank.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Please enter an email address")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}", ErrorMessage = "Password must be between 6 and 20 characters, contain one number, one uppercase and lowercase letter")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm the password")]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please enter a first name")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter a last name")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Range(0, Int32.MaxValue, ErrorMessage = "Please select Permission Level")]
        public int SelectedPermission { get; set; }

        public List<SelectListItem> PermissionList { get; set; }
    }
}