﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Databank.Startup))]
namespace Databank
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}