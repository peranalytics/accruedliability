﻿using System;
using log4net;

namespace Databank
{
    public class Log
    {
        private static readonly Log Instance = new Log();
        protected ILog Logger; 

        private Log()
        {
            Logger = LogManager.GetLogger("WebsiteLogger");
        }

        public static void Error(string message, Exception ex)
        {
            Instance.Logger.Error(message, ex);
        }

        public static void Info(string message)
        {
            Instance.Logger.Info(message);
        }
    }
}