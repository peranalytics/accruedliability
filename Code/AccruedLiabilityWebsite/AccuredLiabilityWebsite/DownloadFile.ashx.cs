﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Services.Services;

namespace Databank
{
    public class DownloadFile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.IsAuthenticated)
            {
                Download(context);
            }

            RedirectToHome(context);
        }

        public bool IsReusable => false;

        private static void Download(HttpContext context)
        {
            int.TryParse(context.Request.QueryString["v"], out var versionId);
            int.TryParse(context.Request.QueryString["raId"], out var relevantAuthorityId);

            if (versionId == 0 || relevantAuthorityId == 0)
            {
                return;
            }

            var currentUserId = Convert.ToInt32(HttpContext.Current.User.Identity.GetUserId());
            var metadataService = new MetadataService();
            var metadata = metadataService.GetMetadataByVersionId(currentUserId, versionId);

            if (metadata == null)
            {
                return;
            }

            metadataService.LogFileDownload(versionId, currentUserId);

            var filePath = ConfigurationManager.AppSettings["DownloadFilePath"];
            var fullFilePath = $"{filePath}{metadata.LocalFileName}";

            context.Response.Clear();
            context.Response.ContentType = "application/octet-stream";
            context.Response.AddHeader("Content-Disposition", $"attachment; filename={metadata.FileName}");
            context.Response.WriteFile(fullFilePath);
            context.Response.End();
        }

        private static void RedirectToHome(HttpContext context)
        {
            var urlHelper = new UrlHelper(context.Request.RequestContext);
            var homeUrl = urlHelper.Action("Index", "Home", null);
            context.Response.Redirect(homeUrl);
        }
    }

}