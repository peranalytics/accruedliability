﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Databank.Helpers;
using Microsoft.AspNet.Identity;
using Services.Services;

namespace Databank.Webforms
{
    public partial class Reports : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

                if (Request.IsAuthenticated)
                {
                    var userName = Context.User.Identity.GetUserName();
                    _userNameLink.Text = userName;

                    _homeLink.NavigateUrl = urlHelper.Action("Index", "Home", null);

                    _administrationLink.NavigateUrl = urlHelper.Action("Index", "Admin", null);
                    _fileHistoryLink.NavigateUrl = urlHelper.Action("Index", "FileHistory", null);

                    _contactUs.NavigateUrl = urlHelper.Action("Index", "ContactUs", null);
                    _contactUsFooter.NavigateUrl = urlHelper.Action("Index", "ContactUs", null);
                    _userNameLink.NavigateUrl = urlHelper.Action("ResetPassword", "Account", null);

                    _adminPanel.Visible = SecurityHelper.IsAdmin();
                    _detailViewerPanel.Visible = SecurityHelper.CanViewDetails();

                    var userId = Convert.ToInt32(HttpContext.Current.User.Identity.GetUserId());
                    var accountService = new AccountService();
                    var reports = accountService.GetUserReports(userId);

                    var sb = new StringBuilder();

                    foreach (var report in reports)
                    {
                        sb.Append("<li class=\"sub-menu-item\">");
                        sb.Append($"<a href = \"{report.ReportUrl}\" > {report.ReportName} </ a >");
                        sb.Append("</li>");
                    }

                    litReportMenu.Text = sb.ToString();
                }
                else
                {
                    var loginUrl = urlHelper.Action("Login", "Account", null);
                    Response.Redirect(loginUrl);
                }

                _copyrightlit.Text = $"Copyright {DateTime.Now.Year }. All rights reserved, Gov.ie, Accrued Liability - Pensions Management";
            }
        }
    }
}