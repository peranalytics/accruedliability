﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Databank.Extensions;
using Services.Enum;
using Services.Interfaces;
using Services.Models;
using Services.Services;

namespace Databank.Webforms
{
    public partial class ReportViewer : Page
    {
        private readonly IReportingDataAccess _reportingDataAccess = new ReportingDataAcccess();
        private int _userId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;

            var reportId = Convert.ToInt32(Request.QueryString["id"]);

            _userId = Convert.ToInt32(Context.User.Identity.GetUserId());

            var accountService = new AccountService();
            var report = accountService.GetUserReports(_userId).FirstOrDefault(x=> x.Id == reportId);

            if (report != null)
            {
                LoadReport(report);
                LogReportView(_userId, reportId);
                litReportName.Text = report.ReportName;
                litReportDescription.Text = report.ReportDescription;
            }

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            _homeLink.NavigateUrl = urlHelper.Action("Index", "Admin", null);
        }

        private void LoadReport(ReportItem report)
        {
            if (!Request.IsAuthenticated)
            {
                var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                var loginUrl = urlHelper.Action("Login", "Account", null);
                Response.Redirect(loginUrl ?? throw new InvalidOperationException());
            }

            var configService = new ConfigurationService();
            var reportConfig = configService.GetReportConfiguration();
            var reportPath = report.ReportPath;
            var userId = Context.User.Identity.GetUserId();

            reportViewer.Reset();
            reportViewer.ShowToolBar = true;
            reportViewer.ServerReport.ReportServerCredentials = new CustomReportCredentials(reportConfig.UserName,
                reportConfig.Password, reportConfig.Domain);
            reportViewer.ServerReport.ReportServerUrl = new Uri(reportConfig.ReportServerUrl);
            reportViewer.ServerReport.ReportPath = reportPath;

            var userParam = new Microsoft.Reporting.WebForms.ReportParameter("UserId", userId);
            var paramList = new List<Microsoft.Reporting.WebForms.ReportParameter> { userParam };

            reportViewer.ServerReport.SetParameters(paramList);
            reportViewer.ServerReport.Refresh();

            var reportName = report.ReportName;

            litHeading.Text = $"{reportName}";
        }

        private static void LogReportView(int userId, int reportId)
        {
            var configurationService = new ConfigurationService();
            configurationService.LogReportView(userId, reportId);
        }
    }
}

