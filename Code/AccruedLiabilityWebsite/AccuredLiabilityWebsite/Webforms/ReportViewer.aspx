﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webforms/Reports.Master" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="Databank.Webforms.ReportViewer" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>
        <asp:Literal runat="server" ID="pageTitle"></asp:Literal></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="admin-content-page-header">
        <h1>
            <asp:Literal runat="server" ID="litHeading"></asp:Literal></h1>

    </div>
    <div class="clearboth"></div>

    <div class="main-breadcrumb">
        <asp:HyperLink runat="server" ID="_homeLink">Home</asp:HyperLink><span class="breadcrumb-separator">/</span>
        Reports
        <span class="breadcrumb-separator">/</span>
        <asp:Literal runat="server" ID="litReportName"></asp:Literal>
    </div>
    <div class="clearboth"></div>
    <span class="admin-homepage">
        <asp:Literal runat="server" ID="litReportDescription"></asp:Literal></span>
    <div class="reportviewer">
        <rsweb:ReportViewer ID="reportViewer" runat="server" ProcessingMode="Remote" CssClass="viewer" Height="2000px" Width="100%">
            <ServerReport ReportPath="/SPS/SummaryMemberStatus" ReportServerUrl="http://td-daldev1/Reports_SSRS" />
        </rsweb:ReportViewer>
    </div>
</asp:Content>
