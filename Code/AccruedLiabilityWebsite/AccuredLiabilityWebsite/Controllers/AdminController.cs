﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using Databank.Extensions;
using Databank.Helpers;
using Databank.Models;
using Microsoft.AspNet.Identity;
using PagedList;
using Services.Enum;
using Services.Models;
using Services.Services;

namespace Databank.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly AccountService _accountService;
        private readonly AdminService _adminService;

        private readonly string _currentUserName;
        private readonly int _currentUserId;


        public AdminController()
        {
            _accountService = new AccountService();
            _currentUserName = System.Web.HttpContext.Current.User.Identity.GetUserName();
            _adminService = new AdminService();

            _currentUserId = Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.GetUserId());
        }

        // GET: Admin
        public ActionResult Index()
        {
            if (SecurityHelper.IsAdmin())
            {
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            if (!SecurityHelper.IsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            var registerViewModel = new RegisterViewModel {PermissionList = BuildPermissionList()};
            return View(registerViewModel);
        }

        public ActionResult RelevantAuthorities(int? page, string currentFilter, string searchString)
        {
            return GetRelevantAuthoritiesViewModel(page, currentFilter, searchString);
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel registerViewModel)
        {
            ViewBag.ShowSuccess = "false";
            if (!ModelState.IsValid)
            {
                registerViewModel.PermissionList = BuildPermissionList();
                return View(registerViewModel);
            }

            var result = _accountService.RegisterUser(registerViewModel.Email, registerViewModel.Password, registerViewModel.FirstName, registerViewModel.LastName, registerViewModel.SelectedPermission);

            if (result.Success)
            {
                ViewBag.ShowSuccess = "true";
                ViewBag.SuccessMessage = $"{registerViewModel.Email} - created successfully";

                registerViewModel = new RegisterViewModel {PermissionList = BuildPermissionList()};
                return View(registerViewModel);
            }

            foreach (var modelStateValue in ModelState.Values)
            {
                modelStateValue.Errors.Clear();
            }

            ModelState.AddModelError("", result.Message);
            registerViewModel.PermissionList = BuildPermissionList();

            return View(registerViewModel);
        }

        private ActionResult GetRelevantAuthoritiesViewModel(int? page, string currentFilter, string searchString)
        {
            var pageSize = ConfigurationManager.AppSettings["PageSize"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) : 20;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var raList = _adminService.GetRelevantAuthorityList(page ?? 1, pageSize, searchString);

            var pagenumber = (page ?? 1) - 1;
            var totalCount = raList.TotalCount;

            var pagedRas = new StaticPagedList<RelevantAuthority>(raList.RelevantAuthorities, pagenumber + 1, pageSize, totalCount);
            return View("RelevantAuthorities", pagedRas);
        }

        private Role GetUserRole()
        {
            var userId = Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.GetUserId());
            var user = _accountService.GetUserDetails(userId);

            return user.Role;
        }

        private List<SelectListItem> BuildPermissionList()
        {
            var role = _accountService.GetUserRole(_currentUserId);
            var list = new List<SelectListItem>();

            if (role == Role.Administrator)
            {
                var adminSi = new SelectListItem
                {
                    Text = Role.PensionAdministrator.GetDescription(),
                    Value = ((int)Role.PensionAdministrator).ToString()
                };
                list.Add(adminSi);
            }

            var si = new SelectListItem
            {
                Text = Role.PensionViewer.GetDescription(),
                Value = ((int)Role.PensionViewer).ToString()
            };
            list.Add(si);

            si = new SelectListItem
            {
                Text = Role.PolicyViewer.GetDescription(),
                Value = ((int)Role.PolicyViewer).ToString()
            };
            list.Add(si);

            list.Insert(0, new SelectListItem { Text = "Please Select", Value = "-1" });
            return list;
        }
    }
}