﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Databank.Extensions;
using Databank.Helpers;
using Databank.Models;
using PagedList;
using Services.Enum;
using Services.Models;
using Services.Services;

namespace Databank.Controllers
{
    [Authorize]
    public class FileHistoryController : Controller
    {
        private readonly AdminService _adminService;
        private readonly MetadataService _metdataService;

        public FileHistoryController()
        {
            _adminService = new AdminService();
            _metdataService = new MetadataService();
        }

        public ActionResult Index()
        {
            if (!SecurityHelper.CanViewDetails())
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public ActionResult Payments(int? page, string currentFilter, string searchString)
        {
            if (!SecurityHelper.CanViewDetails())
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Scheme = SchemeType.Payments;
            return GetFileHistoryListViewModel(page, SchemeType.Payments, currentFilter, searchString);
        }

        public ActionResult Actives(int? page, string currentFilter, string searchString)
        {
            if (!SecurityHelper.CanViewDetails())
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Scheme = SchemeType.Actives;
            return GetFileHistoryListViewModel(page, SchemeType.Actives, currentFilter, searchString);
        }

        public ActionResult Deferreds(int? page, string currentFilter, string searchString)
        {
            if (!SecurityHelper.CanViewDetails())
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Scheme = SchemeType.Deferreds;
            return GetFileHistoryListViewModel(page, SchemeType.Deferreds, currentFilter, searchString);
        }

        public ActionResult SchemeDetails(int relevantAuthorityId, SchemeType schemeType)
        {
            if (!SecurityHelper.CanViewDetails())
            {
                return RedirectToAction("Index", "Home");
            }

            var schemeDetailsViewModel = GetSchemeDetailsViewModel(relevantAuthorityId, schemeType);
            return View(schemeDetailsViewModel);
        }

        public ActionResult FileManager(int relevantAuthorityId, SchemeType schemeType, int year)
        {
            if (!SecurityHelper.CanViewDetails())
            {
                return RedirectToAction("Index", "Home");
            }

            var relevantAuthority = _adminService.GetRelevantAuthorityById(relevantAuthorityId);

            var fileManagerVm = new FileManagerViewModel
            {
                RelevantAuthority = relevantAuthority,
                Year = year,
                SchemeType = schemeType
            };

            var metadataList = _metdataService.GetMetadataByRelevantAuthority(relevantAuthorityId);
            var enumerable = metadataList as IList<Metadata> ?? metadataList.ToList();

            if (!enumerable.Any())
            {
                return View(fileManagerVm);
            }

            var filteredBySchemeList = enumerable.Where(x => x.SchemeType == schemeType.GetDescription()).ToList();
            var filteredByYearList = filteredBySchemeList.Where(x => x.Year == year).ToList();

            fileManagerVm.MetadataList = filteredByYearList;

            return View(fileManagerVm);
        }

        private ActionResult GetFileHistoryListViewModel(int? page, SchemeType schemeType, string currentFilter, string searchString)
        {
            var pageSize = ConfigurationManager.AppSettings["PageSize"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) : 20;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var raList = _adminService.GetRelevantAuthorityList(page ?? 1, pageSize, searchString);

            if (raList.TotalCount == 1)
            {
                var ra = raList.RelevantAuthorities.FirstOrDefault();

                if (ra != null)
                {
                    return RedirectToAction("SchemeDetails", "FileHistory", new { relevantAuthorityId = ra.RelevantAuthorityId, schemeType });
                    return null;
                }
            }

            var metadataList = _metdataService.GetMetadata().ToList();

            var fileHistoryVmList = raList.RelevantAuthorities.Select(ra => new FileHistoryViewModel
            {
                Metadata = metadataList.Where(x => x.RelevantAuthority.RelevantAuthorityId == ra.RelevantAuthorityId && x.SchemeType == schemeType.ToString()),
                RelevantAuthority = ra
            });

            var pagenumber = (page ?? 1) - 1;
            var totalCount = raList.TotalCount;

            var pagedRas = new StaticPagedList<FileHistoryViewModel>(fileHistoryVmList, pagenumber + 1, pageSize, totalCount);
            return View("Scheme", pagedRas);
        }

        private SchemeDetailsViewModel GetSchemeDetailsViewModel(int relevantAuthorityId, SchemeType schemeType)
        {
            var schemeDetailsViewModel = new SchemeDetailsViewModel
            {
                FileHistoryViewModel = GetFileHistoryViewModel(relevantAuthorityId, schemeType),
                SchemeType = schemeType,
                LargeChartViewModel = GetChartViewModel(relevantAuthorityId, schemeType),
                FileLogs = GetFileLogs(relevantAuthorityId, schemeType),
            };

            return schemeDetailsViewModel;
        }

        private FileHistoryViewModel GetFileHistoryViewModel(int relevantAuthorityId, SchemeType schemeType)
        {
            return new FileHistoryViewModel
            {
                Metadata = _metdataService.GetMetadata(relevantAuthorityId: relevantAuthorityId).Where(x => x.SchemeType == schemeType.GetDescription()).ToList(),
                RelevantAuthority = _adminService.GetRelevantAuthorityById(relevantAuthorityId)
            };
        }

        private static ChartViewModel GetChartViewModel(int relevantAuthorityId, SchemeType schemeType)
        {
            var schemeYearDefaultCount = ConfigurationManager.AppSettings["SchemeYearDefaultCount"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["SchemeYearDefaultCount"]) : 8;
            var startYear = (DateTime.Now.Year - schemeYearDefaultCount) < 2013 ? 2013 : (DateTime.Now.Year - schemeYearDefaultCount); // 2013 year SPS started - Will show last x years always

            var chartHelper = new ChartHelper();
            return chartHelper.BuildFileStatsBySchemeChartViewModel(startYear, schemeType, relevantAuthorityId, "100%", "400");
        }

        private IEnumerable<FileLog> GetFileLogs(int relevantAuthorityId, SchemeType schemeType)
        {
            var filelogs = _metdataService.ReadLogFileDownload(relevantAuthorityId).ToList();

            if (filelogs.Any())
            {
                var filteredFileLogs = filelogs.Where(x => x.SchemeType == schemeType);
                var fileLogs = filteredFileLogs as IList<FileLog> ?? filteredFileLogs.ToList();

                if (fileLogs.Any())
                {
                    return fileLogs;
                }
            }

            return new List<FileLog>();
        }

    }
}