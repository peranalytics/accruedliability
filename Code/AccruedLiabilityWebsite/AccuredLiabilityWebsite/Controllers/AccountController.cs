﻿using System;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Databank.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Services.Services;

namespace Databank.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly AccountService _accountService;
        public AccountController()
        {
            _accountService = new AccountService();
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            ViewBag.ShowError = "false";

            if (!ModelState.IsValid)
            {
                // do some error handling
                return View(model);
            }

            var result = _accountService.CheckResetPassword(System.Web.HttpContext.Current.User.Identity.GetUserName(), model.CurrentPassword);

            if (!result.Success)
            {
                foreach (var modelStateValue in ModelState.Values)
                {
                    modelStateValue.Errors.Clear();
                }

                ViewBag.ShowError = "true";

                ModelState.AddModelError("", result.Message);
                return View(model);
            }

            var updateResult = _accountService.UpdatePassword(System.Web.HttpContext.Current.User.Identity.GetUserId(), model.Password);

            if (updateResult.Success)
            {
                ViewBag.ShowSuccess = "true";
                ViewBag.SuccessMessage = "Your password was updated successfully";
            }

            foreach (var modelStateValue in ModelState.Values)
            {
                modelStateValue.Errors.Clear();
            }

            ViewBag.ShowError = "true";

            ModelState.AddModelError("", updateResult.Message);
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            var model = new LoginViewModel
            {
                ReturnUrl = string.IsNullOrEmpty(returnUrl) ? Url.Action("Index", "Home") : returnUrl
            };

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            TempData["SuccessfulLogout"] = false;

            if (!ModelState.IsValid)
            {
                // do some error handling
                return View(model);
            }

            var result = _accountService.LoginUser(model.Email, model.Password);

            if (result.Success)
            {
                var ident = new ClaimsIdentity(
                    new[]
                    {
                        new Claim(ClaimTypes.Name, model.Email),
                        new Claim(ClaimTypes.NameIdentifier, result.UserId.ToString()),

                    },
                    DefaultAuthenticationTypes.ApplicationCookie);

                HttpContext.GetOwinContext().Authentication.SignIn(
                    new AuthenticationProperties
                    {
                        IsPersistent = false,
                        ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(30)
                    }, ident);

                if (model.ReturnUrl == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                return Redirect(model.ReturnUrl); // auth succeed 
            }

            foreach (var modelStateValue in ModelState.Values)
            {
                modelStateValue.Errors.Clear();
            }

            ModelState.AddModelError("", result.Message);
            return View(model);
        }

        public ActionResult Logout(string returnUrl)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;
    }
}