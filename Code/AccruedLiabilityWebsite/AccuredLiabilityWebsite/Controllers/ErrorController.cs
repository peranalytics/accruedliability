﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Databank.Models;

namespace Databank.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index(Exception exception, int errorType)
        {
            Response.TrySkipIisCustomErrors = true;
            var statusCode = GetStatusCode(exception);

            Response.StatusCode = statusCode;

            var errorVm = new ErrorViewModel
            {
                ErrorCode = statusCode,
                Description = exception.Message
            };

            return statusCode == 404 ? NotFound(errorVm) : View(errorVm);
        }

        public ViewResult NotFound(ErrorViewModel errorVm)
        {
            Response.StatusCode = 404;  //you may want to set this to 200
            return View("NotFound", errorVm);
        }

        private static int GetStatusCode(Exception exception)
        {
            return exception is HttpException httpException ? httpException.GetHttpCode() : (int)HttpStatusCode.InternalServerError;
        }
    }
}