﻿using System.Web.Mvc;
using Databank.Models;
using Services.Services;

namespace Databank.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly MetadataService _metadataService;

        public HomeController()
        {
            _metadataService = new MetadataService();
        }
        public ActionResult Index()
        {
            var vm = new HomeViewModel {RecentFiles = _metadataService.GetRecentFileHistory()};
            return View(vm);
        }
    }
}