﻿using System;
using DataTransform.Services;
using DataTransform.Services.Services;

namespace DataTransform
{
    public class Program
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args)
        {
            try
            {
                Log.Info("Start");

                var importService = new ImportService(Log);
                importService.StartDataload();

                Log.Info("Finished");
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal Error Occured", ex);

                if (GlobalSettings.SendEmail)
                {
                    var mailerService = new MailerService();
                    mailerService.SendErrorEmail(ex);
                }
            }
        }
    }
}
