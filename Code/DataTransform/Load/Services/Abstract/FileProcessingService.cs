﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataTransform.DataAccessLayer;
using DataTransform.Services.Services;
using Models.Enums;
using Models.Models;

namespace DataTransform.Services.Abstract
{
    public class FileProcessingService
    {
        private readonly SchemeDataAccess _schemeDataAccess;
        private readonly SchemeService _schemeService;
        private readonly FileHistoryService _fileHistoryService;

        private readonly log4net.ILog _log;

        public FileProcessingService(log4net.ILog log)
        {
            _schemeService = new SchemeService();
            _schemeDataAccess = new SchemeDataAccess();
            _fileHistoryService = new FileHistoryService();
            _log = log;
        }

        public List<KeyViolations> ProcessFile(FileHistory metadata)
        {
            _log.Info("ProcessFile - File Processing Service");
            var fileData = GetFileAsDataSet(metadata);
            _log.Info("ProcessFile - GetFileAsDataSet Succeeeded");

            if (fileData == null || fileData.Tables.Count == 0 || fileData.Tables[0].Rows.Count == 0)
            {
                _log.Info($"ProcessFile -{metadata.SchemeType.ToString()} - Data Empty - All data for relevant authority Id:  {metadata.RelevantAuthority} and Year: {metadata.Year} will be deleted");
            }

            var schemeListModel = GetSchemeListModel();

            List<KeyViolations> keyViolations;

            switch (metadata.SchemeType)
            {
                case "Actives":
                    _log.Info("ProcessFile - Scheme Type Actives");
                    keyViolations = _schemeService.LoadActivesData(fileData, metadata, schemeListModel, _log);
                    break;
                case "Deferreds":
                    _log.Info("ProcessFile - Scheme Type Deferreds");
                    keyViolations= _schemeService.LoadDeferredData(fileData, metadata, schemeListModel,_log);
                    break;
                case "Payments":
                    _log.Info("ProcessFile - Scheme Type Payments");
                    keyViolations= _schemeService.LoadPaymentsData(fileData, metadata, schemeListModel, _log);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(metadata.SchemeType), metadata.SchemeType, null);
            }

            if (!keyViolations.Any())
            {
                // MARK AS TRANSFORMED 
                return keyViolations;
            }
            else
            {
                // SEND ALERT
                return keyViolations;
            }
        }

        public DataSet GetFileAsDataSet(FileHistory metaData)
        {
            DataSet ds = null;
            var filePath = $"{GlobalSettings.ArchiveFileFolder}{metaData.FileName}";

            var csvFileProcessingService = new CsvFileProcessingService();
            _log.Info($"Processing {filePath} as File Type CSV");

            ds = csvFileProcessingService.GetFileAsDataSet(filePath);
            return ds;
        }

        private SchemeListModel GetSchemeListModel()
        {
            var schemeListModel = new SchemeListModel
            {
                BeneficiaryTypes = _schemeDataAccess.GetAllBeneficiaryTypes(),
                BenefitTypes = _schemeDataAccess.GetAllBenefitTypes(),
                CivilStatus = _schemeDataAccess.GetAllCivilStatus(),
                Genders = _schemeDataAccess.GetAllGender(),
                PensionBasis = _schemeDataAccess.GetAllPensionBasis(),
                PensionScheme = _schemeDataAccess.GetAllPensionScheme(),
                PrsiClass = _schemeDataAccess.GetAllPrsiClass()
            };

            return schemeListModel;
        }

        public string ExcludeNonValidChars(string fileName)
        {
            fileName = fileName.ToLower();
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"[^a-z0-9\s-]", ""); // Remove all non valid chars          
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s+", " ").Trim(); // convert multiple spaces into one space  
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s", "-"); // //Replace spaces by dashes
            return fileName;
        }
    }
}
