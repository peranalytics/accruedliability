﻿using System.Data;
using System.IO;

namespace DataTransform.Services.Interfaces
{
    public interface IFileProcessingService
    {
        DataSet GetFileAsDataSet(string path);

        void ProcessFile(FileInfo file, log4net.ILog log);
    }
}
