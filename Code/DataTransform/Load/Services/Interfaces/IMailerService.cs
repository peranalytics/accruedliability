﻿using System;

namespace DataTransform.Services.Interfaces
{
    public interface IMailerService
    {
        void SendSuccessEmail(bool warnMissingKeys = false);

        void SendErrorEmail(Exception exception);
    }
}
