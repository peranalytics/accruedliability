﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataTransform.DataAccessLayer;
using Models.Enums;
using Models.Models;

namespace DataTransform.Services.Services
{
    public class SchemeService
    {
        private readonly SchemeDataAccess _schemeDataAccess;
        private readonly FileHistoryService _fileHistoryService;

        public SchemeService()
        {
            _schemeDataAccess = new SchemeDataAccess();
            _fileHistoryService = new FileHistoryService();
        }

        public List<KeyViolations> LoadActivesData(DataSet ds, FileHistory fileHistory, SchemeListModel schemeList, log4net.ILog log)
        {
            var dt = ds.Tables[0];
            var resultKeyViolations = ValidateActives(dt, fileHistory, schemeList);

            if (resultKeyViolations.Any())
            {
                foreach (var resultKeyViolation in resultKeyViolations)
                {
                    var message =
                        $"Missing Key in file: VersionId - {resultKeyViolation.VersionId}, RelevantAuthorityId - {resultKeyViolation.RelevantAuthorityId}, FileName - {resultKeyViolation.FileName}, FieldName - {resultKeyViolation.FieldName}, FieldValue - {resultKeyViolation.FieldValue}";
                    log.Debug(message);
                }

                return resultKeyViolations;
            }

            _fileHistoryService.TruncateStaging();
            var activeDt = GetActivesTable(fileHistory, dt);
            _schemeDataAccess.LoadActivesDataToStaging(activeDt);
            _schemeDataAccess.LoadActives();
            _fileHistoryService.UpdateFileTransformHistory(fileHistory.VersionId, FileHistoryTransformStatus.Transformed);

            return new List<KeyViolations>();
        }

        public List<KeyViolations> LoadDeferredData(DataSet ds, FileHistory fileHistory, SchemeListModel schemeList, log4net.ILog log)
        {
            var dt = ds.Tables[0];
            var resultKeyViolations = ValidateDeferreds(dt, fileHistory, schemeList);

            if (resultKeyViolations.Any())
            {
                foreach (var resultKeyViolation in resultKeyViolations)
                {
                    var message =
                        $"Missing Key in file: VersionId - {resultKeyViolation.VersionId}, RelevantAuthorityId - {resultKeyViolation.RelevantAuthorityId}, FileName - {resultKeyViolation.FileName}, FieldName - {resultKeyViolation.FieldName}, FieldValue - {resultKeyViolation.FieldValue}";
                    log.Debug(message);
                }

                return resultKeyViolations;
            }

            _fileHistoryService.TruncateStaging();
            var deferredDt = GetDeferredsTable(fileHistory, dt);

            _schemeDataAccess.LoadDeferredsDataToStaging(deferredDt);
            _schemeDataAccess.LoadDeferreds();

            _fileHistoryService.UpdateFileTransformHistory(fileHistory.VersionId, FileHistoryTransformStatus.Transformed);

            return new List<KeyViolations>();
        }

        public List<KeyViolations> LoadPaymentsData(DataSet ds, FileHistory fileHistory, SchemeListModel schemeList, log4net.ILog log)
        {
            var dt = ds.Tables[0];
            var resultKeyViolations = ValidatePayments(dt, fileHistory, schemeList);

            if (resultKeyViolations.Any())
            {
                foreach (var resultKeyViolation in resultKeyViolations)
                {
                    var message =
                        $"Missing Key in file: VersionId - {resultKeyViolation.VersionId}, RelevantAuthorityId - {resultKeyViolation.RelevantAuthorityId}, FileName - {resultKeyViolation.FileName}, FieldName - {resultKeyViolation.FieldName}, FieldValue - {resultKeyViolation.FieldValue}";
                    log.Debug(message);
                }

                return resultKeyViolations;
            }

            _fileHistoryService.TruncateStaging();
            var activeDt = GetPaymentsTable(fileHistory, dt);

            _schemeDataAccess.LoadPaymentsDataToStaging(activeDt);
            _schemeDataAccess.LoadPayments();

            _fileHistoryService.UpdateFileTransformHistory(fileHistory.VersionId, FileHistoryTransformStatus.Transformed);

            return new List<KeyViolations>();
        }

        private List<KeyViolations> ValidateActives(DataTable dt, FileHistory fileHistory, SchemeListModel schemeList)
        {
            var keyViolations = new List<KeyViolations>();

            var violations = ValidateGenders(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            violations = ValidateCivilStatus(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            violations = ValidatePensionScheme(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            return keyViolations;
        }

        private List<KeyViolations> ValidateDeferreds(DataTable dt, FileHistory fileHistory, SchemeListModel schemeList)
        {
            var keyViolations = new List<KeyViolations>();

            var violations = ValidateGenders(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            violations = ValidateCivilStatus(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            violations = ValidatePensionScheme(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            return keyViolations;
        }

        private List<KeyViolations> ValidatePayments(DataTable dt, FileHistory fileHistory, SchemeListModel schemeList)
        {
            var keyViolations = new List<KeyViolations>();

            var violations = ValidateGenders(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            violations = ValidateCivilStatus(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            violations = ValidatePensionScheme(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            violations = ValidateBeneficiary(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            violations = ValidatePensionBasis(dt, fileHistory, schemeList);
            keyViolations.AddRange(violations);

            return keyViolations;
        }

        private List<KeyViolations> ValidateGenders(DataTable dt, FileHistory fileHistory, SchemeListModel schemeList)
        {
            var keyViolations = new List<KeyViolations>();

            var colName = "Gender";

            if (dt.Columns.Contains("Gender "))
            {
                colName = "Gender ";
            }

            var gendersFromFile = (from DataRow dRow in dt.Rows select new { genderValue = dRow[colName].ToString().ToLower().Trim() }).Distinct().ToList();
            var missingGenders = gendersFromFile.Where(p => schemeList.Genders.All(p2 => p2.ToLower() != p.genderValue.ToLower().Trim())).ToList();

            if (missingGenders.Any())
            {
                keyViolations.AddRange(missingGenders.Select(missingGender => new KeyViolations
                {
                    VersionId = fileHistory.VersionId,
                    RelevantAuthorityId = fileHistory.RelevantAuthority,
                    FileName = fileHistory.FileName,
                    FieldName = "Gender",
                    FieldValue = missingGender.genderValue
                }));
            }

            return keyViolations;
        }

        private List<KeyViolations> ValidateCivilStatus(DataTable dt, FileHistory fileHistory, SchemeListModel schemeList)
        {
            var keyViolations = new List<KeyViolations>();
            var colName = "CivilStatus";

            if (dt.Columns.Contains("CivilStatus "))
            {
                colName = "CivilStatus ";
            }

            var civilStatusFromFile = (from DataRow dRow in dt.Rows select new { value = dRow[colName].ToString().ToLower() }).Distinct().ToList();
            var missingCivilStatus = civilStatusFromFile.Where(p => schemeList.CivilStatus.All(p2 => p2.ToLower() != p.value.ToLower().Trim())).ToList();

            if (missingCivilStatus.Any())
            {
                keyViolations.AddRange(missingCivilStatus.Select(missingValue => new KeyViolations
                {
                    VersionId = fileHistory.VersionId,
                    RelevantAuthorityId = fileHistory.RelevantAuthority,
                    FileName = fileHistory.FileName,
                    FieldName = "CivilStatus",
                    FieldValue = missingValue.value
                }));
            }

            return keyViolations;
        }

        private List<KeyViolations> ValidatePensionScheme(DataTable dt, FileHistory fileHistory, SchemeListModel schemeList)
        {
            var keyViolations = new List<KeyViolations>();

            var colName = "PensionScheme";

            if (dt.Columns.Contains("PensionScheme "))
            {
                colName = "PensionScheme ";
            }

            var pensionSchemeFromFile = (from DataRow dRow in dt.Rows select new { value = dRow[colName].ToString().ToLower() }).Distinct().ToList();
            var missingPensionSchemes = pensionSchemeFromFile.Where(p => schemeList.PensionScheme.All(p2 => p2.Trim().ToLower() != p.value.ToLower().Trim())).ToList();

            if (missingPensionSchemes.Any())
            {
                keyViolations.AddRange(missingPensionSchemes.Select(missingValue => new KeyViolations
                {
                    VersionId = fileHistory.VersionId,
                    RelevantAuthorityId = fileHistory.RelevantAuthority,
                    FileName = fileHistory.FileName,
                    FieldName = "PensionScheme",
                    FieldValue = missingValue.value
                }));
            }

            return keyViolations;
        }

        private List<KeyViolations> ValidateBeneficiary(DataTable dt, FileHistory fileHistory, SchemeListModel schemeList)
        {
            var keyViolations = new List<KeyViolations>();

            var typeOfBeneficiariesFromFile = (from DataRow dRow in dt.Rows select new { beneficiaryValue = dRow["TypeOfBeneficiary"].ToString().ToLower().Trim() }).Distinct().ToList();
            var missingBeneficiaries = typeOfBeneficiariesFromFile.Where(p => schemeList.BeneficiaryTypes.All(p2 => p2.Trim().ToLower() != p.beneficiaryValue.ToLower().Trim())).ToList();

            if (missingBeneficiaries.Any())
            {
                keyViolations.AddRange(missingBeneficiaries.Select(missingBenefiary => new KeyViolations
                {
                    VersionId = fileHistory.VersionId,
                    RelevantAuthorityId = fileHistory.RelevantAuthority,
                    FileName = fileHistory.FileName,
                    FieldName = "TypeOfBeneficiary",
                    FieldValue = missingBenefiary.beneficiaryValue
                }));
            }

            return keyViolations;
        }

        private List<KeyViolations> ValidatePensionBasis(DataTable dt, FileHistory fileHistory, SchemeListModel schemeList)
        {
            var keyViolations = new List<KeyViolations>();

            var typeOfPensionBasisFromFile = (from DataRow dRow in dt.Rows select new { pensionBasisValue = dRow["BasisPensionCommenced"].ToString().ToLower().Trim() }).Distinct().ToList();
            var missingPensionsBasis = typeOfPensionBasisFromFile.Where(p => schemeList.PensionBasis.All(p2 => p2.ToLower() != p.pensionBasisValue.ToLower().Trim())).ToList();

            if (missingPensionsBasis.Any())
            {
                keyViolations.AddRange(missingPensionsBasis.Select(missingPensionBasis => new KeyViolations
                {
                    VersionId = fileHistory.VersionId,
                    RelevantAuthorityId = fileHistory.RelevantAuthority,
                    FileName = fileHistory.FileName,
                    FieldName = "BasisPensionCommenced",
                    FieldValue = missingPensionBasis.pensionBasisValue
                }));
            }

            return keyViolations;
        }

        private static DataTable GetActivesTable(FileHistory fileHistory, DataTable dt)
        {
            var activesDt = new DataTable();

            activesDt.Columns.Add("VersionId", typeof(int));
            activesDt.Columns.Add("RelevantAuthorityId", typeof(int));
            activesDt.Columns.Add("Year", typeof(int));
            activesDt.Columns.Add("PPSN", typeof(string));

            activesDt.Columns.Add("DateOfBirth", typeof(DateTime));
            activesDt.Columns.Add("Gender", typeof(string));
            activesDt.Columns.Add("CivilStatus", typeof(string));
            activesDt.Columns.Add("PensionScheme", typeof(string));
            activesDt.Columns.Add("DateOfEntryIntoScheme", typeof(DateTime));
            activesDt.Columns.Add("MinimumNormalRetirementAge", typeof(int));
            activesDt.Columns.Add("PrsiClass", typeof(string));
            activesDt.Columns.Add("AnnualPensionablePayFTE", typeof(float));
            activesDt.Columns.Add("LengthOfService", typeof(float));
            activesDt.Columns.Add("FTE", typeof(float));
            activesDt.Columns.Add("Grade", typeof(string));
            activesDt.Columns.Add("ScalePoint", typeof(string));
            activesDt.Columns.Add("IncrementDate", typeof(DateTime));
            activesDt.Columns.Add("MandatoryRetirementAge", typeof(int));
            activesDt.Columns.Add("AnnualBasicPensionablePayFTE", typeof(float));

            foreach (DataRow row in dt.Rows)
            {
                var newRow = activesDt.NewRow();

                newRow["VersionId"] = fileHistory.VersionId;
                newRow["RelevantAuthorityId"] = fileHistory.RelevantAuthority;
                newRow["Year"] = fileHistory.Year;
                newRow["PPSN"] = row[0].ToString();

                var dateOfBirth = row[1].ToString();
                newRow["DateOfBirth"] = dateOfBirth == string.Empty ? DBNull.Value : (object)Convert.ToDateTime(dateOfBirth);

                newRow["Gender"] = row[2].ToString().Trim();
                newRow["CivilStatus"] = row[3].ToString().Trim();
                newRow["PensionScheme"] = row[4].ToString().Trim();
                newRow["DateOfEntryIntoScheme"] = Convert.ToDateTime(row[5].ToString());
                newRow["MinimumNormalRetirementAge"] = Convert.ToInt32(row[6].ToString());
                newRow["PrsiClass"] = row[7].ToString().Trim();
                newRow["AnnualPensionablePayFTE"] = Math.Round(Convert.ToDecimal(row[8].ToString()), 2);
                newRow["LengthOfService"] = Math.Round(Convert.ToDecimal(row[9].ToString()), 2);
                newRow["FTE"] = Math.Round(Convert.ToDecimal(row[10].ToString()), 2);
                newRow["Grade"] = row[11].ToString().Trim();
                newRow["ScalePoint"] = row[12].ToString().Trim();

                var incrementDate = row[13].ToString();
                newRow["IncrementDate"] = incrementDate == string.Empty ? DBNull.Value : (object)Convert.ToDateTime(row[13].ToString());

                newRow["MandatoryRetirementAge"] = Convert.ToInt32(row[14].ToString());
                newRow["AnnualBasicPensionablePayFTE"] = Math.Round(Convert.ToDecimal(row[15].ToString()), 2);

                activesDt.Rows.Add(newRow);
            }

            return activesDt;
        }

        private static DataTable GetDeferredsTable(FileHistory fileHistory, DataTable dt)
        {
            var deferredsDt = new DataTable();

            deferredsDt.Columns.Add("VersionId", typeof(int));
            deferredsDt.Columns.Add("RelevantAuthorityId", typeof(int));
            deferredsDt.Columns.Add("Year", typeof(int));
            deferredsDt.Columns.Add("PPSN", typeof(string));

            deferredsDt.Columns.Add("DateOfBirth", typeof(DateTime));
            deferredsDt.Columns.Add("Gender", typeof(string));
            deferredsDt.Columns.Add("CivilStatus", typeof(string));
            deferredsDt.Columns.Add("PensionScheme", typeof(string));

            deferredsDt.Columns.Add("PreservedPensionAge", typeof(int));
            deferredsDt.Columns.Add("PrsiClass", typeof(string));
            deferredsDt.Columns.Add("FTE", typeof(float));
            deferredsDt.Columns.Add("PensionableEmploymentStartDate", typeof(DateTime));
            deferredsDt.Columns.Add("PensionableEmploymentLeaveDate", typeof(DateTime));
            deferredsDt.Columns.Add("PensionableRemunerationFuturePensionBasedOn", typeof(float));
            deferredsDt.Columns.Add("PensionableServiceFuturePensionBasedOn", typeof(float));

            foreach (DataRow row in dt.Rows)
            {
                var newRow = deferredsDt.NewRow();

                newRow["VersionId"] = fileHistory.VersionId;
                newRow["RelevantAuthorityId"] = fileHistory.RelevantAuthority;
                newRow["Year"] = fileHistory.Year;
                newRow["PPSN"] = row[0].ToString();

                var dateOfBirth = row[1].ToString();
                newRow["DateOfBirth"] = dateOfBirth == string.Empty ? DBNull.Value : (object)Convert.ToDateTime(dateOfBirth);

                newRow["Gender"] = row[2].ToString().Trim();
                newRow["CivilStatus"] = row[3].ToString().Trim();
                newRow["PensionScheme"] = row[4].ToString().Trim();
                newRow["PreservedPensionAge"] = Convert.ToInt32(row[5].ToString());
                newRow["PrsiClass"] = row[6].ToString().Trim();
                newRow["FTE"] = Math.Round(Convert.ToDecimal(row[7].ToString()), 2);
                newRow["PensionableEmploymentStartDate"] = Convert.ToDateTime(row[8].ToString());
                newRow["PensionableEmploymentLeaveDate"] = Convert.ToDateTime(row[9].ToString());
                newRow["PensionableRemunerationFuturePensionBasedOn"] = Math.Round(Convert.ToDecimal(row[10].ToString()), 2);

                var pensionableServiceFuturePensionBasedOn = row[11].ToString();
                newRow["PensionableServiceFuturePensionBasedOn"] = pensionableServiceFuturePensionBasedOn == string.Empty ? DBNull.Value : (object)Math.Round(Convert.ToDecimal(pensionableServiceFuturePensionBasedOn));

                deferredsDt.Rows.Add(newRow);
            }

            return deferredsDt;
        }

        private static DataTable GetPaymentsTable(FileHistory fileHistory, DataTable dt)
        {
            var paymentsDt = new DataTable();

            paymentsDt.Columns.Add("VersionId", typeof(int));
            paymentsDt.Columns.Add("RelevantAuthorityId", typeof(int));
            paymentsDt.Columns.Add("Year", typeof(int));
            paymentsDt.Columns.Add("PPSN", typeof(string));

            paymentsDt.Columns.Add("DateOfBirth", typeof(DateTime));
            paymentsDt.Columns.Add("Gender", typeof(string));
            paymentsDt.Columns.Add("CivilStatus", typeof(string));
            paymentsDt.Columns.Add("PensionScheme", typeof(string));

            paymentsDt.Columns.Add("TypeOfBeneficiary", typeof(string));
            paymentsDt.Columns.Add("PensionCommencementDate", typeof(DateTime));
            paymentsDt.Columns.Add("AnnualPensionValue", typeof(float));
            paymentsDt.Columns.Add("AnnualSupplementaryPension", typeof(float));
            paymentsDt.Columns.Add("BasisPensionCommenced", typeof(string));
            paymentsDt.Columns.Add("PensionableServiceYears", typeof(float));
            paymentsDt.Columns.Add("BasicPayValuePensionableRemuneration", typeof(float));
            paymentsDt.Columns.Add("PensionableAllowancesPensionableRemuneration", typeof(float));

            foreach (DataRow row in dt.Rows)
            {
                var newRow = paymentsDt.NewRow();

                newRow["VersionId"] = fileHistory.VersionId;
                newRow["RelevantAuthorityId"] = fileHistory.RelevantAuthority;
                newRow["Year"] = fileHistory.Year;
                newRow["PPSN"] = row[0].ToString();

                var dateOfBirth = row[1].ToString();
                newRow["DateOfBirth"] = dateOfBirth == string.Empty ? DBNull.Value : (object)Convert.ToDateTime(dateOfBirth);

                newRow["Gender"] = row[2].ToString().Trim();
                newRow["CivilStatus"] = row[3].ToString().Trim();
                newRow["PensionScheme"] = row[4].ToString().Trim();

                newRow["TypeOfBeneficiary"] = row[5].ToString().Trim();
                newRow["PensionCommencementDate"] = Convert.ToDateTime(row[6].ToString());
                newRow["AnnualPensionValue"] = Math.Round(Convert.ToDecimal(row[7].ToString()), 2);
                newRow["AnnualSupplementaryPension"] = Math.Round(Convert.ToDecimal(row[8].ToString()), 2);

                newRow["BasisPensionCommenced"] = row[9].ToString().Trim();

                var pensionableServiceYears = row[10].ToString();
                newRow["PensionableServiceYears"] = pensionableServiceYears == string.Empty ? DBNull.Value : (object)Math.Round(Convert.ToDecimal(pensionableServiceYears));

                var basicPayValuePensionableRemuneration = row[11].ToString();
                newRow["BasicPayValuePensionableRemuneration"] = basicPayValuePensionableRemuneration == string.Empty ? DBNull.Value : (object)Math.Round(Convert.ToDecimal(basicPayValuePensionableRemuneration));

                var pensionableAllowancesPensionableRemuneration = row[12].ToString();
                newRow["PensionableAllowancesPensionableRemuneration"] = pensionableAllowancesPensionableRemuneration == string.Empty ? DBNull.Value : (object)Math.Round(Convert.ToDecimal(pensionableAllowancesPensionableRemuneration));

                paymentsDt.Rows.Add(newRow);
            }

            return paymentsDt;
        }
    }
}
