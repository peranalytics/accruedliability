﻿using System.Collections.Generic;
using DataTransform.DataAccessLayer;
using Models.Enums;
using Models.Models;

namespace DataTransform.Services.Services
{
    public class FileHistoryService
    {
        private readonly FileHistoryAccessService _fileHistoryDataAccess;


        public FileHistoryService()
        {
            _fileHistoryDataAccess = new FileHistoryAccessService();
        }

        public List<FileHistory> GetFilesToTransform()
        {
            return _fileHistoryDataAccess.GetFilesToTransform();
        }

        public void UpdateFileTransformHistory(int versionId, FileHistoryTransformStatus status)
        {
            _fileHistoryDataAccess.UpdateFileTransformHistory(versionId, (int)status);
        }

        public void TruncateStaging()
        {
            _fileHistoryDataAccess.TruncateStaging();
        }
    }
}
