﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataTransform.DataAccessLayer;
using DataTransform.Services.Abstract;
using DataTransform.Services.Interfaces;
using Models.Models;

namespace DataTransform.Services.Services
{
    public class ImportService
    {
        private readonly log4net.ILog _log;
        private readonly FileProcessingService _fileProcessingService;
        private readonly DataAccessService _dataAccessService;
        private readonly FileHistoryService _metadataService;
        private readonly IMailerService _mailerService;

        private string _fileFolder;

        public ImportService(log4net.ILog log)
        {
            _dataAccessService = new DataAccessService();
            _mailerService = new MailerService();
            _metadataService = new FileHistoryService();
            _fileProcessingService = new FileProcessingService(log);
            _log = log;
        }

        public void StartDataload()
        {
            LoadData();
            _log.Info("Finish");
            ArchiveLogFiles();
        }

        public void LoadData()
        {
            _fileFolder = GlobalSettings.FileFolder;
            _log.Info($"FolderPath: {_fileFolder}");

            var result = LoadFiles();

            if (result.Any())
            {
                _log.Info("Missing Keys - Please Review the log");
            }

            // LOAD FLAT FILES HERE
            _log.Info("Loading Flat files for reporting - Start");
            LoadFlatFiles();
            _log.Info("Loading Flat files for reporting - Finish");

            if (GlobalSettings.SendEmail)
            {
                var warnMissingKets = result.Any();
                _mailerService.SendSuccessEmail(warnMissingKets);
            }
        }

        private List<KeyViolations> LoadFiles()
        {
            var activeKeyViolations = new List<KeyViolations>();
            var deferredKeyViolations = new List<KeyViolations>();
            var paymentKeyViolations = new List<KeyViolations>();

            _log.Info("Get files to Transform - Start");

            var metas = _metadataService.GetFilesToTransform();

            _log.Info("Get files to Transform - Finish");
            _log.Info($"{metas.Count} files to Transform");


            var activeMetas = metas.Where(x => x.SchemeType == "Actives");
            var activesMetasEnumerable = activeMetas as IList<FileHistory> ?? activeMetas.ToList();

            if (activesMetasEnumerable.Any())
            {
                _log.Info("Active Scheme Start");
                activeKeyViolations = LoadFiles(activesMetasEnumerable);
                _log.Info("Active Scheme Finish");
            }

            var deferredMetas = metas.Where(x => x.SchemeType == "Deferreds");
            var deferredMetasEnumerable = deferredMetas as IList<FileHistory> ?? deferredMetas.ToList();

            if (deferredMetasEnumerable.Any())
            {
                _log.Info("Deferred Scheme Start");
                deferredKeyViolations = LoadFiles(deferredMetasEnumerable);
                _log.Info("Deferred Scheme Finish");
            }

            var paymentMetas = metas.Where(x => x.SchemeType == "Payments");
            var paymentMetasEnumerable = paymentMetas as IList<FileHistory> ?? paymentMetas.ToList();

            if (paymentMetasEnumerable.Any())
            {
                _log.Info("Payments Scheme Start");
                paymentKeyViolations = LoadFiles(paymentMetasEnumerable);
                _log.Info("Payments Scheme Finish");
            }

            var allKeyViolations = new List<KeyViolations>();
            allKeyViolations.AddRange(activeKeyViolations);
            allKeyViolations.AddRange(deferredKeyViolations);
            allKeyViolations.AddRange(paymentKeyViolations);


            return allKeyViolations;
        }

        public List<KeyViolations> LoadFiles(IEnumerable<FileHistory> metas)
        {
            var keyViolations = new List<KeyViolations>();
            foreach (var metaData in metas)
            {
                var result = _fileProcessingService.ProcessFile(metaData);
                keyViolations.AddRange(result);
            }

            return keyViolations;
        }

        private void LoadFlatFiles()
        {
            _dataAccessService.PopulateActivesFlatFile();
            _dataAccessService.PopulateDeferredFlatFiles();
            _dataAccessService.PopulatePaymentsFlatFiles();
        }

        private static void ArchiveLogFiles()
        {
            var info = new DirectoryInfo(GlobalSettings.LogFileFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            var deleteDate = DateTime.Now.AddDays(-GlobalSettings.LogDaysKeep);

            if (!files.Any())
            {
                return;
            }

            var filesToDelete = files.Where(x => x.CreationTime < deleteDate);

            foreach (var logFile in filesToDelete)
            {
                logFile.Delete();
            }
        }
    }
}



