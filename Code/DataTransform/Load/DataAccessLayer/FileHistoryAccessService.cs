﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Models.Models;

namespace DataTransform.DataAccessLayer
{
    public class FileHistoryAccessService
    {
        public void TruncateStaging()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[TruncateStaging]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateFileTransformHistory(int versionId, int status)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileTransformUpdate]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@VersionId", SqlDbType.Int).Value = versionId;
                    command.Parameters.Add("@StatusId", SqlDbType.Int).Value = status;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<FileHistory> GetFilesToTransform()
        {
            var fileHistories = new List<FileHistory>();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileTransformGet]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            if (ds.Tables.Count < 1 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                return fileHistories;
            }

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var fileHistory = new FileHistory
                {
                    VersionId = Convert.ToInt32(row["VersionId"].ToString()),
                    RelevantAuthority = Convert.ToInt32(row["RelevantAUthorityId"].ToString()),
                    Year = Convert.ToInt32(row["Year"].ToString()),
                    OriginalFileName = row["FileName"].ToString(),
                    FileName = row["LocalFileName"].ToString(),
                    SchemeType = row["Scheme"].ToString()
                };


                fileHistories.Add(fileHistory);
            }

            return fileHistories;
        }
    }
}
