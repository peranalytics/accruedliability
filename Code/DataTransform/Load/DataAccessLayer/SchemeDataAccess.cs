﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DataTransform.DataAccessLayer
{
    public class SchemeDataAccess
    {
        public List<string> GetAllBeneficiaryTypes()
        {
            var values = new List<string>();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[BeneficiaryTypeGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            if (ds.Tables.Count < 1 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                return values;
            }

            values.AddRange(from DataRow dataRow in ds.Tables[0].Rows select dataRow["MappingBeneficiaryType"].ToString());

            return values;
        }

        public List<string> GetAllBenefitTypes()
        {
            var values = new List<string>();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[BenefitTypeGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            if (ds.Tables.Count < 1 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                return values;
            }

            values.AddRange(from DataRow dataRow in ds.Tables[0].Rows select dataRow["MappingBenefitType"].ToString());

            return values;
        }

        public List<string> GetAllGender()
        {
            var values = new List<string>();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[GenderGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            if (ds.Tables.Count < 1 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                return values;
            }

            values.AddRange(from DataRow dataRow in ds.Tables[0].Rows select dataRow["MappingGender"].ToString());

            return values;
        }

        public List<string> GetAllCivilStatus()
        {
            var values = new List<string>();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[CivilStatusGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            if (ds.Tables.Count < 1 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                return values;
            }

            values.AddRange(from DataRow dataRow in ds.Tables[0].Rows select dataRow["MappingCivilStatus"].ToString());

            return values;
        }

        public List<string> GetAllPensionBasis()
        {
            var values = new List<string>();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[PensionBasisGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            if (ds.Tables.Count < 1 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                return values;
            }

            values.AddRange(from DataRow dataRow in ds.Tables[0].Rows select dataRow["MappingPensionBasis"].ToString());

            return values;
        }

        public List<string> GetAllPensionScheme()
        {
            var values = new List<string>();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[PensionSchemeGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            if (ds.Tables.Count < 1 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                return values;
            }

            values.AddRange(from DataRow dataRow in ds.Tables[0].Rows select dataRow["MappingPensionScheme"].ToString());

            return values;
        }

        public List<string> GetAllPrsiClass()
        {
            var values = new List<string>();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[PrsiClassGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            if (ds.Tables.Count < 1 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
            {
                return values;
            }

            values.AddRange(from DataRow dataRow in ds.Tables[0].Rows select dataRow["MappingPrsiClass"].ToString());

            return values;
        }

        public void LoadActivesDataToStaging(DataTable table)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "[dbo].[StagingActives]";
                    con.Open();
                    sqlBulkCopy.WriteToServer(table);

                    con.Close();
                }
            }
        }

        public void LoadActives()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[LoadActives]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void LoadDeferredsDataToStaging(DataTable table)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "[dbo].[StagingDeferreds]";
                    con.Open();
                    sqlBulkCopy.WriteToServer(table);

                    con.Close();
                }
            }
        }

        public void LoadDeferreds()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[LoadDeferreds]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void LoadPaymentsDataToStaging(DataTable table)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "[dbo].[StagingPayments]";
                    con.Open();
                    sqlBulkCopy.WriteToServer(table);

                    con.Close();
                }
            }
        }

        public void LoadPayments()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[LoadPayments]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
