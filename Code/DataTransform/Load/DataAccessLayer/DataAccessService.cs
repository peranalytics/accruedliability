﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DataTransform.DataAccessLayer
{
    public class DataAccessService
    {
        public void PopulateActivesFlatFile()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[PopulateActivesFlatFiles]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.CommandTimeout = 300;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void PopulateDeferredFlatFiles()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[PopulateDeferredFlatFiles]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.CommandTimeout = 300;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void PopulatePaymentsFlatFiles()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[PopulatePaymentsFlatFiles]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.CommandTimeout = 300;
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
