﻿using System;
using System.Data;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Services;
using Models.Enums;
using Models.Models;
using Services.Interfaces;

namespace Services.Services
{
    public class MembersService : IMemberService
    {
        private readonly IMemberDataAccessService _memberDataAccessService;

        public MembersService()
        {
            _memberDataAccessService = new MemberDataAccessService();
        }
        public ExecuteResult SaveMember(int versionId)
        {
            return _memberDataAccessService.SaveMember(versionId);
        }

        public void LoadMembershipToStaging(DataTable dt)
        {
            _memberDataAccessService.LoadMembershipToStaging(dt);
        }

        public void TruncateStagingTables()
        {
            _memberDataAccessService.TruncateStagingTables();
        }

        public void CreateMemberFlatFile()
        {
            _memberDataAccessService.CreateMemberFlatfile();
        }

        public DataTable GetMemberDataTable(DataSet ds, int versionId)
        {
            var dt = CreateTargetTable();

            if (ds == null || ds.Tables.Count <= 0)
            {
                return null;
            }

            var fileData = ds.Tables[0];


            var nonEmptyRows = fileData.AsEnumerable().Where(row => row[0].ToString() != string.Empty);

            var count = nonEmptyRows.Count();

            foreach (DataRow row in nonEmptyRows)
            {
                var newRow = dt.NewRow();

                newRow["VersionId"] = versionId;

                var origYearString = row[0].ToString() == "" ? null : row[0].ToString();
                var origYearDecimal = origYearString != null ? Convert.ToDecimal(origYearString) : 0;
                var origYearInt = origYearDecimal != 0 ? (int?)(origYearDecimal) : 0;

                newRow["Year"] = origYearInt;
                newRow["PPSN"] = row[1];
                newRow["Dob"] = row[2];
                newRow["FirstName"] = row[3];
                newRow["Surname"] = row[4];
                newRow["MaidenName"] = row[5];
                newRow["Title"] = null;
                newRow["Gender"] = row[6];
                newRow["CivilStatus"] = row[7];
                newRow["PensionAdjustmentOrder"] = row[8];
                newRow["DateOfDeath"] = row[9].ToString() == "" ? DBNull.Value : row[9];
                newRow["HomeAddress1"] = null;
                newRow["HomeAddress2"] = null;
                newRow["HomeAddress3"] = null;
                newRow["HomeAddress4"] = null;
                newRow["HomeAddress5"] = null;
                newRow["Eircode"] = null;

                var raNumberString = row[10].ToString() == "" ? null : row[10].ToString();
                var raNumberDecimal = raNumberString != null ? Convert.ToDecimal(raNumberString) : 0;
                var raNumberInt = raNumberDecimal != 0 ? (int?)(raNumberDecimal) : 0;

                newRow["RaNumber"] = raNumberInt;
                newRow["RaName"] = row[11];
                newRow["PayrollNumber"] = row[12];
                newRow["AdditionalPayrollNumber"] = row[13];
                newRow["PRSIClass"] = row[14];
                newRow["PensionableEmploymentStartDate"] = row[15];
                newRow["PensionableEmploymentLeaveDate"] = row[16].ToString() == "" ? DBNull.Value : row[16];
                newRow["MemberStatus"] = row[17];
                newRow["NormalRetirementDate"] = row[18].ToString() == "" ? DBNull.Value : row[18];
                newRow["MembershipCategory"] = row[19];
                newRow["EmployeeContributionsPaid"] = row[20].ToString() == "" ? DBNull.Value : row[20]; ;
                newRow["TotalGrossPensionableRemunerationPaid"] = row[21].ToString() == "" ? DBNull.Value : row[21];
                newRow["PensionReferableAmount"] = row[22].ToString() == "" ? DBNull.Value : row[22];
                newRow["LumpSumReferableAmount"] = row[23].ToString() == "" ? DBNull.Value : row[23];
                newRow["GrossRefundAmountPaid"] = row[24].ToString() == "" ? DBNull.Value : row[24];
                newRow["RefundDate"] = row[25].ToString() == "" ? DBNull.Value : row[25];
                newRow["RefundMembershipCategory"] = row[26];
                newRow["RefundPensionableEmploymentStartDate"] = row[27].ToString() == "" ? DBNull.Value : row[27];
                newRow["RefundPensionableEmploymentLeaveDate"] = row[28].ToString() == "" ? DBNull.Value : row[28];
                newRow["RefundPensionReferableAmount"] = row[29].ToString() == "" ? DBNull.Value : row[29];
                newRow["RefundLumpsumReferableAmount"] = row[30].ToString() == "" ? DBNull.Value : row[30];

                var origRaNumberString  = row[31].ToString() == "" ? null : row[31].ToString();
                var origRaNumberDecimal = origRaNumberString != null ? Convert.ToDecimal(origRaNumberString) : 0;
                var origRaNumberInt = origRaNumberDecimal != 0 ? (int ?)(origRaNumberDecimal) : 0;

                newRow["RROriginalRaNumber"] = origRaNumberInt == 0 ? (object)DBNull.Value : origRaNumberInt;
                newRow["RROriginalRaName"] = row[32];
                newRow["RRFinalDateOfPayment"] = row[33].ToString() == "" ? DBNull.Value : row[33];
                newRow["RRPensionableEmploymentStartDate"] = row[34].ToString() == "" ? DBNull.Value : row[34];
                newRow["RRPensionableEmploymentLeaveDate"] = row[35].ToString() == "" ? DBNull.Value : row[35];
                newRow["RRGrossContributeAmount"] = row[36].ToString() == "" ? DBNull.Value : row[36];
                newRow["RRCompoundInterestmount"] = row[37].ToString() == "" ? DBNull.Value : row[37];
                newRow["RRMembershipCategory"] = row[38];
                newRow["RRPensionReferableAmount"] = row[39].ToString() == "" ? DBNull.Value : row[39];
                newRow["RRLumpsumReferableAmount"] = row[40].ToString() == "" ? DBNull.Value : row[40];

                dt.Rows.Add(newRow);
            }

            return dt;
        }

        private static DataTable CreateTargetTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("VersionId", typeof(int));
            dt.Columns.Add("Year", typeof(int));
            dt.Columns.Add("PPSN", typeof(string));
            dt.Columns.Add("Dob", typeof(DateTime));
            dt.Columns.Add("FirstName", typeof(string));
            dt.Columns.Add("Surname", typeof(string));
            dt.Columns.Add("MaidenName", typeof(string));
            dt.Columns.Add("Title", typeof(string));
            dt.Columns.Add("Gender", typeof(string));
            dt.Columns.Add("CivilStatus", typeof(string));
            dt.Columns.Add("PensionAdjustmentOrder", typeof(string));
            dt.Columns.Add("DateOfDeath", typeof(DateTime));
            dt.Columns.Add("HomeAddress1", typeof(string));
            dt.Columns.Add("HomeAddress2", typeof(string));
            dt.Columns.Add("HomeAddress3", typeof(string));
            dt.Columns.Add("HomeAddress4", typeof(string));
            dt.Columns.Add("HomeAddress5", typeof(string));
            dt.Columns.Add("Eircode", typeof(string));
            dt.Columns.Add("RaNumber", typeof(int));
            dt.Columns.Add("RaName", typeof(string));
            dt.Columns.Add("PayrollNumber", typeof(string));
            dt.Columns.Add("AdditionalPayrollNumber", typeof(string));
            dt.Columns.Add("PRSIClass", typeof(string));
            dt.Columns.Add("PensionableEmploymentStartDate", typeof(DateTime));
            dt.Columns.Add("PensionableEmploymentLeaveDate", typeof(DateTime)).AllowDBNull = true;
            dt.Columns.Add("MemberStatus", typeof(string));
            dt.Columns.Add("NormalRetirementDate", typeof(DateTime));
            dt.Columns.Add("MembershipCategory", typeof(string));
            dt.Columns.Add("EmployeeContributionsPaid", typeof(decimal));
            dt.Columns.Add("TotalGrossPensionableRemunerationPaid", typeof(decimal));
            dt.Columns.Add("PensionReferableAmount", typeof(decimal));
            dt.Columns.Add("LumpSumReferableAmount", typeof(decimal));
            dt.Columns.Add("GrossRefundAmountPaid", typeof(decimal));
            dt.Columns.Add("RefundDate", typeof(DateTime));
            dt.Columns.Add("RefundMembershipCategory", typeof(string));
            dt.Columns.Add("RefundPensionableEmploymentStartDate", typeof(DateTime));
            dt.Columns.Add("RefundPensionableEmploymentLeaveDate", typeof(DateTime));
            dt.Columns.Add("RefundPensionReferableAmount", typeof(decimal));
            dt.Columns.Add("RefundLumpsumReferableAmount", typeof(decimal));
            dt.Columns.Add("RROriginalRaNumber", typeof(int));
            dt.Columns.Add("RROriginalRaName", typeof(string));
            dt.Columns.Add("RRFinalDateOfPayment", typeof(DateTime));
            dt.Columns.Add("RRPensionableEmploymentStartDate", typeof(DateTime));
            dt.Columns.Add("RRPensionableEmploymentLeaveDate", typeof(DateTime));
            dt.Columns.Add("RRGrossContributeAmount", typeof(decimal));
            dt.Columns.Add("RRCompoundInterestmount", typeof(decimal));
            dt.Columns.Add("RRMembershipCategory", typeof(string));
            dt.Columns.Add("RRPensionReferableAmount", typeof(decimal));
            dt.Columns.Add("RRLumpsumReferableAmount", typeof(decimal));

            return dt;
        }
    }
}
