﻿using Newtonsoft.Json;

namespace Models.Models
{
    public class FileHistory
    {
        public int VersionId { get; set; }

        public int RelevantAuthority { get; set; }

        public string FileName { get; set; }

        public string OriginalFileName { get; set; }

        public int Year { get; set; }

        [JsonProperty(PropertyName = "num_records")]
        public int RecordCount { get; set; }

        public string SchemeType { get; set; }
    }
}
