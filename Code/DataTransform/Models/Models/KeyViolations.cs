﻿namespace Models.Models
{
    public class KeyViolations
    {
        public int VersionId { get; set; }

        public int RelevantAuthorityId { get; set; }

        public string FileName { get; set; }

        public bool Result { get; set; }
        public string FieldName { get; set; }

        public string FieldValue { get; set; }
    }
}
