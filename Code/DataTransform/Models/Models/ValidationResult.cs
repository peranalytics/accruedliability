﻿using System.Collections.Generic;

namespace Models.Models
{
    public class ValidationResult
    {
        public bool Result { get; set; }

        public List<KeyViolations> KeyViolations { get; set; }

    }
}
