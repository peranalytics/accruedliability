﻿using System.Collections.Generic;

namespace Models.Enums
{
    public class SchemeListModel
    {
        public List<string> BeneficiaryTypes { get; set; }

        public List<string> BenefitTypes { get; set; }

        public List<string> Genders { get; set; }

        public List<string> CivilStatus { get; set; }

        public List<string> PensionBasis { get; set; }

        public List<string> PensionScheme { get; set; }

        public List<string> PrsiClass { get; set; }
    }
}
