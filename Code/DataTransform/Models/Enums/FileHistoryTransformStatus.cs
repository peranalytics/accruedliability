﻿namespace Models.Enums
{
    public enum FileHistoryTransformStatus
    {
        NotTransformed = 0,
        Transformed = 1,
        TransformFailed = 2
    }
}
