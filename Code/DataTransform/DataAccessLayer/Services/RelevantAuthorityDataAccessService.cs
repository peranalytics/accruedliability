﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Services
{
    public class RelevantAuthorityDataAccessService : IRelevantAuthorityDataAccessService
    {
        public DataSet GetRelevantAuthories()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[RelevantAuthoriesGet]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ds;
        }

        public void UpdateRelavantAuthority(int id, double latitude, double longtitude)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[RelevantAuthoriesUpdate]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                    command.Parameters.Add("@Latitude", SqlDbType.Float).Value = latitude;
                    command.Parameters.Add("@Longitude", SqlDbType.Float).Value = longtitude;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
