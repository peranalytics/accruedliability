USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS Security.[GetUserReports]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Get all config values
-- =============================================
CREATE PROCEDURE Security.[GetUserReports]
	@UserId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT ur.ReportId, r.ReportName, r.reportDescription, r.ReportUrl
	FROM Security.UserReports ur
	INNER JOIN Report.Reports r
	ON r.ReportId = ur.ReportId
	WHERE ur.UserId = @UserId
END
GO

GRANT EXECUTE ON Security.[GetUserReports] TO accruedliabilityviewer 
