USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS GetAllSubSectors
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load deferreds to the table
-- =============================================
CREATE PROCEDURE GetAllSubSectors
	@SectorId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT SubSectorId, SubSectorName
	FROM SubSectors
	WHERE SectorId = @SectorId

END
GO

GRANT EXECUTE ON GetAllSubSectors TO AccruedLiabilityViewer 
