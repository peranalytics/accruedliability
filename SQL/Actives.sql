USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [Actives]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Actives]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[Actives](
	[VersionId] [int] NOT NULL,
	[RelevantAuthorityId] [int] NOT NULL,
	[YearId] [int] NOT NULL,
	[PPSN] VARCHAR(20) NOT NULL,
	[DateOfBirth] DATETIME NOT NULL,
	GenderId INT NOT NULL,
	CivilStatusId INT NOT NULL,
	PensionSchemeId INT NOT NULL,
	[DateOfEntryIntoScheme] DATETIME NOT NULL,
	MinimumNormalRetirementAge INT NOT NULL,
	PrsiClass NVARCHAR(100) NOT NULL,
	AnnualPensionablePayFTE DECIMAL(10,2) NOT NULL,
	LengthOfService DECIMAL(10,2) NOT NULL,
	FTE DECIMAL(10,2) NOT NULL,
	Grade NVARCHAR(250) NOT NULL,
	ScalePoint NVARCHAR(250) NOT NULL,
	IncrementDate DATETIME NULL,
	CreateDate DATETIME DEFAULT GETDATE() NOT NULL,
	ModifyDate DATETIME DEFAULT GETDATE() NOT NULL
	)
END
GO

