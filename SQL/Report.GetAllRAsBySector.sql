USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS Report.GetAllRAsBySector
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load deferreds to the table
-- =============================================
CREATE PROCEDURE Report.GetAllRAsBySector
	@UserId INT,
	@Sectors NVARCHAR(max) = '-1'

	AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT CAST(Value AS INT) AS SectorId
	INTO #SectorList FROM STRING_SPLIT(@Sectors, ',')  
	WHERE RTRIM(value) <> '';
	
	SELECT
	ras.SectorId,
	s.SectorName,
	ras.RelevantAuthorityId,
	ra.RelevantAuthorityName
	
FROM RelevantAuthoritySectors AS ras
INNER JOIN RelevantAuthority AS ra 
	ON ras.RelevantAuthorityId = ra.RelevantAuthorityId
INNER JOIN Sectors AS s
	ON ras.SectorId = s.SectorId
WHERE (@Sectors = '-1') OR  ras.SectorId IN (SELECT sub.SectorId FROm #SectorList sub)

ORDER BY ras.RelevantAuthorityId

	DROP TABLE #SectorList
	
	
END
GO



GRANT EXECUTE ON Report.GetAllRAsBySector TO AccruedLiabilityViewer 
