USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM PensionBasis

DELETE FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary early retirement'
DELETE FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary age grounds'
DELETE FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver'
DELETE FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retire. scheme nurses'



IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary early retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'voluntary early retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary age grounds')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'voluntary age grounds'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'ver'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retire. scheme nurses')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'early retire. scheme nurses'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'widows pension')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'widows pension'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'deceased')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'deceased'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'death in retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'death in retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'vr')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'vr'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'retirement liability unknown')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'retirement liability unknown'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'preserved pensioner')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'preserved pensioner'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'preserved')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'preserved'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'late retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'late retirement'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ex-gratia')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'ex-gratia'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'deferred pension')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'deferred pension'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 5, 'death of parent'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 5, 'court order'
END

