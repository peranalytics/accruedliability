USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS LoadActives
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load actives to the table
-- =============================================
CREATE PROCEDURE LoadActives
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE a
	FROM Actives a
	INNER JOIN StagingActives sa
	ON a.RelevantAuthorityId = sa.RelevantAuthorityId
	AND a.YearId = sa.YearId


	INSERT INTO [dbo].[Actives]
			   ([VersionId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[GenderId]
			   ,[CivilStatusId]
			   ,[PensionSchemeId]
			   ,[DateOfEntryIntoScheme]
			   ,[MinimumNormalRetirementAge]
			   ,[PrsiClass]
			   ,[AnnualPensionablePayFTE]
			   ,[LengthOfService]
			   ,[FTE]
			   ,[Grade]
			   ,[ScalePoint]
			   ,[IncrementDate]
			   ,[CreateDate]
			   ,[ModifyDate]
			   ,MandatoryRetirementAge
			   ,AnnualBasicPensionablePayFTE)
	SELECT [VersionId]
		  ,[RelevantAuthorityId]
		  ,[YearId]
		  ,[PPSN]
		  ,[DateOfBirth]
		  ,mg.Id
		  ,msc.Id
		  ,mps.Id
		  ,[DateOfEntryIntoScheme]
		  ,[MinimumNormalRetirementAge]
		  ,[PrsiClass]
		  ,[AnnualPensionablePayFTE]
		  ,[LengthOfService]
		  ,[FTE]
		  ,[Grade]
		  ,[ScalePoint]
		  ,[IncrementDate]
		, GETDATE()
		,GETDATE()
		,MandatoryRetirementAge
		,AnnualBasicPensionablePayFTE
  FROM [dbo].[StagingActives] sa
  INNER JOIN MappingGender mg
  ON mg.MappingGender = sa.Gender
  INNER JOIN MappingCivilStatus msc
  ON msc.MappingCivilStatus = sa.CivilStatus
  INNER JOIN MappingPensionScheme mps
  ON mps.MappingPensionScheme = sa.PensionScheme


END
GO

GRANT EXECUTE ON LoadActives TO AccruedLiabilityViewer 
