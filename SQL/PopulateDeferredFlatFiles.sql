USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS PopulateDeferredFlatFiles
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Populate flat files
-- =============================================
CREATE PROCEDURE PopulateDeferredFlatFiles
AS
BEGIN
	
	SET NOCOUNT ON;

	DELETE FROM [DeferredsFlatFile]

	INSERT INTO [dbo].[DeferredsFlatFile]
           ([RelevantAuthorityId]
           ,[RelevantAuthorityName]
           ,[SectorId]
           ,[SectorName]
           ,[SubsectorId]
           ,[SubsectorName]
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[Gender]
           ,[CivilStatus]
           ,[PensionScheme]
           ,[PreservedPensionAge]
           ,[PrsiClass]
           ,[FTE]
           ,[PensionableEmploymentStartDate]
           ,[PensionableEmploymentLeaveDate]
           ,[PensionableRemunerationFuturePensionBasedOn]
           ,[PensionableServiceFuturePensionBasedOn])
   SELECT 
		a.RelevantAuthorityId,
		ra.RelevantAuthorityName,
		secs.SectorId,
		secs.SectorName,
		subsecs.SubsectorId,
		subsecs.SubsectorName,
		a.YearId,
		PPSN,
		DateOfBirth,
		Gender,
		CivilStatus,
		PensionScheme,
		[PreservedPensionAge]
		,[PrsiClass]
		,[FTE]
		,[PensionableEmploymentStartDate]
		,[PensionableEmploymentLeaveDate]
		,[PensionableRemunerationFuturePensionBasedOn]
		,[PensionableServiceFuturePensionBasedOn]
	FROM Deferreds a WITH(NOLOCK)
	INNER JOIN RelevantAuthoritySectors ras WITH(NOLOCK)
	ON ras.RelevantAuthorityId = a.RelevantAuthorityId
	INNER JOIN RelevantAuthority ra WITH(NOLOCK)
	ON ra.RelevantAuthorityId = a.RelevantAuthorityId
	INNER JOIN Sectors secs WITH(NOLOCK)
	ON secs.SectorId = ras.SectorId
	INNER JOIN Subsectors subsecs WITH(NOLOCK)
	ON subsecs.SubsectorId = ras.SubsectorId
	AND subsecs.SectorId = ras.SectorId
	INNER JOIN Gender g WITH(NOLOCK) 
	ON a.GenderId = g.Id
	INNER JOIN CivilStatus cs WITH(NOLOCK) 
	ON a.CivilStatusId = cs.Id
	INNER JOIN PensionScheme p WITH(NOLOCK) 
	ON a.PensionSchemeId = p.Id

END
GO

GRANT EXECUTE ON PopulateDeferredFlatFiles TO AccruedLiabilityViewer 
