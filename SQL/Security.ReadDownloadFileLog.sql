USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS [Security].ReadDownloadFileLog
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Read the download file log
-- =============================================
CREATE PROCEDURE [Security].ReadDownloadFileLog
	@RelevantAuthorityId INT = -1
AS
BEGIN
	
	SET NOCOUNT ON;

	
	SELECT DISTINCT TOP 10 
		md.VersionId,
		users.UserName,
		FileName, 
		fl.CreateDate,
		Scheme,
		Year
	FROM [Security].[FileLog] fl
	INNER JOIN FileHistory md
	ON md.VersionId = fl.VersionId
	INNER JOIN [Security].[Users] users
	ON users.UserId = fl.UserId
	WHERE (@RelevantAuthorityId = -1 OR md.RelevantAUthorityId = @RelevantAuthorityId)
	ORDER BY fl.CreateDate DESC

END
GO

GRANT EXECUTE ON [Security].ReadDownloadFileLog TO [AccruedLiabilityviewer] 
