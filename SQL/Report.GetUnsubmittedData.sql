USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS Report.GetUnsubmittedData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load deferreds to the table
-- =============================================
CREATE PROCEDURE Report.GetUnsubmittedData
@Year INT
	
AS
BEGIN
	
	SET NOCOUNT ON;
	--Payments
	--Actives
	--Deferreds

	SELECT
		RelevantAuthorityId,
		RelevantAuthorityName, 'Payments' as Scheme
			
	FROM RelevantAuthority WHERE RelevantAuthorityId NOT IN 
	(SELECT fh.RelevantAuthorityId from FileHistory fh WHERE Scheme = 'Payments'  AND fh.Year = @Year)
	UNION ALL
		SELECT RelevantAuthorityId, RelevantAuthorityName, 'Actives' FROM RelevantAuthority WHERE RelevantAuthorityId NOT IN 
	(SELECT fh.RelevantAuthorityId from FileHistory fh WHERE Scheme = 'Actives' AND fh.Year = @Year)
	UNION ALL
		SELECT RelevantAuthorityId, RelevantAuthorityName, 'Deferreds' FROM RelevantAuthority WHERE RelevantAuthorityId NOT IN 
	(SELECT fh.RelevantAuthorityId from FileHistory fh WHERE Scheme = 'Deferreds' AND fh.Year = @Year)
	
END
GO

GRANT EXECUTE ON Report.GetUnsubmittedData TO AccruedLiabilityViewer 
