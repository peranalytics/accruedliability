USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[SchemeTypesGetAll] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tracy
-- Create date: 05/05/2020
-- Description:	Gets Scheme Types
-- =============================================
CREATE PROCEDURE [dbo].[SchemeTypesGetAll] 

AS
BEGIN
	SET NOCOUNT ON;

SELECT 
		[SchemeType]

	FROM SchemeType

END
GO

GRANT EXECUTE ON [dbo].[SchemeTypesGetAll] TO AccruedLiabilityViewer AS [dbo]
GO