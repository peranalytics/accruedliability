USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [Security].[RegisterUser]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Registers the user
-- =============================================
CREATE PROCEDURE [Security].[RegisterUser] 
	@HashedPassword NVARCHAR(MAX),
	@SecurityStamp NVARCHAR(MAX),
	@Username NVARCHAR(50),
	@FirstName NVARCHAR(50),
	@LastName NVARCHAR(50),
	@RoleId INT

AS

BEGIN
	SET NOCOUNT ON;

	DECLARE @UserId INT
	
	IF NOT EXISTS (SELECT 1 FROM [Security].[Users] WHERE Username = @Username)
	BEGIN
		INSERT INTO [Security].[Users]
           ([UserName]
           ,[PasswordHash]
           ,[SecurityStamp]
           ,[Enabled]
           ,[ResetRequired]
           ,[ModifyDate]
           ,[CreateDate]
           ,[Deleted]
           ,[FirstName]
           ,[LastName]
           ,[LastLogin]
           ,[FailedAttempts]
           ,[LastFailedAttempt]
           ,[PhoneNumber])
		SELECT 
			@Username, 
			@HashedPassword, 
			@SecurityStamp, 
			1, 
			0, 
			GETDATE(), 
			GETDATE(), 
			0, 
			@FirstName, 
			@LastName,
			GETDATE(),
			NULL,
			NULL,
			NULL

			SET @UserId = SCOPE_IDENTITY();

		INSERT INTO [Security].UserRoles
		SELECT @UserId, @RoleId, GETDATE()

		INSERT INTO [Security].UserReports
		SELECT @UserId, ReportId, GETDATE()
		FROM Report.Reports WHERE Deleted = 0

	END


END
GO

GRANT EXECUTE ON [Security].[RegisterUser] TO [accruedliabilityviewer] AS [dbo]
GO


