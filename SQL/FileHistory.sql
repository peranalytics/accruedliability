
USE [AccruedLiability]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileHistory](
	[VersionId] [int] IDENTITY(1,1) NOT NULL,
	[DataStrategyId] [int] NOT NULL,
	[RelevantAUthorityId] [int] NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[Year] [int] NOT NULL,
	[UploadedBy] [nvarchar](255) NOT NULL,
	[TransactionTypeId] [int] NOT NULL,
	[FileTypeId] [int] NOT NULL,
	[RecordCount] [int] NOT NULL,
	[Validated] [bit] NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[Scheme] [nvarchar](50) NULL,
	[Success] [bit] NULL,
	[EntryUpdateDate] [datetime] NULL,
	[LocalFileName] [nvarchar](255) NULL,
 CONSTRAINT [PK_FileHistory_VersionId] PRIMARY KEY CLUSTERED 
(
	[VersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF__FileHistory__Succes__3C1FE2D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FileHistory] ADD  DEFAULT ((0)) FOR [Success]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF__FileHistory__EntryU__40E497F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FileHistory] ADD  DEFAULT (getdate()) FOR [EntryUpdateDate]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileHistory_YearId]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileHistory]'))
ALTER TABLE [dbo].[FileHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileHistory_RelevantAuthorityId] FOREIGN KEY(RelevantAuthorityId)
REFERENCES [dbo].[RelevantAuthority] (RelevantAuthorityId)
GO


