
USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[CivilStatusGetAll] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 25/07/2019
-- Description:	Get the mapping lookup data
-- =============================================
CREATE PROCEDURE [dbo].[CivilStatusGetAll] 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, MappingCivilStatus FROM [dbo].MappingCivilStatus
END
GO

GRANT EXECUTE ON [dbo].[CivilStatusGetAll] TO budgetviewer AS [dbo]
GO


