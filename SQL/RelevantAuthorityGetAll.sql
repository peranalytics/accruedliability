USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[RelevantAuthorityGetAll]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Get the Relevant Authories
-- =============================================
CREATE PROCEDURE [dbo].[RelevantAuthorityGetAll] 
	@Page INT = 1,
	@PageSize INT = 10,
	@FilterBy NVARCHAR(100) = '',
	@TotalRaCount INT OUTPUT
AS

BEGIN
	SET @TotalRaCount = (SELECT COUNT([RelevantAuthorityId]) FROM RelevantAuthority WHERE RelevantAuthorityName LIKE '%' + @FilterBy + '%')

	;WITH PagedRas AS
	(
		SELECT 
		  [RelevantAuthorityId]
		  ,[RelevantAuthorityName]
		  ,ra.[ModifyDate]
		  ,ra.[CreateDate]
		  ,ra.[ModifiedBy]
		  ,ROW_NUMBER() OVER(ORDER BY RelevantAuthorityName) AS rowNumber
		FROM [dbo].[RelevantAuthority] ra WITH(NOLOCK)
		WHERE RelevantAuthorityName LIKE '%' + @FilterBy + '%'
	)
	SELECT 
		[RelevantAuthorityId],
		[RelevantAuthorityName],
		[ModifyDate],
		[CreateDate],
		[ModifiedBy]
	FROM PagedRas
	WHERE rowNumber BETWEEN ((@Page - 1) * @PageSize) + 1 AND (@Page * @PageSize) 
END
GO

GRANT EXECUTE ON [dbo].[RelevantAuthorityGetAll] TO [budgetviewer] AS [dbo]
GO


