USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM BeneficiaryType


IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'child')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'child'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'childrens')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'childrens'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'preserved pensioner')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'preserved pensioner'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'retired')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'retired'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'individual')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'individual'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'spouse -legal partner')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 'spouse -legal partner'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'spouse legal partner')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 'spouse legal partner'
END
