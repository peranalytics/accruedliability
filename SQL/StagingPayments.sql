USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [StagingPayments]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingPayments]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[StagingPayments](
		[VersionId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] VARCHAR(20) NOT NULL,
		[DateOfBirth] DATETIME NULL,
		Gender NVARCHAR(100) NOT NULL,
		CivilStatus NVARCHAR(100)  NULL,
		PensionScheme NVARCHAR(250) NOT NULL,
		TypeOfBeneficiary NVARCHAR(250) NOT NULL,
		PensionCommencementDate DATETIME NOT NULL,
		AnnualPensionValue DECIMAL(10,2) NOT NULL,
		AnnualSupplementaryPension DECIMAL(10,2) NOT NULL,
		BasisPensionCommenced NVARCHAR(250) NOT NULL,
		PensionableServiceYears DECIMAL(10,2) NULL,
		ActualEmploymentBasedServiceYears DECIMAL(10,2) NULL,
		PensionableServiceDifferenceReasons NVARCHAR(250) NULL,
		BasicPayValuePensionableRemuneration DECIMAL(10,2) NULL,
		PensionableAllowancesPensionableRemuneration DECIMAL(10,2) NULL,
		HowMuchAbatementIfAny NVARCHAR(250) NULL,
	)
END
GO

