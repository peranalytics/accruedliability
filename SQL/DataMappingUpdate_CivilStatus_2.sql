USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM CivilStatus
SELECT * FROM MappingCivilStatus

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'partner')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 2, 'partner'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'se')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 6, 'se'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'sp')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 6, 'sp'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'p')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'p'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'o')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'o'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'h')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'h'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'a')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'a'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'j')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'j'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'lp')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'lp'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'nd')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'nd'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'enga')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'enga'
END