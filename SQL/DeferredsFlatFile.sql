USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [DeferredsFlatFile]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeferredsFlatFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeferredsFlatFile](
	[RelevantAuthorityId] [int] NOT NULL,
	[RelevantAuthorityName] [nvarchar](255) NOT NULL,
	[SectorId] [int] NOT NULL,
	[SectorName] [nvarchar](250) NULL,
	[SubsectorId] [int] NOT NULL,
	[SubsectorName] [nvarchar](250) NULL,
	[YearId] [int] NOT NULL,
	[PPSN] [varchar](20) NOT NULL,
	[DateOfBirth] [datetime] NULL,
	[Gender] [nvarchar](50) NOT NULL,
	[CivilStatus] [nvarchar](50) NOT NULL,
	[PensionScheme] [nvarchar](50) NOT NULL,
	[PreservedPensionAge] [int] NOT NULL,
	[PrsiClass] [nvarchar](100) NOT NULL,
	[FTE] [decimal](10, 2) NOT NULL,
	[PensionableEmploymentStartDate] [datetime] NOT NULL,
	[PensionableEmploymentLeaveDate] [datetime] NOT NULL,
	[PensionableRemunerationFuturePensionBasedOn] [decimal](10, 2) NOT NULL,
	[PensionableServiceFuturePensionBasedOn] [decimal](10, 2) NULL
) ON [PRIMARY]
END
GO

