USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS GetAllSectors
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load deferreds to the table
-- =============================================
CREATE PROCEDURE GetAllSectors
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT SectorId, SectorName
	FROM Sectors

END
GO

GRANT EXECUTE ON GetAllSectors TO AccruedLiabilityViewer 
