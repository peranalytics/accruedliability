
USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileHistoryAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FileHistoryAdd] AS' 
END
GO



-- =============================================
-- Author:		Audrey
-- Create date: 06/02/2018
-- Description:	Add the meta data
-- =============================================
ALTER PROCEDURE [dbo].[FileHistoryAdd] 
		@DataStrategyId INT,
		@RelevantAuthorityId INT,
		@FileName NVARCHAR(255),
		@Year INT,
		@UploadedBy NVARCHAR(255),
		@TransactionTypeId INT,
		@FileTypeId INT,
		@RecordCount INT,
		@Validated BIT,
		@ModifyDate DATETIME,
		@CreateDate DATETIME,
		@Result INT OUTPUT,
		@SchemeType NVARCHAR(50),
		@ResultMessage NVARCHAR(100) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SET @Result = -1

	INSERT INTO [dbo].FileHistory([DataStrategyId],RelevantAuthorityId,[FileName],[Year],[UploadedBy],[TransactionTypeId],[FileTypeId],[RecordCount],[Validated],[ModifyDate],[CreateDate], Scheme)
	SELECT @DataStrategyId, @RelevantAuthorityId, @FileName, @Year, @UploadedBy, @TransactionTypeId, @FileTypeId, @RecordCount, @Validated, @ModifyDate, @CreateDate, @SchemeType

	SET @Result = @@IDENTITY

	INSERT INTO [FileTransformHistory]([VersionId], [FileTransformStatusId], [CreateDate], [ModifyDate])
	SELECT @Result, 0, GETDATE(), GETDATE()
END
GO

GRANT EXECUTE ON [dbo].[FileHistoryAdd] TO accruedLiabilityviewer AS [dbo]
GO


