
USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[BeneficiaryTypeGetAll] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 25/07/2019
-- Description:	Get the mapping lookup data
-- =============================================
CREATE PROCEDURE [dbo].[BeneficiaryTypeGetAll] 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, MappingBeneficiaryType FROM [dbo].MappingBeneficiaryType
END
GO

GRANT EXECUTE ON [dbo].[BeneficiaryTypeGetAll] TO budgetviewer AS [dbo]
GO


