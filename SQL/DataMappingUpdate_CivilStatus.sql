USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM CivilStatus
SELECT * FROM MappingCivilStatus


IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'cohabitting')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 11, 'cohabitting'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'co-habiting')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 11, 'co-habiting'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'co habitating')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 11, 'co habitating'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'cohabiting')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 11, 'cohabiting'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'co habiting')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 11, 'co habiting'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'co-hab')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 11, 'co-hab'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'in a civil partnership')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 2, 'in a civil partnership'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'civpar')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 2, 'civpar'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'di')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 3, 'di'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'd')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 3, 'd'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'div.')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 3, 'div.'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'divorce')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 3, 'divorce'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'd divorced')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 3, 'd divorced'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'a surviving civil partner')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 7, 'a surviving civil partner'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'judicially seperated')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 5, 'judicially seperated'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'judically separated')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 5, 'judically separated'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'maried')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 1, 'maried'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'marr.')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 1, 'marr.'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'ma')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 1, 'ma'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'm married')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 1, 'm married'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'separ.')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 6, 'separ.'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 's separated')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 6, 's separated'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'si')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 0, 'si'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'singe')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 0, 'singe'
END


IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 's single')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 0, 's single'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'u')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'u'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'other')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'other'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'not known')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'not known'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'un')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'un'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'undefined')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'undefined'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'unkown')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'unkown'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = '-')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, '-'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'w')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 8, 'w'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'wid.')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 8, 'wid.'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'w widowed')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 8, 'w widowed'
END
