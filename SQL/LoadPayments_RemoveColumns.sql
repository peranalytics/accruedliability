/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [AccruedLiability]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load Payments to the table
-- =============================================
ALTER PROCEDURE [dbo].[LoadPayments]
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE p
	FROM Payments p
	INNER JOIN StagingPayments sp
	ON p.RelevantAuthorityId = sp.RelevantAuthorityId
	AND p.YearId = sp.YearId

	INSERT INTO [dbo].[Payments]
			   ([VersionId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[GenderId]
			   ,[CivilStatusId]
			   ,[PensionSchemeId]
			   ,[TypeOfBeneficiaryId]
			   ,[PensionCommencementDate]
			   ,[AnnualPensionValue]
			   ,[AnnualSupplementaryPension]
			   ,[BasisPensionCommencedId]
			   ,[PensionableServiceYears]
			   ,[ActualEmploymentBasedServiceYears]
			   ,[PensionableServiceDifferenceReasons]
			   ,[BasicPayValuePensionableRemuneration]
			   ,[PensionableAllowancesPensionableRemuneration]
			   ,[HowMuchAbatementIfAny]
			   ,[CreateDate]
			   ,[ModifyDate])
	SELECT [VersionId]
		  ,[RelevantAuthorityId]
		  ,[YearId]
		  ,[PPSN]
		  ,[DateOfBirth]
		  ,mg.Id
		  ,msc.Id
		  ,mps.Id
		  ,mbt.Id
		  ,[PensionCommencementDate]
		  ,[AnnualPensionValue]
		  ,[AnnualSupplementaryPension]
		  ,mpb.Id
		  ,[PensionableServiceYears]
		  ,NULL
		  ,NULL
		  ,[BasicPayValuePensionableRemuneration]
		  ,[PensionableAllowancesPensionableRemuneration]
		  ,NULL
		  ,GETDATE()
		  ,GETDATE()
	  FROM [dbo].StagingPayments sp
	  INNER JOIN MappingGender mg
	  ON mg.MappingGender = sp.Gender
	  INNER JOIN MappingCivilStatus msc
	  ON msc.MappingCivilStatus = sp.CivilStatus
	  INNER JOIN MappingPensionScheme mps
	  ON mps.MappingPensionScheme = sp.PensionScheme
	  INNER JOIN MappingBeneficiaryType mbt
	  ON mbt.MappingBeneficiaryType = sp.TypeOfBeneficiary
	  INNER JOIN MappingPensionBasis mpb
	  ON mpb.MappingPensionBasis = sp.BasisPensionCommenced


END
GO


