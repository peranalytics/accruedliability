USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [MappingBenefitType]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MappingBenefitType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MappingBenefitType](
	[Id] [int] NOT NULL,
	[MappingBenefitType] [nvarchar](100) NOT NULL
)
END
GO

DELETE FROM [dbo].[MappingBenefitType]
INSERT INTO [dbo].[MappingBenefitType]
SELECT * FROM SPS..BenefitType

SELECT * FROM MappingBenefitType


