
USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileTransformUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FileTransformUpdate] AS' 
END
GO


-- =============================================
-- Author:		Audrey
-- Create date: 06/02/2018
-- Description:	Add the meta data
-- =============================================
ALTER PROCEDURE [dbo].[FileTransformUpdate] 
	@VersionId INT,
	@StatusId INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE FileTransformHistory 
	SET FileTransformStatusId = @StatusId
	WHERE VersionId = @VersionId
END
GO

GRANT EXECUTE ON [dbo].[FileTransformUpdate] TO accruedLiabilityviewer AS [dbo]
GO


