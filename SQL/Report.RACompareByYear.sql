USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS Report.RACompareByYear
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tracy
-- Create date: 27/03/2023
-- Description:	Compares record count for each file by year
-- =============================================
CREATE PROCEDURE Report.RACompareByYear
	@UserId INT


	AS
BEGIN
	
	SET NOCOUNT ON;

SELECT	
			'Actives' as FileType,
			a.SectorId,
			s.SectorName,
			a.RelevantAuthorityId,
			ra.RelevantAuthorityName,
			a.YearId, 
			Count(DISTINCT a.PPSN) as RecordCount
	FROM ActivesFlatFile a WITH(NOLOCK)
	INNER JOIN RelevantAuthority AS ra 
	ON ra.RelevantAuthorityId = a.RelevantAuthorityId
	INNER JOIN Sectors AS s
	ON a.SectorId = s.SectorId	
	GROUP BY a.RelevantAuthorityId, a.YearId, a.SectorId, s.SectorName, ra.RelevantAuthorityName
		UNION ALL

	SELECT 
			'Payments' as FileType,
			p.SectorId,
			s.SectorName,
			p.RelevantAuthorityId, 
			ra.RelevantAuthorityName,
			p.YearId, 
			Count(DISTINCT p.PPSN) as RecordCount
	FROM PaymentsFlatFile p WITH(NOLOCK)
	INNER JOIN RelevantAuthority AS ra 
	ON ra.RelevantAuthorityId = p.RelevantAuthorityId
		INNER JOIN Sectors AS s
	ON p.SectorId = s.SectorId
	GROUP BY p.RelevantAuthorityId, p.YearId, p.SectorId, s.SectorName, ra.RelevantAuthorityName
		UNION ALL 

	SELECT
			'Deferreds' as FileType,
			d.SectorId,
			s.SectorName,
			d.RelevantAuthorityId, 
			ra.RelevantAuthorityName,
			d.YearId, 
			Count(DISTINCT d.PPSN) as RecordCount
	FROM DeferredsFlatFile d WITH(NOLOCK)
	INNER JOIN RelevantAuthority AS ra 
	ON ra.RelevantAuthorityId = d.RelevantAuthorityId
	INNER JOIN Sectors AS s
	ON d.SectorId = s.SectorId
	GROUP BY d.RelevantAuthorityId, d.YearId, d.SectorId, s.SectorName, ra.RelevantAuthorityName

	
END
GO



GRANT EXECUTE ON Report.RACompareByYear TO AccruedLiabilityViewer 
