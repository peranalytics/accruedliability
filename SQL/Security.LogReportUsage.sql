USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [Security].[LogReportUsage]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Disable user
-- =============================================
CREATE PROCEDURE [Security].[LogReportUsage]
	@UserId INT,
	@ReportId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	INSERT INTO [Security].[ReportUsage] ([UserId], [ReportId])
	SELECT @UserId, @ReportId
	
	
END
GO

GRANT EXECUTE ON [Security].[LogReportUsage] TO [accruedliabilityviewer] AS [dbo]
GO


