USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS Security.[GetUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Get all config values
-- =============================================
CREATE PROCEDURE Security.[GetUser]
	@UserId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT us.Userid, UserName, ur.RoleId 
	FROM Security.Users us
	INNER JOIN Security.UserRoles ur
	ON us.UserId = ur.UserId
	WHERE us.UserId = @UserId
END
GO

GRANT EXECUTE ON Security.[GetUser] TO accruedliabilityviewer 
