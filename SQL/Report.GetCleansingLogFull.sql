USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS Report.GetCleansingLogFull
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================
-- Author:		Tracy
-- Create date: 05/05/2020
-- Description:	Load data from Cleansing Log Full File
-- =====================================================
CREATE PROCEDURE Report.GetCleansingLogFull
	@UserId INT,
	@Schemes NVARCHAR(max) = '-1',
	@PPSN VARCHAR (20) = NULL,
	@Year INT
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @DetailAccess INT
	SELECT @DetailAccess = DetailAccess FROM Security.Roles r INNER JOIN Security.UserRoles ur ON ur.RoleId = r.RoleId WHERE ur.UserId = @UserId
	

	SELECT VALUE AS Scheme
	INTO #SchemeList FROM STRING_SPLIT(@Schemes, ',')
	WHERE RTRIM(value) <> '';


	SELECT 
		  [Scheme]
		  ,[VersionId]
		  ,cl.[SectorId]
		  ,s.SectorName
		  ,cl.RelevantAuthorityId 
		  ,ra.RelevantAuthorityName
		  ,[YearId]
		  ,(CASE WHEN @DetailAccess = 1 THEN PPSN ELSE '--------' END) AS PPSN
		  ,[DateOfBirth]
		  ,[Property]
		  ,[OriginalValue]
		  ,[UpdatedValue]
	 FROM [dbo].[CleaningLogFull] cl WITH(NOLOCK)
	INNER JOIN RelevantAuthority ra ON cl.RelevantAuthorityId = ra.RelevantAuthorityId
	INNER JOIN Sectors s ON cl.SectorId = s.SectorId
	WHERE(@Schemes = '-1') OR  Scheme IN (SELECT Scheme FROM #SchemeList)
	AND (@PPSN IS NULL OR @PPSN = PPSN)
	AND (@Year = YearId)

	DROP TABLE #SchemeList


END

GO



GRANT EXECUTE ON Report.GetCleansingLogFull TO AccruedLiabilityViewer 
