
USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileHistorySummaryCreate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FileHistorySummaryCreate] AS' 
END
GO

-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Summary for all FileHistory
-- =============================================
ALTER PROCEDURE [dbo].[FileHistorySummaryCreate]
AS
BEGIN
	
	SET NOCOUNT ON;

	DELETE FROM FileHistorySummary

	CREATE TABLE #Years
	(
		YrId INT 
	)

	;WITH yearlist AS
	(
		SELECT YEAR(GetDate()) - 1 AS YrId
		UNION ALL
		SELECT yl.YrId + 1 AS YrId
		FROM yearlist yl
		WHERE yl.YrId + 1 <= YEAR(GetDate()) + 7
	)
	INSERT INTO #Years(YrId)
	SELECT YrId FROM yearlist

	DECLARE @YearCount INT
	SELECT @YearCount = COUNT(YrId) FROM #Years

	CREATE TABLE #FileHistorySummary(
		RelevantAuthorityId INT NOT NULL,
		Yr int NOT NULL,
		[FileName] NVARCHAR(255) NOT NULL,
		RecordCount INT NOT NULL,
		ModifyDate datetime NOT NULL,
		Success bit NULL,
		Scheme nvarchar(50) NULL,
		UploadedBy NVARCHAR(255) NOT NULL,
		EntryUpdateDate datetime NULL
	)

	;WITH FileHistorySummary AS (
	SELECT ra.RelevantAuthorityId AS RelevantAuthorityId, md.Year, FileName, RecordCount, md.ModifyDate, Success, Scheme, UploadedBy, EntryUpdateDate, RANK()  OVER (PARTITION BY md.Year,  ra.RelevantAuthorityId, Scheme ORDER BY EntryUpdateDate DESC, VersionId DESC) Position
	FROM RelevantAuthority ra WITH(NOLOCK)
	LEFT JOIN FileHistory md WITH(NOLOCK)
	ON ra.RelevantAuthorityId = md.RelevantAuthorityId
	WHERE success = 1
	)
	INSERT INTO #FileHistorySummary(RelevantAuthorityId, Yr, [FileName], RecordCount, ModifyDate, Success, Scheme, UploadedBy, EntryUpdateDate)
	SELECT RelevantAuthorityId, Year, [FileName], RecordCount, ModifyDate, Success, Scheme, UploadedBy, EntryUpdateDate FROM FileHistorySummary md 
	WHERE Position = 1
	ORDER By RelevantAuthorityId, Scheme, md.Year

	CREATE TABLE #AllYearsbyRaAndScheme
	(
		YrId INT, 
		RelevantAuthorityId INT, 
		RelevantAuthorityName NVARCHAR(255),
		Scheme NVARCHAR(50)
	)
	INSERT INTO #AllYearsbyRaAndScheme (YrId, RelevantAuthorityId, RelevantAuthorityName, Scheme)
	SELECT YrId, ra.RelevantAuthorityId, RelevantAuthorityName, SchemeType
	FROM #Years, RelevantAuthority ra, SchemeType

	INSERT INTO FileHistorySummary(YrId, RelevantAuthorityId, RelevantAuthorityName, Scheme, FileName, RecordCount, ModifyDate, Success, UploadedBy)
	SELECT YrId,  y.RelevantAuthorityId, y.RelevantAuthorityName, y.Scheme, FileName, RecordCount, ModifyDate, Success, UploadedBy
	FROM #AllYearsbyRaAndScheme y
	OUTER APPLY
	(
		SELECT FileName, RecordCount, ModifyDate, Success, UploadedBy
		FROM #FileHistorySummary mds
		WHERE mds.Yr = y.YrId
		AND mds.RelevantAuthorityId = y.RelevantAuthorityId
		AND mds.Scheme = y.Scheme
	) A

	DROP TABLE #Years
	DROP TABLE #AllYearsbyRaAndScheme
	DROP TABLE #FileHistorySummary
END
GO

GRANT EXECUTE ON [dbo].[FileHistorySummaryCreate] TO AccruedLiabilityviewer AS [dbo]
GO


