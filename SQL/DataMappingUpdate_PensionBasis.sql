USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM PensionBasis

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary early retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'voluntary early retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'scheme of early retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'scheme of early retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retirement/voluntary redundancy scheme')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'early retirement/voluntary redundancy scheme'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'early retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'early'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'cost neutral retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'cost neutral retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'cost neutral early retirments')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'cost neutral early retirments'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'permanent infirmity')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'permanent infirmity'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'permament infirmity')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'permament infirmity'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'permament infirmity')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'permament infirmity'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'permament infirmity')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'permament infirmity'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ill-health retirment')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'ill-health retirment'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ill-health early retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'ill-health early retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary redundancy scheme')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'voluntary redundancy scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary early retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'voluntary early retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary age grounds')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'voluntary age grounds'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'ver'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'incentivised scheme for early retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'incentivised scheme for early retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'incentivised early retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'incentivised early retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retire. scheme nurses')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'early retire. scheme nurses'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'retired')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'retired'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'reached minimum retirement age')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'reached minimum retirement age'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'normal retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'normal retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'spouses')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'spouses'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'spouse ill-health')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'spouse ill-health'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'dependent')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'dependent'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'death of spouse')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'death of spouse'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'child incapacitated')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'child incapacitated'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'child')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'child'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'not known')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 5, 'not known'
END
