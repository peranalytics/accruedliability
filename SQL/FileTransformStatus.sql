USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileTransformStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileTransformStatus](
	[Id] [int] NOT NULL,
	[FileTransformStatus] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FileTransformStatus_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

DELETE FROM FileTransformStatus

INSERT INTO FileTransformStatus(Id, [FileTransformStatus])
SELECT 0, 'Not Transformed' UNION ALL
SELECT 1, 'Transformed' UNION ALL
SELECT 2, 'Transform Failed'

SELECT * FROM FileTransformStatus