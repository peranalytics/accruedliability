USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [Security].[UserReports]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserReports]') AND type in (N'U'))
BEGIN
CREATE TABLE[Security].[UserReports](
		[UserId] [int] NOT NULL,
		ReportId INT NOT NULL,
		ModifyDate DATETIME NOT NULL,
CONSTRAINT [PK_UserReports] PRIMARY KEY CLUSTERED 
(
	[UserId], ReportId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserReports_RoleId]') AND parent_object_id = OBJECT_ID(N'[Security].[UserReports]'))
ALTER TABLE [Security].[UserReports]  WITH CHECK ADD  CONSTRAINT [FK_UserReports_RoleId] FOREIGN KEY(ReportId)
REFERENCES [Report].[Reports] (ReportId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserReports_UserId]') AND parent_object_id = OBJECT_ID(N'[Security].[UserReports]'))
ALTER TABLE [Security].[UserReports]  WITH CHECK ADD  CONSTRAINT [FK_UserReports_UserId] FOREIGN KEY([UserId])
REFERENCES [Security].[Users] (UserId)
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF__UserReports_ModifyDate]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserReports] ADD  DEFAULT ((GETDATE())) FOR ModifyDate
END

INSERT INTO [Security].UserReports(UserId, ReportId, ModifyDate)
SELECT UserId, ReportId, GETDATE() 
FROM [Report].Reports r,Security.Users u





