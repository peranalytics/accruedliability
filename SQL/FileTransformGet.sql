
USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileTransformGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FileTransformGet] AS' 
END
GO



-- =============================================
-- Author:		Audrey
-- Create date: 06/02/2018
-- Description:	Add the meta data
-- =============================================
ALTER PROCEDURE [dbo].[FileTransformGet] 
		
AS
BEGIN
	SET NOCOUNT ON;

	SELECT fh.VersionId, RelevantAUthorityId, Year, FileName, LocalFileName, Scheme 
	FROM FileHistory fh
	INNER JOIN FileTransformHistory fth
	ON fh.VersionId = fth.VersionId
	WHERE NOT fth.FileTransformStatusId = 1
	AND Success = 1
	ORDER BY VersionId
END
GO

GRANT EXECUTE ON [dbo].[FileTransformGet] TO accruedLiabilityviewer AS [dbo]
GO


