
USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[PensionBasisGetAll] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 25/07/2019
-- Description:	Get the mapping lookup data
-- =============================================
CREATE PROCEDURE [dbo].[PensionBasisGetAll] 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, MappingPensionBasis FROM [dbo].MappingPensionBasis
END
GO

GRANT EXECUTE ON [dbo].[PensionBasisGetAll] TO budgetviewer AS [dbo]
GO


