USE AccruedLiability

INSERT INTO [FileTransformHistory]([VersionId], [FileTransformStatusId], [CreateDate], [ModifyDate])
SELECT VersionId, 0, GETDATE(), GETDATE() FROM FileHistory
WHERE VersionId NOT IN (SELECT fth.VersionId FROM FileTransformHistory fth)