USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM MappingGender
SELECT * FROM Gender

IF NOT EXISTS (SELECT 1 FROM MappingGender WHERE MappingGender = '0')
BEGIN
	INSERT INTO MappingGender(Id, MappingGender)
	SELECT 2, '0'
END

IF NOT EXISTS (SELECT 1 FROM MappingGender WHERE MappingGender = 'female unknown')
BEGIN
	INSERT INTO MappingGender(Id, MappingGender)
	SELECT 2, 'female unknown'
END

