USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [MappingPensionScheme]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MappingPensionScheme]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MappingPensionScheme](
	[Id] [int] NOT NULL,
	[MappingPensionScheme] [nvarchar](50) NOT NULL,
)
END
GO



DELETE FROM [dbo].MappingPensionScheme
INSERT INTO [dbo].MappingPensionScheme
SELECT * FROM PensionScheme

SELECT * FROM MappingPensionScheme