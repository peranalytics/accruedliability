USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Year]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[Year](
	[YearId] [int] NOT NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_Year_Id] PRIMARY KEY CLUSTERED 
(
	[YearId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Year_ModifiedBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[Year]'))
ALTER TABLE [dbo].[Year]  WITH CHECK ADD  CONSTRAINT [FK_Year_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [Security].[Users] (UserId)
GO


