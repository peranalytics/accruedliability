USE AccruedLiability
GO

IF NOT EXISTS (SELECT 1 FROM Config WHERE ConfigName = 'ConfirmationExpiryHours')
BEGIN
INSERT INTO [dbo].Config
           ([ConfigName]
		   ,[ConfigValue])
     VALUES
         ('WindowsReportPassport', '5abd5c90fc!')
END

IF NOT EXISTS (SELECT 1 FROM Config WHERE ConfigName = 'ForgotPasswordExpiryHours')
BEGIN
INSERT INTO [dbo].Config
           ([ConfigName]
		   ,[ConfigValue])
     VALUES
         ('WindowsReportUsername', 'zTDzogciorssa')
END

IF NOT EXISTS (SELECT 1 FROM Config WHERE ConfigName = 'ForgotPasswordExpiryHours')
BEGIN
INSERT INTO [dbo].Config
           ([ConfigName]
		   ,[ConfigValue])
     VALUES
         ('WindowsReportDomain', 'TestDomain')
END

IF NOT EXISTS (SELECT 1 FROM Config WHERE ConfigName = 'ForgotPasswordExpiryHours')
BEGIN
INSERT INTO [dbo].Config
           ([ConfigName]
		   ,[ConfigValue])
     VALUES
         ('ReportServerURL', 'http://td-daldev1/ReportServer_SSRS')
END