USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MappingPrsiClass]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MappingPrsiClass](
	[Id] [int] NOT NULL,
	[MappingPrsiClass] [nvarchar](50) NOT NULL
)
END
GO

DELETE FROM [dbo].MappingPrsiClass
INSERT INTO [dbo].MappingPrsiClass
SELECT * FROM PrsiClass



SELECT * FROM MappingPrsiClass