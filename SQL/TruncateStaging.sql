USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS TruncateStaging
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Truncates the staging tables
-- =============================================
CREATE PROCEDURE TruncateStaging
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE FROM StagingActives
	DELETE FROM StagingPayments
	DELETE FROM StagingDeferreds

END
GO

GRANT EXECUTE ON TruncateStaging TO AccruedLiabilityViewer 
