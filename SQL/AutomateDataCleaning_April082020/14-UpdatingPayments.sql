USE AccruedLiability

UPDATE [PaymentsDataUpdates]
SET GenderId = OriginalGenderId
WHERE GenderId is null

UPDATE [PaymentsDataUpdates]
SET TypeOfBeneficiaryId = OriginalTypeOfBeneficiaryId
WHERE TypeOfBeneficiaryId is null

UPDATE [PaymentsDataUpdates]
SET AnnualPensionValue = OriginalAnnualPensionValue
WHERE AnnualPensionValue is null


DELETE FROM Payments
WHERE PensionCommencementDate > '31 dec 2018'

UPDATE a
SET a.GenderId = u.GenderId
FROM Payments a
INNER JOIN [PaymentsDataUpdates] u
ON a.PPSN = u.PPSN
AND (a.DateOfBirth = u.DateOfBirth OR (a.DateOfBirth IS NULL AND u.DateOfBirth IS NULL))
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.GenderId = u.OriginalGenderId
AND NOT u.GenderId = u.OriginalGenderId

UPDATE a
SET a.TypeOfBeneficiaryId = u.TypeOfBeneficiaryId
FROM Payments a
INNER JOIN [PaymentsDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.TypeOfBeneficiaryId = u.OriginalTypeOfBeneficiaryId
AND NOT u.TypeOfBeneficiaryId = u.OriginalTypeOfBeneficiaryId

UPDATE a
SET a.AnnualPensionValue = u.AnnualPensionValue
FROM Payments a
INNER JOIN [PaymentsDataUpdates] u
ON a.PPSN = u.PPSN
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.AnnualPensionValue = u.OriginalAnnualPensionValue
AND NOT u.AnnualPensionValue = u.OriginalAnnualPensionValue

SELECT * FROM Payments WHERE AnnualPensionValue < = 0
SELECT * FROM Payments WHERE TypeOfBeneficiaryId= 5
SELECT * FROM Payments WHERE GenderId= 2

