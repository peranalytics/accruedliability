USE [AccruedLiability]
GO

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'PaymentsDataUpdates'))
BEGIN

	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[PaymentsDataUpdates](
		[VersionId] [int] NOT NULL,
		[SectorId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] [varchar](20) NOT NULL,
		[DateOfBirth] [datetime] NULL,
		[PensionCommencementDate] [datetime] NOT NULL,
		[OriginalGenderId] [int] NOT NULL,
		[GenderId] [int] NULL,
		[OriginalTypeOfBeneficiaryId] [int] NOT NULL,
		[TypeOfBeneficiaryId] [int] NULL,
		[OriginalAnnualPensionValue] [decimal](10, 2) NOT NULL,
		[AnnualPensionValue] [decimal](10, 2) NULL,
		[ModifyDate] [datetime] NOT NULL
	) ON [PRIMARY]

END

GO


SELECT * FROM [PaymentsDataUpdates]
