USE AccruedLiability


IF NOT EXISTS (SELECT 1 FROM [ActivesDataUpdates])
BEGIN
	INSERT INTO [dbo].[ActivesDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[DateOfEntryIntoScheme]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalMinimumNormalRetirementAge]
			   ,[MinimumNormalRetirementAge]
			   ,[OriginalAnnualPensionablePayFTE]
			   ,[AnnualPensionablePayFTE]
			   ,[ModifyDate])
	SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, DateOfEntryIntoScheme, a.GenderId, NULL, a.PensionSchemeId, NULL, a.MinimumNormalRetirementAge, NULL, a.AnnualPensionablePayFTE, NULL, GETDATE()
	FROM Actives a
	INNER JOIN RelevantAuthoritySectors ras
	ON a.RelevantAuthorityId = ras.RelevantAuthorityId
	WHERE GenderId = 2
END
GO


UPDATE a
SET [GenderId] = CAST(VAlue AS INT)
FROM [ActivesDataUpdates] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE Scheme = 'Actives' AND Property = 'Gender'
AND GenderId IS NULL
AND [OriginalGenderId] = 2


INSERT INTO [dbo].[ActivesDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[DateOfEntryIntoScheme]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalMinimumNormalRetirementAge]
			   ,[MinimumNormalRetirementAge]
			   ,[OriginalAnnualPensionablePayFTE]
			   ,[AnnualPensionablePayFTE]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, DateOfEntryIntoScheme,  a.GenderId, NULL, a.PensionSchemeId, NULL, a.MinimumNormalRetirementAge, NULL, a.AnnualPensionablePayFTE, NULL, GETDATE()
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionSchemeId = 6
AND PPSN NOT IN (SELECT sub.PPSN FROM [ActivesDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)


UPDATE a
SET PensionSchemeId = 1
FROM [ActivesDataUpdates] a
WHERE [OriginalPensionSchemeId] = 6 
AND [PensionSchemeId] IS NULL
AND [DateOfEntryIntoScheme] <= '05 apr 1995'
--1486

UPDATE a
SET PensionSchemeId = 2
FROM [ActivesDataUpdates] a
WHERE [OriginalPensionSchemeId] = 6 
AND [PensionSchemeId] IS NULL
AND [DateOfEntryIntoScheme] BETWEEN ' 06 apr 1995' AND '31 mar 2004'
--2705

UPDATE a
SET PensionSchemeId = 3
FROM [ActivesDataUpdates] a
WHERE [OriginalPensionSchemeId] = 6 
AND [PensionSchemeId] IS NULL
AND [DateOfEntryIntoScheme] BETWEEN ' 01 apr 2004' AND '31 dec 2012'
--3936

UPDATE a
SET PensionSchemeId = 4
FROM [ActivesDataUpdates] a
WHERE [OriginalPensionSchemeId] = 6 
AND [PensionSchemeId] IS NULL
AND [DateOfEntryIntoScheme] >= ' 01 jan 2013'
--1262


INSERT INTO [dbo].[ActivesDeleted]
           ([VersionId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[GenderId]
           ,[CivilStatusId]
           ,[PensionSchemeId]
           ,[DateOfEntryIntoScheme]
           ,[MinimumNormalRetirementAge]
           ,[PrsiClass]
           ,[AnnualPensionablePayFTE]
           ,[LengthOfService]
           ,[FTE]
           ,[Grade]
           ,[ScalePoint]
           ,[IncrementDate]
           ,[CreateDate]
           ,[ModifyDate])

SELECT [VersionId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
      ,[DateOfBirth]
      ,[GenderId]
      ,[CivilStatusId]
      ,[PensionSchemeId]
      ,[DateOfEntryIntoScheme]
      ,[MinimumNormalRetirementAge]
      ,[PrsiClass]
      ,[AnnualPensionablePayFTE]
      ,[LengthOfService]
      ,[FTE]
      ,[Grade]
      ,[ScalePoint]
      ,[IncrementDate]
      ,[CreateDate]
      ,[ModifyDate]
  FROM [dbo].[Actives] a
WHERE [DateOfEntryIntoScheme] > '31 dec 2018'
AND PPSN NOT in (SELECT sub.PPSN FROM ActivesDeleted sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId])


INSERT INTO [dbo].[ActivesDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[DateOfEntryIntoScheme]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalMinimumNormalRetirementAge]
			   ,[MinimumNormalRetirementAge]
			   ,[OriginalAnnualPensionablePayFTE]
			   ,[AnnualPensionablePayFTE]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, DateOfEntryIntoScheme,  a.GenderId, NULL, a.PensionSchemeId, NULL, a.MinimumNormalRetirementAge, NULL, a.AnnualPensionablePayFTE, NULL, GETDATE()
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE (MinimumNormalRetirementAge IS NULL OR MinimumNormalRetirementAge = 0)
AND PPSN NOT IN (SELECT sub.PPSN FROM [ActivesDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)


UPDATE a
SET [MinimumNormalRetirementAge] = 60
FROM [ActivesDataUpdates] a
WHERE ([OriginalMinimumNormalRetirementAge] IS NULL OR [OriginalMinimumNormalRetirementAge] = 0)
AND ([OriginalPensionSchemeId] IN (1,2) OR [PensionSchemeId] IN (1,2))
AND [OriginalMinimumNormalRetirementAge] = 0
AND [MinimumNormalRetirementAge] IS NULL
--2

UPDATE a
SET [MinimumNormalRetirementAge] = 65
FROM [ActivesDataUpdates] a
WHERE ([OriginalMinimumNormalRetirementAge] IS NULL OR [OriginalMinimumNormalRetirementAge] = 0)
AND ([OriginalPensionSchemeId] IN (3) OR [PensionSchemeId] IN (3))
AND [OriginalMinimumNormalRetirementAge] = 0
AND [MinimumNormalRetirementAge] IS NULL
--4


UPDATE a
SET [MinimumNormalRetirementAge] = 66
FROM [ActivesDataUpdates] a
WHERE ([OriginalMinimumNormalRetirementAge] IS NULL OR [OriginalMinimumNormalRetirementAge] = 0)
AND ([OriginalPensionSchemeId] IN (4) OR [PensionSchemeId] IN (4))
AND [OriginalMinimumNormalRetirementAge] = 0
AND DateOfBirth < '1 jan 1955'
AND [MinimumNormalRetirementAge] IS NULL
--0

UPDATE a
SET [MinimumNormalRetirementAge] = 67
FROM [ActivesDataUpdates] a
WHERE ([OriginalMinimumNormalRetirementAge] IS NULL OR [OriginalMinimumNormalRetirementAge] = 0)
AND ([OriginalPensionSchemeId] IN (4) OR [PensionSchemeId] IN (4))
AND [OriginalMinimumNormalRetirementAge] = 0
AND DateOfBirth >= '1 jan 1955' and DateOfBirth <  '31 dec 1960'
AND [MinimumNormalRetirementAge] IS NULL
--0

UPDATE a
SET [MinimumNormalRetirementAge] = 68
FROM [ActivesDataUpdates] a
WHERE ([OriginalMinimumNormalRetirementAge] IS NULL OR [OriginalMinimumNormalRetirementAge] = 0)
AND ([OriginalPensionSchemeId] IN (4) OR [PensionSchemeId] IN (4))
AND [OriginalMinimumNormalRetirementAge] = 0
AND DateOfBirth >= '1 jan 1961'
AND [MinimumNormalRetirementAge] IS NULL

UPDATE a
SET [MinimumNormalRetirementAge] = 65
FROM [ActivesDataUpdates] a
WHERE ([OriginalMinimumNormalRetirementAge] IS NULL OR [OriginalMinimumNormalRetirementAge] = 0)
AND ([OriginalPensionSchemeId] IN (5) OR [PensionSchemeId] IN (5))
AND [OriginalMinimumNormalRetirementAge] = 0
AND ([MinimumNormalRetirementAge] IS NULL OR [MinimumNormalRetirementAge] = 0)
--150 non established

INSERT INTO [dbo].[ActivesDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[DateOfEntryIntoScheme]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalMinimumNormalRetirementAge]
			   ,[MinimumNormalRetirementAge]
			   ,[OriginalAnnualPensionablePayFTE]
			   ,[AnnualPensionablePayFTE]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, DateOfEntryIntoScheme, a.GenderId, NULL, a.PensionSchemeId, NULL, a.MinimumNormalRetirementAge, NULL, a.AnnualPensionablePayFTE, NULL, GETDATE()
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE ([AnnualPensionablePayFTE] IS NULL OR [AnnualPensionablePayFTE] <=0)
AND PPSN NOT IN (SELECT sub.PPSN FROM [ActivesDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)

UPDATE a
SET AnnualPensionablePayFTE = CAST(Value AS DECIMAL(10,2))
FROM [ActivesDataUpdates] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE Scheme = 'Actives' AND Property = 'AnnualPensionablePayFTE'
AND ([OriginalAnnualPensionablePayFTE] IS NULL OR [OriginalAnnualPensionablePayFTE] <=0)

INSERT INTO [dbo].[ActivesDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[DateOfEntryIntoScheme]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalMinimumNormalRetirementAge]
			   ,[MinimumNormalRetirementAge]
			   ,[OriginalAnnualPensionablePayFTE]
			   ,[AnnualPensionablePayFTE]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, DateOfEntryIntoScheme, a.GenderId, NULL, a.PensionSchemeId, NULL, a.MinimumNormalRetirementAge, a.MinimumNormalRetirementAge, a.AnnualPensionablePayFTE, NULL, GETDATE()
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') > MinimumNormalRetirementAge
AND PPSN NOT IN (SELECT sub.PPSN FROM [ActivesDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)
--7087 / 5027 new

UPDATE [ActivesDataUpdates]
SET MinimumNormalRetirementAge = OriginalMinimumNormalRetirementAge
WHERE MinimumNormalRetirementAge IS NULL

UPDATE [ActivesDataUpdates]
SET MinimumNormalRetirementAge = DATEDIFF(YEAR, DateOfBirth, '31 dec 2019')
WHERE DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') > MinimumNormalRetirementAge
--5045 updated min retirement age


