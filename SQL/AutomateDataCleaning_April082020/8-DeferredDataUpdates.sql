USE [AccruedLiability]
GO

--DROP TABLE DeferredsDataUpdates


IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'DeferredsDataUpdates'))
BEGIN

	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[DeferredsDataUpdates](
		[VersionId] [int] NOT NULL,
		[SectorId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] [varchar](20) NOT NULL,
		DateOfBirth DATETIME NULL,
		PensionableEmploymentStartDate DATETIME NOT NULL,
		[OriginalGenderId] [int] NOT NULL,
		[GenderId] [int] NULL,
		[OriginalPensionSchemeId] [int] NOT NULL,
		[PensionSchemeId] [int] NULL,
		[OriginalPreservedPensionAge] [int] NOT NULL,
		[PreservedPensionAge] [int] NULL,
		[OriginalPensionableRemunerationFuturePensionBasedOn] [decimal](10, 2) NOT NULL,
		[PensionableRemunerationFuturePensionBasedOn] [decimal](10, 2) NULL,
		[ModifyDate] [datetime] NOT NULL DEFAULT GETDATE()
	) ON [PRIMARY]

END


SELECT * FROM [DeferredsDataUpdates]