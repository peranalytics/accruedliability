USE AccruedLiability

DELETE FROM Actives
WHERE [DateOfEntryIntoScheme] > '31 dec 2018'

UPDATE a
SET a.GenderId = u.GenderId
FROM Actives a
INNER JOIN [ActivesDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.GenderId = u.OriginalGenderId
AND NOT u.GenderId = u.OriginalGenderId

UPDATE a
SET a.PensionSchemeId = u.PensionSchemeId
FROM Actives a
INNER JOIN [ActivesDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionSchemeId = u.OriginalPensionSchemeId
AND NOT u.PensionSchemeId = u.OriginalPensionSchemeId

UPDATE a
SET a.MinimumNormalRetirementAge = u.MinimumNormalRetirementAge
FROM Actives a
INNER JOIN [ActivesDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.MinimumNormalRetirementAge = u.OriginalMinimumNormalRetirementAge
AND NOT u.MinimumNormalRetirementAge = u.OriginalMinimumNormalRetirementAge
--5184 total

UPDATE a
SET a.AnnualPensionablePayFTE = u.AnnualPensionablePayFTE
FROM Actives a
INNER JOIN [ActivesDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.AnnualPensionablePayFTE = u.OriginalAnnualPensionablePayFTE
AND NOT u.AnnualPensionablePayFTE = u.OriginalAnnualPensionablePayFTE


SELECT DATEDIFF(YEAR, DateOfBirth, '31 dec 2018'),  * FROM Actives WHERE DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') > MinimumNormalRetirementAge
SELECT * FROM Actives WHERE MinimumNormalRetirementAge = 0 -- 2 with null dob
SELECT * From Actives WHERE PensionSchemeId = 6
SELECT * FROM Actives WHERE AnnualPensionablePayFTE = 0


