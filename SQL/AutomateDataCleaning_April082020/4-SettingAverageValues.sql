use AccruedLiability

--select *From Actives --201329

;WITH RankedGenders AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	Gender, 
	GenderId,
	COUNT(GenderId) AS Count,
	RANK() OVER (PARTITION BY s.SectorId, SectorName ORDER BY COUNT(GenderId) DESC) AS Rank  
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE NOT Gender = 'Unknown'
GROUP BY s.SectorId, SectorName, GenderId, Gender
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Actives', 'Gender', CAST(GenderId AS NVARCHAR(100)) FROM RankedGenders 
WHERE Rank = 1 AND SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Actives' AND Property = 'Gender')


;WITH AverageSalaryBySector AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	AVG(AnnualPensionablePayFTE) AvgPay
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE (NOT AnnualPensionablePayFTE IS NULL) AND AnnualPensionablePayFTE > 0
GROUP BY s.SectorId, SectorName
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Actives', 'AnnualPensionablePayFTE',CAST(AvgPay AS DECIMAL(18,2)) FROM AverageSalaryBySector 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Actives' AND Property = 'AnnualPensionablePayFTE')


;WITH RankedGenders AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	Gender, 
	GenderId,
	COUNT(GenderId) AS Count,
	RANK() OVER (PARTITION BY s.SectorId, SectorName ORDER BY COUNT(GenderId) DESC) AS Rank  
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE NOT Gender = 'Unknown'
GROUP BY s.SectorId, SectorName, GenderId, Gender
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Deferreds', 'Gender', CAST(GenderId AS NVARCHAR(100)) FROM RankedGenders 
WHERE Rank = 1 AND SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Deferreds' AND Property = 'Gender')


;WITH AverageSalaryBySector AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	AVG(PensionableRemunerationFuturePensionBasedOn) AvgPay
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE (NOT PensionableRemunerationFuturePensionBasedOn IS NULL) AND PensionableRemunerationFuturePensionBasedOn > 0
GROUP BY s.SectorId, SectorName
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Deferreds', 'PensionableRemunerationFuturePensionBasedOn',CAST(AvgPay AS DECIMAL(18,2)) FROM AverageSalaryBySector 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Deferreds' AND Property = 'PensionableRemunerationFuturePensionBasedOn')


;WITH RankedGenders AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	Gender, 
	GenderId,
	COUNT(GenderId) AS Count,
	RANK() OVER (PARTITION BY s.SectorId, SectorName ORDER BY COUNT(GenderId) DESC) AS Rank  
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE NOT Gender = 'Unknown'
GROUP BY s.SectorId, SectorName, GenderId, Gender
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Payments', 'Gender', CAST(GenderId AS NVARCHAR(100)) FROM RankedGenders 
WHERE Rank = 1 AND SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Payments' AND Property = 'Gender')


;WITH AverageSalaryBySector AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	AVG(AnnualPensionValue) AvgPay
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE (NOT AnnualPensionValue IS NULL) AND AnnualPensionValue > 0
GROUP BY s.SectorId, SectorName
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Payments', 'AnnualPensionValue',CAST(AvgPay AS DECIMAL(18,2)) FROM AverageSalaryBySector 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Payments' AND Property = 'AnnualPensionValue')


SELECT * FROM [DataCleaningValuesBySector]


