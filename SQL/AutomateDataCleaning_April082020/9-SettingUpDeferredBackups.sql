USE AccruedLiability


IF NOT EXISTS (SELECT 1 FROM [DeferredsDataUpdates])
BEGIN
	INSERT INTO [dbo].[DeferredsDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[PensionableEmploymentStartDate]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalPreservedPensionAge]
			   ,[PreservedPensionAge]
			   ,[OriginalPensionableRemunerationFuturePensionBasedOn]
			   ,[PensionableRemunerationFuturePensionBasedOn]
			   ,[ModifyDate])
	SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, a.GenderId, NULL, a.PensionSchemeId, NULL, a.PreservedPensionAge, NULL, a.PensionableRemunerationFuturePensionBasedOn, NULL, GETDATE()
	FROM Deferreds a
	INNER JOIN RelevantAuthoritySectors ras
	ON a.RelevantAuthorityId = ras.RelevantAuthorityId
	WHERE GenderId = 2
END
GO


UPDATE a
SET [GenderId] = CAST(VAlue AS INT)
FROM [DeferredsDataUpdates] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE Scheme = 'Deferreds' AND Property = 'Gender'
AND GenderId IS NULL
AND [OriginalGenderId] = 2


INSERT INTO [dbo].[DeferredsDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[PensionableEmploymentStartDate]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalPreservedPensionAge]
			   ,[PreservedPensionAge]
			   ,[OriginalPensionableRemunerationFuturePensionBasedOn]
			   ,[PensionableRemunerationFuturePensionBasedOn]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate,  a.GenderId, NULL, a.PensionSchemeId, NULL, a.PreservedPensionAge, NULL, a.PensionableRemunerationFuturePensionBasedOn, NULL, GETDATE()
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionSchemeId = 6
AND PPSN NOT IN (SELECT sub.PPSN FROM [DeferredsDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)


UPDATE a
SET PensionSchemeId = 1
FROM [DeferredsDataUpdates] a
WHERE [OriginalPensionSchemeId] = 6 
AND [PensionSchemeId] IS NULL
AND [PensionableEmploymentStartDate] <= '05 apr 1995'
--386

UPDATE a
SET PensionSchemeId = 2
FROM [DeferredsDataUpdates] a
WHERE [OriginalPensionSchemeId] = 6 
AND [PensionSchemeId] IS NULL
AND [PensionableEmploymentStartDate] BETWEEN ' 06 apr 1995' AND '31 mar 2004'
--388

UPDATE a
SET PensionSchemeId = 3
FROM [DeferredsDataUpdates] a
WHERE [OriginalPensionSchemeId] = 6 
AND [PensionSchemeId] IS NULL
AND [PensionableEmploymentStartDate] BETWEEN ' 01 apr 2004' AND '31 dec 2012'
--1235

UPDATE a
SET PensionSchemeId = 4
FROM [DeferredsDataUpdates] a
WHERE [OriginalPensionSchemeId] = 6 
AND [PensionSchemeId] IS NULL
AND [PensionableEmploymentStartDate] >= ' 01 jan 2013'
--25


INSERT INTO [dbo].[DeferredsDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[PensionableEmploymentStartDate]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalPreservedPensionAge]
			   ,[PreservedPensionAge]
			   ,[OriginalPensionableRemunerationFuturePensionBasedOn]
			   ,[PensionableRemunerationFuturePensionBasedOn]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate,  a.GenderId, NULL, a.PensionSchemeId, NULL, a.PreservedPensionAge, NULL, a.PensionableRemunerationFuturePensionBasedOn, NULL, GETDATE()
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE (PreservedPensionAge IS NULL OR PreservedPensionAge <= 0)
AND PPSN NOT IN (SELECT sub.PPSN FROM [DeferredsDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)

UPDATE a
SET [PreservedPensionAge] = 60
FROM [DeferredsDataUpdates] a
WHERE ([OriginalPreservedPensionAge] IS NULL OR [OriginalPreservedPensionAge] = 0)
AND ([OriginalPensionSchemeId] IN (1,2) OR [PensionSchemeId] IN (1,2))
AND [OriginalPreservedPensionAge] <= 0
AND [PreservedPensionAge] IS NULL
--229

UPDATE a
SET [PreservedPensionAge] = 65
FROM [DeferredsDataUpdates] a
WHERE ([OriginalPreservedPensionAge] IS NULL OR [OriginalPreservedPensionAge] = 0)
AND ([OriginalPensionSchemeId] IN (3) OR [PensionSchemeId] IN (3))
AND [OriginalPreservedPensionAge] <= 0
AND [PreservedPensionAge] IS NULL
--0

UPDATE a
SET [PreservedPensionAge] = 66
FROM [DeferredsDataUpdates] a
WHERE ([OriginalPreservedPensionAge] IS NULL OR [OriginalPreservedPensionAge] = 0)
AND ([OriginalPensionSchemeId] IN (4) OR [PensionSchemeId] IN (4))
AND [OriginalPreservedPensionAge] <= 0
AND DateOfBirth < '1 jan 1955'
AND [PreservedPensionAge] IS NULL
--0

UPDATE a
SET [PreservedPensionAge] = 67
FROM [DeferredsDataUpdates] a
WHERE ([OriginalPreservedPensionAge] IS NULL OR [OriginalPreservedPensionAge] = 0)
AND ([OriginalPensionSchemeId] IN (4) OR [PensionSchemeId] IN (4))
AND [OriginalPreservedPensionAge] <= 0
AND DateOfBirth >= '1 jan 1955' and DateOfBirth <  '31 dec 1960'
AND [PreservedPensionAge] IS NULL
--0

UPDATE a
SET [PreservedPensionAge] = 68
FROM [DeferredsDataUpdates] a
WHERE ([OriginalPreservedPensionAge] IS NULL OR [OriginalPreservedPensionAge] <= 0)
AND ([OriginalPensionSchemeId] IN (4) OR [PensionSchemeId] IN (4))
AND [OriginalPreservedPensionAge] <= 0
AND DateOfBirth >= '1 jan 1961'
AND [PreservedPensionAge] IS NULL
--2

UPDATE a
SET [PreservedPensionAge] = 65
FROM [DeferredsDataUpdates] a
WHERE ([OriginalPreservedPensionAge] IS NULL OR [PreservedPensionAge] = 0)
AND ([OriginalPensionSchemeId] IN (5) OR [PensionSchemeId] IN (5))
AND [OriginalPreservedPensionAge] = 0
AND [PreservedPensionAge] = 0

INSERT INTO [dbo].[DeferredsDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,PensionableEmploymentStartDate
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalPreservedPensionAge]
			   ,PreservedPensionAge
			   ,[OriginalPensionableRemunerationFuturePensionBasedOn]
			   ,[PensionableRemunerationFuturePensionBasedOn]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, a.GenderId, NULL, a.PensionSchemeId, NULL, a.PreservedPensionAge, a.PreservedPensionAge, a.PensionableRemunerationFuturePensionBasedOn, NULL, GETDATE()
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') > PreservedPensionAge
AND PPSN NOT IN (SELECT sub.PPSN FROM [DeferredsDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)
--5178

UPDATE [DeferredsDataUpdates]
SET PreservedPensionAge = OriginalPreservedPensionAge
WHERE PreservedPensionAge IS NULL

UPDATE [DeferredsDataUpdates]
SET PreservedPensionAge = DATEDIFF(YEAR, DateOfBirth, '31 dec 2019')
WHERE DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') > PreservedPensionAge
--4949

INSERT INTO [dbo].[DeferredsDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[PensionableEmploymentStartDate]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalPensionSchemeId]
			   ,[PensionSchemeId]
			   ,[OriginalPreservedPensionAge]
			   ,[PreservedPensionAge]
			   ,[OriginalPensionableRemunerationFuturePensionBasedOn]
			   ,[PensionableRemunerationFuturePensionBasedOn]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, a.GenderId, NULL, a.PensionSchemeId, NULL, a.PreservedPensionAge, NULL, a.PensionableRemunerationFuturePensionBasedOn, NULL, GETDATE()
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE ([PensionableRemunerationFuturePensionBasedOn] IS NULL OR [PensionableRemunerationFuturePensionBasedOn] <=0)
AND PPSN NOT IN (SELECT sub.PPSN FROM [DeferredsDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)

UPDATE a
SET PensionableRemunerationFuturePensionBasedOn = CAST(Value AS DECIMAL(10,2))
FROM [DeferredsDataUpdates] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE Scheme = 'Deferreds' AND Property = 'PensionableRemunerationFuturePensionBasedOn'
AND ([OriginalPensionableRemunerationFuturePensionBasedOn] IS NULL OR [OriginalPensionableRemunerationFuturePensionBasedOn] <=0)

