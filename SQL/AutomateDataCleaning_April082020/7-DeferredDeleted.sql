USE [AccruedLiability]
GO

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'DeferredsDeleted'))
BEGIN

	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[DeferredsDeleted](
		[VersionId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] [varchar](20) NOT NULL,
		[DateOfBirth] [datetime] NULL,
		[GenderId] [int] NOT NULL,
		[CivilStatusId] [int] NULL,
		[PensionSchemeId] [int] NOT NULL,
		[PreservedPensionAge] [int] NOT NULL,
		[PrsiClass] [nvarchar](100) NOT NULL,
		[FTE] [decimal](10, 2) NOT NULL,
		[PensionableEmploymentStartDate] [datetime] NOT NULL,
		[PensionableEmploymentLeaveDate] [datetime] NOT NULL,
		[PensionableRemunerationFuturePensionBasedOn] [decimal](10, 2) NOT NULL,
		[PensionableServiceFuturePensionBasedOn] [decimal](10, 2) NULL,
		[CreateDate] [datetime] NOT NULL,
		[ModifyDate] [datetime] NOT NULL
	) ON [PRIMARY]
END

SELECT * FROM DeferredsDeleted

