use AccruedLiability
GO

;WITH AverageSalaryBySector AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	AVG(PensionableRemunerationFuturePensionBasedOn) AvgPay
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE (NOT PensionableRemunerationFuturePensionBasedOn IS NULL) AND PensionableRemunerationFuturePensionBasedOn > 0
GROUP BY s.SectorId, SectorName
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Deferreds', 'PensionableRemunerationFuturePensionBasedOn',CAST(AvgPay AS DECIMAL(18,2)) FROM AverageSalaryBySector 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Deferreds' AND Property = 'PensionableRemunerationFuturePensionBasedOn')


select * from Deferreds where PensionableRemunerationFuturePensionBasedOn > 0 -- 56869, 42808
select * from Deferreds where PensionableRemunerationFuturePensionBasedOn is null



select * From Deferreds