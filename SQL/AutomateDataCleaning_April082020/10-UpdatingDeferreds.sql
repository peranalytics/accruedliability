USE AccruedLiability

UPDATE [DeferredsDataUpdates]
SET GenderId = OriginalGenderId
WHERE GenderId is null

UPDATE [DeferredsDataUpdates]
SET PensionSchemeId = OriginalPensionSchemeId
WHERE PensionSchemeId is null

UPDATE [DeferredsDataUpdates]
SET PreservedPensionAge = OriginalPreservedPensionAge
WHERE PreservedPensionAge is null

UPDATE [DeferredsDataUpdates]
SET PensionableRemunerationFuturePensionBasedOn = OriginalPensionableRemunerationFuturePensionBasedOn
WHERE PensionableRemunerationFuturePensionBasedOn is null

UPDATE a
SET a.GenderId = u.GenderId
FROM Deferreds a
INNER JOIN [DeferredsDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.GenderId = u.OriginalGenderId
AND NOT u.GenderId = u.OriginalGenderId

UPDATE a
SET a.PensionSchemeId = u.PensionSchemeId
FROM Deferreds a
INNER JOIN [DeferredsDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionSchemeId = u.OriginalPensionSchemeId
AND NOT u.PensionSchemeId = u.OriginalPensionSchemeId

UPDATE a
SET a.PreservedPensionAge = u.PreservedPensionAge
FROM Deferreds a
INNER JOIN [DeferredsDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PreservedPensionAge = u.OriginalPreservedPensionAge
AND NOT u.PreservedPensionAge = u.OriginalPreservedPensionAge

UPDATE a
SET a.PensionableRemunerationFuturePensionBasedOn = u.PensionableRemunerationFuturePensionBasedOn
FROM Deferreds a
INNER JOIN [DeferredsDataUpdates] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionableRemunerationFuturePensionBasedOn = u.OriginalPensionableRemunerationFuturePensionBasedOn
AND NOT u.PensionableRemunerationFuturePensionBasedOn = u.OriginalPensionableRemunerationFuturePensionBasedOn

-- DO SOMETHING ABOUT DUPLICATES
/*

SELECT 
a.ppsn, count(a.ppsn)
FROM Deferreds a
INNER JOIN [DeferredsDataUpdates] u
ON a.PPSN = u.PPSN
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
group by a.ppsn
having count(a.ppsn) > 1

SELECT * FROM [DeferredsDataUpdates] where ppsn = '7875076E'

select * from RelevantAuthority where RelevantAuthorityId = 139



select * from Deferreds where ppsn in (
SELECT 
 a.ppsn
FROM Deferreds a
group by a.relevantauthorityId, a.ppsn
having count(a.ppsn) > 1
)
*/
