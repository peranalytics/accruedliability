USE [AccruedLiability]
GO

--DROP TABLE ActivesDataUpdates


IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'ActivesDataUpdates'))
BEGIN

	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[ActivesDataUpdates](
		[VersionId] [int] NOT NULL,
		[SectorId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] [varchar](20) NOT NULL,
		DateOfBirth DATETIME NULL,
		DateOfEntryIntoScheme DATETIME NOT NULL,
		[OriginalGenderId] [int] NOT NULL,
		[GenderId] [int] NULL,
		[OriginalPensionSchemeId] [int] NOT NULL,
		[PensionSchemeId] [int] NULL,
		[OriginalMinimumNormalRetirementAge] [int] NOT NULL,
		[MinimumNormalRetirementAge] [int] NULL,
		[OriginalAnnualPensionablePayFTE] [decimal](10, 2) NOT NULL,
		[AnnualPensionablePayFTE] [decimal](10, 2) NULL,
		[ModifyDate] [datetime] NOT NULL DEFAULT GETDATE()
	) ON [PRIMARY]

END


SELECT * FROM [ActivesDataUpdates]