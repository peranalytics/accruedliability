USE AccruedLiability

IF NOT EXISTS (SELECT 1 FROM [PaymentsDataUpdates])
BEGIN
	INSERT INTO [dbo].[PaymentsDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[PensionCommencementDate]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalTypeOfBeneficiaryId]
			   ,[TypeOfBeneficiaryId]
			   ,[OriginalAnnualPensionValue]
			   ,[AnnualPensionValue]
			   ,[ModifyDate])
	SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate, a.GenderId, NULL, a.TypeOfBeneficiaryId, NULL, a.AnnualPensionValue, NULL, GETDATE()
	FROM Payments a
	INNER JOIN RelevantAuthoritySectors ras
	ON a.RelevantAuthorityId = ras.RelevantAuthorityId
	WHERE GenderId = 2
END
GO


UPDATE a
SET [GenderId] = CAST(VAlue AS INT)
FROM [PaymentsDataUpdates] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE Scheme = 'Payments' AND Property = 'Gender'
AND GenderId IS NULL
AND [OriginalGenderId] = 2


INSERT INTO [dbo].[PaymentsDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[PensionCommencementDate]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalTypeOfBeneficiaryId]
			   ,[TypeOfBeneficiaryId]
			   ,[OriginalAnnualPensionValue]
			   ,[AnnualPensionValue]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate,  a.GenderId, NULL, a.TypeOfBeneficiaryId, NULL, a.AnnualPensionValue, NULL, GETDATE()
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE TypeOfBeneficiaryId = 5
AND PPSN NOT IN (SELECT sub.PPSN FROM [PaymentsDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)
--42 unknowns

UPDATE a
SET TypeOfBeneficiaryId = 1 -- MEMBER
FROM [PaymentsDataUpdates] a
WHERE [OriginalTypeOfBeneficiaryId] = 5
AND [TypeOfBeneficiaryId] IS NULL
AND DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') >= 22
--42

UPDATE a
SET TypeOfBeneficiaryId = 4 -- Children
FROM [PaymentsDataUpdates] a
WHERE [OriginalTypeOfBeneficiaryId] = 5
AND [TypeOfBeneficiaryId] IS NULL
AND DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') < 22



INSERT INTO [dbo].[PaymentsDeleted]
           ([VersionId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[GenderId]
           ,[CivilStatusId]
           ,[PensionSchemeId]
           ,[TypeOfBeneficiaryId]
           ,[PensionCommencementDate]
           ,[AnnualPensionValue]
           ,[AnnualSupplementaryPension]
           ,[BasisPensionCommencedId]
           ,[PensionableServiceYears]
           ,[ActualEmploymentBasedServiceYears]
           ,[PensionableServiceDifferenceReasons]
           ,[BasicPayValuePensionableRemuneration]
           ,[PensionableAllowancesPensionableRemuneration]
           ,[HowMuchAbatementIfAny]
           ,[CreateDate]
           ,[ModifyDate])

SELECT [VersionId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
      ,[DateOfBirth]
      ,[GenderId]
      ,[CivilStatusId]
      ,[PensionSchemeId]
      ,[TypeOfBeneficiaryId]
      ,[PensionCommencementDate]
      ,[AnnualPensionValue]
      ,[AnnualSupplementaryPension]
      ,[BasisPensionCommencedId]
      ,[PensionableServiceYears]
      ,[ActualEmploymentBasedServiceYears]
      ,[PensionableServiceDifferenceReasons]
      ,[BasicPayValuePensionableRemuneration]
      ,[PensionableAllowancesPensionableRemuneration]
      ,[HowMuchAbatementIfAny]
      ,[CreateDate]
      ,[ModifyDate]
FROM [dbo].[Payments] a
WHERE [PensionCommencementDate] > '31 dec 2018'
AND PPSN NOT in (SELECT sub.PPSN FROM PaymentsDeleted sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId])


INSERT INTO [dbo].[PaymentsDataUpdates]
			   ([VersionId]
			   ,[SectorId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[PensionCommencementDate]
			   ,[OriginalGenderId]
			   ,[GenderId]
			   ,[OriginalTypeOfBeneficiaryId]
			   ,[TypeOfBeneficiaryId]
			   ,[OriginalAnnualPensionValue]
			   ,[AnnualPensionValue]
			   ,[ModifyDate])
SELECT VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate, a.GenderId, NULL, a.TypeOfBeneficiaryId, NULL, a.AnnualPensionValue, NULL, GETDATE()
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE ([AnnualPensionValue] IS NULL OR [AnnualPensionValue] <=0)
AND PPSN NOT IN (SELECT sub.PPSN FROM [PaymentsDataUpdates] sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId] AND sub.DateOfBirth = a.DateOfBirth)


UPDATE a
SET AnnualPensionValue = CAST(Value AS DECIMAL(10,2))
FROM [PaymentsDataUpdates] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE Scheme = 'Payments' AND Property = 'AnnualPensionValue'
AND ([OriginalAnnualPensionValue] IS NULL OR [OriginalAnnualPensionValue] <=0)

SELECT * FROM [PaymentsDataUpdates]