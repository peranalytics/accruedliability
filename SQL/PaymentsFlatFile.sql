USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [PaymentsFlatFile]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaymentsFlatFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PaymentsFlatFile](
	[RelevantAuthorityId] [int] NOT NULL,
	[RelevantAuthorityName] [nvarchar](255) NOT NULL,
	[SectorId] [int] NOT NULL,
	[SectorName] [nvarchar](250) NULL,
	[SubsectorId] [int] NOT NULL,
	[SubsectorName] [nvarchar](250) NULL,
	[YearId] [int] NOT NULL,
	[PPSN] [varchar](20) NOT NULL,
	[DateOfBirth] [datetime] NULL,
	[Gender] [nvarchar](50) NOT NULL,
	[CivilStatus] [nvarchar](50) NOT NULL,
	[PensionScheme] [nvarchar](50) NOT NULL,
	[BeneficiaryType] [nvarchar](100) NOT NULL,
	[PensionCommencementDate] [datetime] NOT NULL,
	[AnnualPensionValue] [decimal](10, 2) NOT NULL,
	[AnnualSupplementaryPension] [decimal](10, 2) NOT NULL,
	[PensionBasis] [nvarchar](50) NOT NULL,
	[PensionableServiceYears] [decimal](10, 2) NULL,
	[ActualEmploymentBasedServiceYears] [decimal](10, 2) NULL,
	[PensionableServiceDifferenceReasons] [nvarchar](250) NULL,
	[BasicPayValuePensionableRemuneration] [decimal](10, 2) NULL,
	[PensionableAllowancesPensionableRemuneration] [decimal](10, 2) NULL,
	[HowMuchAbatementIfAny] [nvarchar](250) NULL
	) ON [PRIMARY]
END
GO


