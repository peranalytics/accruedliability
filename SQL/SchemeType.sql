/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SchemeType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SchemeType](
	[SchemeType] [nvarchar](50) NOT NULL
) ON [PRIMARY]
END
GO

INSERT INTO SchemeType
SELECT 'Actives' UNION ALL
SELECT 'Deferreds' UNION ALL
SELECT 'Payments' 
