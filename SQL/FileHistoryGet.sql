USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[FileHistoryGet]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Deletes the staging tables
-- =============================================
CREATE PROCEDURE [dbo].[FileHistoryGet] 
	
AS

BEGIN
	
SELECT [VersionId]
      ,[ProviderId]
      ,[RelevantAuthorityId]
      ,[StageId]
      ,[Valid]
      ,[OriginalFileName]
      ,[ArchivedFileName]
      ,[ModifyDate]
      ,[CreateDate]
  FROM [dbo].[FileHistory]

END
GO

GRANT EXECUTE ON [dbo].[FileHistoryGet] TO [budgetviewer] AS [dbo]
GO


