
USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[PrsiClassGetAll] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 25/07/2019
-- Description:	Get the mapping lookup data
-- =============================================
CREATE PROCEDURE [dbo].[PrsiClassGetAll] 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, MappingPrsiClass FROM [dbo].MappingPrsiClass
END
GO

GRANT EXECUTE ON [dbo].[PrsiClassGetAll] TO AccruedLiabilityViewer AS [dbo]
GO


