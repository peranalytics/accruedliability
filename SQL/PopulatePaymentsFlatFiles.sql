USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS PopulatePaymentsFlatFiles
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Populate flat files
-- =============================================
CREATE PROCEDURE PopulatePaymentsFlatFiles
AS
BEGIN
	
	SET NOCOUNT ON;

	DELETE FROM [PaymentsFlatFile]

	INSERT INTO [dbo].[PaymentsFlatFile]
           ([RelevantAuthorityId]
           ,[RelevantAuthorityName]
           ,[SectorId]
           ,[SectorName]
           ,[SubsectorId]
           ,[SubsectorName]
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[Gender]
           ,[CivilStatus]
           ,[PensionScheme]
           ,[BeneficiaryType]
           ,[PensionCommencementDate]
           ,[AnnualPensionValue]
           ,[AnnualSupplementaryPension]
           ,[PensionBasis]
           ,[PensionableServiceYears]
           ,[ActualEmploymentBasedServiceYears]
           ,[PensionableServiceDifferenceReasons]
           ,[BasicPayValuePensionableRemuneration]
           ,[PensionableAllowancesPensionableRemuneration]
           ,[HowMuchAbatementIfAny])
	SELECT
      p.[RelevantAuthorityId],
	  ra.RelevantAuthorityName,
	  secs.SectorId,
	  secs.SectorName,
	  subsecs.SubsectorId,
	  subsecs.SubsectorName
      ,[YearId]
      ,[PPSN]
      ,[DateOfBirth]
      ,[Gender]
      ,[CivilStatus]
      ,[PensionScheme]
      ,BeneficiaryType
      ,[PensionCommencementDate]
      ,[AnnualPensionValue]
      ,[AnnualSupplementaryPension]
      ,PensionBasis
      ,[PensionableServiceYears]
      ,[ActualEmploymentBasedServiceYears]
      ,[PensionableServiceDifferenceReasons]
      ,[BasicPayValuePensionableRemuneration]
      ,[PensionableAllowancesPensionableRemuneration]
      ,[HowMuchAbatementIfAny]
  FROM [dbo].[Payments] p
  INNER JOIN RelevantAuthoritySectors ras WITH(NOLOCK)
	ON ras.RelevantAuthorityId = p.RelevantAuthorityId
	INNER JOIN RelevantAuthority ra WITH(NOLOCK)
	ON ra.RelevantAuthorityId = p.RelevantAuthorityId
	INNER JOIN Sectors secs WITH(NOLOCK)
	ON secs.SectorId = ras.SectorId
	INNER JOIN Subsectors subsecs WITH(NOLOCK)
	ON subsecs.SubsectorId = ras.SubsectorId
	AND subsecs.SectorId = ras.SectorId
	INNER JOIN Gender g WITH(NOLOCK) 
	ON p.GenderId = g.Id
	INNER JOIN CivilStatus cs WITH(NOLOCK) 
	ON p.CivilStatusId = cs.Id
	INNER JOIN PensionScheme ps WITH(NOLOCK) 
	ON p.PensionSchemeId = ps.Id
	INNER JOIN BeneficiaryType bt WITH(NOLOCK) 
	ON p.TypeOfBeneficiaryId = bt.Id
	INNER JOIN PensionBasis pb WITH(NOLOCK) 
	ON p.BasisPensionCommencedId = pb.Id

END
GO

GRANT EXECUTE ON PopulatePaymentsFlatFiles TO AccruedLiabilityViewer 
