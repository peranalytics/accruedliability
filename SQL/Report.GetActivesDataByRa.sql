USE [AccruedLiability]
GO

DROP PROCEDURE IF EXISTS [Report].[GetActivesDataByRa]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load deferreds to the table
-- =============================================
CREATE PROCEDURE [Report].[GetActivesDataByRa]
	@UserId INT,
	@Year INT,
	@RaList NVARCHAR(max) = '-1'
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT CAST(Value AS INT) AS RaId
	INTO #RaList FROM STRING_SPLIT(@RaList, ',')  
	WHERE RTRIM(value) <> '';


	DECLARE @DetailAccess INT
	SELECT @DetailAccess = DetailAccess FROM Security.Roles r INNER JOIN Security.UserRoles ur ON ur.RoleId = r.RoleId WHERE ur.UserId = @UserId

		
	SELECT 
		(CASE WHEN @DetailAccess = 1 THEN PPSN ELSE '--------' END) AS PPSN,
		YearId,
		DateOfBirth,
		Gender,
		CivilStatus,
		PensionScheme,
		DateOfEntryIntoScheme,
		MinimumNormalRetirementAge,
		PrsiClass,
		AnnualPensionablePayFTE,
		LengthOfService,
		FTE,
		Grade,
		ScalePoint,
		IncrementDate,
		MandatoryRetirementAge,
		AnnualBasicPensionablePayFTE
	FROM ActivesFlatFile a  WITH(NOLOCK)
	WHERE (@RaList = '-1') OR  RelevantAuthorityId IN (SELECT sub.RaId FROm #RaList sub) AND
   		  (@Year = YearId) 



	DROP TABLE #RaList

END
GO

GRANT EXECUTE ON [Report].[GetActivesDataByRa] TO accruedLiabilityviewer AS [dbo]
GO


