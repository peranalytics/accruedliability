USE AccruedLiability
GO
DELETE FROM [CleaningLogTemporary]
-- SETTING PENSION VALUES

-- CREATEING BACKUPS
 INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'PensionScheme', PensionSchemeId, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionSchemeId = 6
--9389

UPDATE a
SET [UpdatedValue] = 1
FROM [CleaningLogTemporary] a
WHERE [OriginalValue] = 6 
AND [UpdatedValue] IS NULL
AND [DateOfEntryIntoScheme] <= '05 apr 1995'
--1486

UPDATE a
SET [UpdatedValue] = 2
FROM [CleaningLogTemporary] a
WHERE [OriginalValue] = 6 
AND [UpdatedValue] IS NULL
AND [DateOfEntryIntoScheme] BETWEEN ' 06 apr 1995' AND '31 mar 2004'
--2705

UPDATE a
SET [UpdatedValue] = 3
FROM [CleaningLogTemporary] a
WHERE [OriginalValue] = 6 
AND [UpdatedValue] IS NULL
AND [DateOfEntryIntoScheme] BETWEEN ' 01 apr 2004' AND '31 dec 2012'
--3936

UPDATE a
SET [UpdatedValue] = 4
FROM [CleaningLogTemporary] a
WHERE [OriginalValue] = 6 
AND [UpdatedValue] IS NULL
AND [DateOfEntryIntoScheme] >= ' 01 jan 2013'
--1262

--UPDATING LIVE FILE

UPDATE a
SET a.PensionSchemeId = CAST(u.[UpdatedValue] AS INT)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionSchemeId = CAST(u.[OriginalValue] AS INT)


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]

SELECT * FROM [CleaningLogFull]