USE AccruedLiability

DELETE FROM [CleaningLogTemporary]

 INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'DateofBirth', DateOfBirth, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE NOT PensionSchemeId = 10
AND DATEDIFF(Year, DateOfBirth, '31 dec 2018') > 70
--1198


UPDATE u
SET [UpdatedValue] = CAST(dc.value AS DATETIME)
FROM CleaningLogTemporary u
INNER JOIN Actives a
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.DateOfEntryIntoScheme = u.DateOfEntryIntoScheme
INNER JOIN DataCleaningValuesBySector dc
ON  CAST(a.PensionSchemeId AS nvarchar(100)) = dc.Property
AND u.SectorId = dc.SectorId
WHERE dc.Scheme = 'Actives'


UPDATE a
SET a.DateOfBirth = CAST(u.[UpdatedValue] AS datetime)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.DateOfEntryIntoScheme = u.DateOfEntryIntoScheme
AND a.DateOfBirth = CAST(u.[OriginalValue] AS datetime)


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]



DELETE FROM CleaningLogTemporary where Property = 'DateOfEntryIntoScheme'


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'DateOfEntryIntoScheme', DateOfEntryIntoScheme, NULL
FROM [CleaningLogTemporary] a
WHERE DATEDIFF(Year, CAST(UpdatedValue AS DATETIME), DateOfEntryIntoScheme) < 20
--221


UPDATE [CleaningLogTemporary] 
SET [UpdatedValue] = DATEADD(Year, 20, DateOfBirth)
WHERE Property = 'DateOfEntryIntoScheme'
--221

DELETE FROM CleaningLogTemporary where Property = 'DateofBirth'

UPDATE a
SET a.DateOfEntryIntoScheme = CAST(u.[UpdatedValue] AS datetime)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.DateOfEntryIntoScheme = CAST(u.[OriginalValue] AS datetime)
--221


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]

