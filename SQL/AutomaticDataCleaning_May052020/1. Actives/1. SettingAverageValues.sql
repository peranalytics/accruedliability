USE AccruedLiability

--select *From Actives --201329

--delete from [DataCleaningValuesBySector]

;WITH RankedGenders AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	Gender, 
	GenderId,
	COUNT(GenderId) AS Count,
	RANK() OVER (PARTITION BY s.SectorId, SectorName ORDER BY COUNT(GenderId) DESC) AS Rank  
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE NOT Gender = 'Unknown'
GROUP BY s.SectorId, SectorName, GenderId, Gender
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Actives', 'Gender', CAST(GenderId AS NVARCHAR(100)) FROM RankedGenders 
WHERE Rank = 1 AND SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Actives' AND Property = 'Gender')


;WITH AverageSalaryBySector AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	AVG(AnnualPensionablePayFTE) AvgPay
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE (NOT AnnualPensionablePayFTE IS NULL) AND AnnualPensionablePayFTE > 0
GROUP BY s.SectorId, SectorName
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Actives', 'AnnualPensionablePayFTE',CAST(AvgPay AS DECIMAL(18,2)) FROM AverageSalaryBySector 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Actives' AND Property = 'AnnualPensionablePayFTE')


;WITH RankedGenders AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	Gender, 
	GenderId,
	COUNT(GenderId) AS Count,
	RANK() OVER (PARTITION BY s.SectorId, SectorName ORDER BY COUNT(GenderId) DESC) AS Rank  
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE NOT Gender = 'Unknown'
GROUP BY s.SectorId, SectorName, GenderId, Gender
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Deferreds', 'Gender', CAST(GenderId AS NVARCHAR(100)) FROM RankedGenders 
WHERE Rank = 1 AND SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Deferreds' AND Property = 'Gender')


;WITH AverageSalaryBySector AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	AVG(PensionableRemunerationFuturePensionBasedOn) AvgPay
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE (NOT PensionableRemunerationFuturePensionBasedOn IS NULL) AND PensionableRemunerationFuturePensionBasedOn > 0
GROUP BY s.SectorId, SectorName
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Deferreds', 'PensionableRemunerationFuturePensionBasedOn',CAST(AvgPay AS DECIMAL(18,2)) FROM AverageSalaryBySector 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Deferreds' AND Property = 'PensionableRemunerationFuturePensionBasedOn')


;WITH RankedGenders AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	Gender, 
	GenderId,
	COUNT(GenderId) AS Count,
	RANK() OVER (PARTITION BY s.SectorId, SectorName ORDER BY COUNT(GenderId) DESC) AS Rank  
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE NOT Gender = 'Unknown'
GROUP BY s.SectorId, SectorName, GenderId, Gender
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Payments', 'Gender', CAST(GenderId AS NVARCHAR(100)) FROM RankedGenders 
WHERE Rank = 1 AND SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Payments' AND Property = 'Gender')


;WITH AverageSalaryBySector AS
(SELECT 
	s.SectorId, 
	s.SectorName, 
	AVG(AnnualPensionValue) AvgPay
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
INNER JOIN Gender g
ON g.Id = a.GenderId
WHERE (NOT AnnualPensionValue IS NULL) AND AnnualPensionValue > 0
GROUP BY s.SectorId, SectorName
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Payments', 'AnnualPensionValue',CAST(AvgPay AS DECIMAL(18,2)) FROM AverageSalaryBySector 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Payments' AND Property = 'AnnualPensionValue')


;WITH AverageAgeBySectorAndScheme AS
(SELECT 
	s.SectorId, 
	s.SectorName,
	a.PensionSchemeId, 
	CAST(AVG(CAST(DateOfBirth AS INT)) AS DATETIME) AS AvgDOB
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
WHERE NOT PensionSchemeId = 10 -- EXCLUDE MINISTER PENSION
GROUP BY s.SectorId, SectorName, PensionSchemeId
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Actives', CAST(PensionSchemeId AS nvarchar(10)) AS Prop, CAST(AvgDOB AS DATE) FROM AverageAgeBySectorAndScheme 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Actives' AND Property = CAST(PensionSchemeId AS nvarchar(10)))
ORDER BY SectorId,Prop


;WITH AverageAgeBySectorAndScheme AS
(SELECT 
	s.SectorId, 
	s.SectorName,
	a.PensionSchemeId, 
	CAST(AVG(CAST(DateOfBirth AS INT)) AS DATETIME) AS AvgDOB
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
INNER JOIN Sectors s
ON ras.SectorId = s.SectorId
WHERE NOT PensionSchemeId = 10 -- EXCLUDE MINISTER PENSION
GROUP BY s.SectorId, SectorName, PensionSchemeId
)

INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Deferreds', CAST(PensionSchemeId AS nvarchar(10)) AS Prop, CAST(AvgDOB AS DATE) FROM AverageAgeBySectorAndScheme 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Deferreds' AND Property = CAST(PensionSchemeId AS nvarchar(10)))
ORDER BY SectorId,Prop


;WITH DistinctDates AS
(
	SELECT  DISTINCT ((CAST(PensionCommencementDate AS INT)))AS PensionCommencementDate,
	SectorId
	FROM Payments  a
	INNER JOIN RelevantAuthoritySectors ras
	ON a.RelevantAuthorityId = ras.RelevantAuthorityId
	WHERE PensionCommencementDate < GETDATE() AND PensionCommencementDate > '01 jan 1901'
)
INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Payments', 'AvgPensionCommencementDate', CAST(CAST(AVG(PensionCommencementDate) AS DATETIME) AS DATE) FROM DistinctDates 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Payments' AND Property = 'AvgPensionCommencementDate')
GROUP BY SectorId


;WITH DistinctDates AS
(
	SELECT  DISTINCT ((CAST(PensionableEmploymentStartDate AS INT)))AS PensionableEmploymentStartDate,
	SectorId
	FROM Deferreds  a
	INNER JOIN RelevantAuthoritySectors ras
	ON a.RelevantAuthorityId = ras.RelevantAuthorityId
	WHERE PensionableEmploymentStartDate < GETDATE() AND PensionableEmploymentStartDate > '01 jan 1901'
)
INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Deferreds', 'AvgPensionableEmploymentStartDate', CAST(CAST(AVG(PensionableEmploymentStartDate) AS DATETIME) AS DATE) FROM DistinctDates 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Deferreds' AND Property = 'AvgPensionableEmploymentStartDate')
GROUP BY SectorId


;WITH DistinctDates AS
(
	SELECT  DISTINCT ((CAST(PensionableEmploymentLeaveDate AS INT)))AS PensionableEmploymentLeaveDate,
	SectorId
	FROM Deferreds  a
	INNER JOIN RelevantAuthoritySectors ras
	ON a.RelevantAuthorityId = ras.RelevantAuthorityId
	WHERE PensionableEmploymentLeaveDate < GETDATE() AND PensionableEmploymentLeaveDate > '01 jan 1901'
)
INSERT INTO [DataCleaningValuesBySector](SectorId,Scheme, Property,[Value])
SELECT SectorId, 'Deferreds', 'AvgPensionableEmploymentLeaveDate', CAST(CAST(AVG(PensionableEmploymentLeaveDate) AS DATETIME) AS DATE) FROM DistinctDates 
WHERE  SectorId NOT IN (SELECT sub.SectorId FROM [DataCleaningValuesBySector] Sub WHERE Scheme = 'Deferreds' AND Property = 'AvgPensionableEmploymentLeaveDate')
GROUP BY SectorId



select * from [DataCleaningValuesBySector] ORDER BY Scheme, Property, SectorId
--delete FROM [DataCleaningValuesBySector]
