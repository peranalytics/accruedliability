USE AccruedLiability

--NRAs
DELETE FROM [CleaningLogTemporary]

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'MinimumNormalRetirementAge', MinimumNormalRetirementAge, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE (MinimumNormalRetirementAge IS NULL OR MinimumNormalRetirementAge = 0)
--160 Unknown nras


UPDATE u
SET [UpdatedValue] = 60
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (1,2)
AND [UpdatedValue] IS NULL
--2


UPDATE u
SET [UpdatedValue] = 65
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (3)
AND [UpdatedValue] IS NULL
--4

UPDATE u
SET [UpdatedValue] = 66
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (4)
AND u.DateOfBirth < '1 jan 1955'
AND [UpdatedValue] IS NULL
--0


UPDATE u
SET [UpdatedValue] = 67
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (4)
AND u.DateOfBirth >= '1 jan 1955' and u.DateOfBirth <  '31 dec 1960'
AND [UpdatedValue] IS NULL
--0


UPDATE u
SET [UpdatedValue] = 68
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (4)
AND u.DateOfBirth >= '1 jan 1961'
AND [UpdatedValue] IS NULL
--4


UPDATE u
SET [UpdatedValue] = 65
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND (u.DateOfBirth IS NULL OR  a.DateOfBirth = u.DateOfBirth)
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (5)
AND [UpdatedValue] IS NULL
--150 non established


--UPDATING LIVE FILE
UPDATE a
SET a.MinimumNormalRetirementAge = CAST(u.[UpdatedValue] AS INT)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.MinimumNormalRetirementAge = CAST(u.[OriginalValue] AS INT)

INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]

SELECT * FROM [CleaningLogFull]

