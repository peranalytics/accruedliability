USE AccruedLiability
GO

DELETE FROM [CleaningLogTemporary]

-- CIVIL SERVICE
INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE NOT a.RelevantAuthorityId IN (378,149,189) AND SectorId = 1 AND LengthOfService <= 45
--1306

UPDATE [CleaningLogTemporary]
SET [UpdatedValue] = (DATEDIFF(DAY, DateOfEntryIntoScheme, '31 Dec 2018')/365.25)
WHERE [UpdatedValue] IS NULL

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE NOT a.RelevantAuthorityId IN (378,149,189) AND SectorId = 1 AND LengthOfService > 45
--32395

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE SectorId = 6 and a.RelevantAuthorityId IN (237,242,245,264,266,286,292,339,340,368)
--2075

UPDATE [CleaningLogTemporary]
SET UpdatedValue = CAST([OriginalValue] AS DECIMAL(10,2)) / 365.25
WHERE [UpdatedValue] IS NULL


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE SectorId = 6 and a.RelevantAuthorityId IN (188) AND PPSN = '3633741C'

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE SectorId = 6 and a.RelevantAuthorityId IN (271) AND PPSN = '7767680Q'

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE SectorId = 3 and a.RelevantAuthorityId IN (50) AND PPSN = '8944875D'

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE SectorId = 2 and a.RelevantAuthorityId IN (234) AND PPSN = '5358560H'

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE SectorId = 5 and a.RelevantAuthorityId IN (130) AND PPSN = '2079797K'

UPDATE [CleaningLogTemporary]
SET [UpdatedValue] = (DATEDIFF(DAY, DateOfEntryIntoScheme, '31 Dec 2018')/365.25)
WHERE [UpdatedValue] IS NULL


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE SectorId = 4 and a.RelevantAuthorityId IN (518) 
--3028 / Garda Civilians

UPDATE [CleaningLogTemporary]
SET UpdatedValue = CAST([OriginalValue] AS DECIMAL(10,2)) / 365.25
WHERE [UpdatedValue] IS NULL


UPDATE a
SET a.LengthOfService = CAST(u.[UpdatedValue] AS DECIMAL(10,2))
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.LengthOfService = CAST(u.[OriginalValue] AS DECIMAL(10,2))


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]


DELETE FROM [CleaningLogTemporary]


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'DateOfEntryIntoScheme', DateOfEntryIntoScheme, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE DateOfEntryIntoScheme < DATEADD(YEAR, 15, DateOfBirth)
--5560


UPDATE u
SET UpdatedValue =  DATEADD(YEAR, -LengthOfService, '31 Dec 2018')
FROM [CleaningLogTemporary] u 
INNER JOIN Actives a
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.DateOfEntryIntoScheme = CAST(u.[OriginalValue] AS datetime)
AND NOT LengthOfService IS NULL AND NOT LengthOfService = 0 
--5256

UPDATE [CleaningLogTemporary]
SET UpdatedValue = OriginalValue
WHERE UpdatedValue IS NULL

UPDATE a
SET a.DateOfEntryIntoScheme = CAST(u.[UpdatedValue] AS datetime)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.DateOfEntryIntoScheme = CAST(u.[OriginalValue] AS datetime)
AND NOT u.UpdatedValue IS NULL


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]
WHERE NOT UpdatedValue IS NULL




-- STILL DODGY DOE AND LOS WRITE QUERY for this