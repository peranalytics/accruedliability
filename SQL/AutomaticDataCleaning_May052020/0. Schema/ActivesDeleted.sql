USE [AccruedLiability]
GO


IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'ActivesDeleted'))
BEGIN
	CREATE TABLE [dbo].[ActivesDeleted](
		[VersionId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] [varchar](20) NOT NULL,
		[DateOfBirth] [datetime] NULL,
		[GenderId] [int] NOT NULL,
		[CivilStatusId] [int] NOT NULL,
		[PensionSchemeId] [int] NOT NULL,
		[DateOfEntryIntoScheme] [datetime] NOT NULL,
		[MinimumNormalRetirementAge] [int] NOT NULL,
		[PrsiClass] [nvarchar](100) NOT NULL,
		[AnnualPensionablePayFTE] [decimal](10, 2) NOT NULL,
		[LengthOfService] [decimal](10, 2) NOT NULL,
		[FTE] [decimal](10, 2) NOT NULL,
		[Grade] [nvarchar](250) NOT NULL,
		[ScalePoint] [nvarchar](250) NOT NULL,
		[IncrementDate] [datetime] NULL,
		[CreateDate] [datetime] NOT NULL,
		[ModifyDate] [datetime] NOT NULL
	) ON [PRIMARY]
END



