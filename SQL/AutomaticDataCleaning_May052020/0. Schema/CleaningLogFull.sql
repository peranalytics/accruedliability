USE [AccruedLiability]
GO

--DROP TABLE [CleaningLogFull]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'CleaningLogFull'))
BEGIN
   
	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[CleaningLogFull](
		[Scheme] NVARCHAR(100) NULL,
		[VersionId] [int] NOT NULL,
		[SectorId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] [varchar](20) NOT NULL,
		[DateOfBirth] DATETIME,
		[Property] NVARCHAR(100) NULL,
		[OriginalValue] NVARCHAR(100) NOT NULL,
		[UpdatedValue] NVARCHAR(100) NOT NULL
	)

 --Do Stuff
END
GO

SELECT * FROM [CleaningLogFull]