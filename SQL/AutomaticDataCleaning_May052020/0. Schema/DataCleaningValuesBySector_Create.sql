USE [AccruedLiability]
GO

--DROP TABLE [DataCleaningValuesBySector]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'DataCleaningValuesBySector'))
BEGIN
   
	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[DataCleaningValuesBySector](
		[SectorId] [int] NOT NULL,
		[Scheme] NVARCHAR(100) NULL,
		[Property] NVARCHAR(100) NULL,
		[Value] NVARCHAR(100) NOT NULL
	)

 --Do Stuff
END
GO

SELECT * FROM [DataCleaningValuesBySector]