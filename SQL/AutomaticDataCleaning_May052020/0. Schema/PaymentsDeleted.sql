USE [AccruedLiability]
GO

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'PaymentsDeleted'))
BEGIN

/****** Object:  Table [dbo].[Payments]    Script Date: 08/04/2020 08:54:45 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[PaymentsDeleted](
		[VersionId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] [varchar](20) NOT NULL,
		[DateOfBirth] [datetime] NULL,
		[GenderId] [int] NOT NULL,
		[CivilStatusId] [int] NULL,
		[PensionSchemeId] [int] NOT NULL,
		[TypeOfBeneficiaryId] [int] NOT NULL,
		[PensionCommencementDate] [datetime] NOT NULL,
		[AnnualPensionValue] [decimal](10, 2) NOT NULL,
		[AnnualSupplementaryPension] [decimal](10, 2) NOT NULL,
		[BasisPensionCommencedId] [int] NOT NULL,
		[PensionableServiceYears] [decimal](10, 2) NULL,
		[ActualEmploymentBasedServiceYears] [decimal](10, 2) NULL,
		[PensionableServiceDifferenceReasons] [nvarchar](250) NULL,
		[BasicPayValuePensionableRemuneration] [decimal](10, 2) NULL,
		[PensionableAllowancesPensionableRemuneration] [decimal](10, 2) NULL,
		[HowMuchAbatementIfAny] [nvarchar](250) NULL,
		[CreateDate] [datetime] NOT NULL,
		[ModifyDate] [datetime] NOT NULL
	) ON [PRIMARY]

END

SELECT * FROM PaymentsDeleted
