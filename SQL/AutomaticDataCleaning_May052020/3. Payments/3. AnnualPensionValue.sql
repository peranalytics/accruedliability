USE AccruedLiability

--NRAs
DELETE FROM [CleaningLogTemporary]

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Payments', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate, 'AnnualPensionValue', AnnualPensionValue, NULL
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE ([AnnualPensionValue] IS NULL OR [AnnualPensionValue] <=0)
--261 Empty Pay

UPDATE a
SET [UpdatedValue] = CAST(VAlue AS DECIMAL(10,2))
FROM [CleaningLogTemporary] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE dcs.Scheme = 'Payments' AND dcs.Property = 'AnnualPensionValue'
AND [UpdatedValue] IS NULL

--UPDATING LIVE FILE
UPDATE a
SET a.AnnualPensionValue = CAST(u.[UpdatedValue] AS DECIMAL(10,2))
FROM Payments a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.AnnualPensionValue = CAST(u.[OriginalValue] AS DECIMAL(10,2))


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]

SELECT * FROM [CleaningLogFull] WHERE Scheme = 'Payments'
