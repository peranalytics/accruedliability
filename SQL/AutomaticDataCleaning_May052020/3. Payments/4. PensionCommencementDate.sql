USE AccruedLiability

DELETE FROM [CleaningLogTemporary]

 INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Payments', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate, 'PensionCommencementDate', PensionCommencementDate, NULL
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE (PensionCommencementDate IN ('01 jan 1900') OR PensionCommencementDate IS NULL)
--1922


UPDATE a
SET [UpdatedValue] = CAST(VAlue AS DATETIME)
FROM [CleaningLogTemporary] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE dcs.Scheme = 'Payments' AND dcs.Property = 'AvgPensionCommencementDate'
AND [UpdatedValue] IS NULL


UPDATE a
SET a.PensionCommencementDate = CAST(u.[UpdatedValue] AS datetime)
FROM Payments a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionCommencementDate = u.DateOfEntryIntoScheme
AND a.PensionCommencementDate = CAST(u.[OriginalValue] AS datetime)


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]
