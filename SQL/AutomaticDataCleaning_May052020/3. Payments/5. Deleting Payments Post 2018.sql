USE AccruedLiability


INSERT INTO [dbo].[PaymentsDeleted]
           ([VersionId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[GenderId]
           ,[CivilStatusId]
           ,[PensionSchemeId]
           ,[TypeOfBeneficiaryId]
           ,[PensionCommencementDate]
           ,[AnnualPensionValue]
           ,[AnnualSupplementaryPension]
           ,[BasisPensionCommencedId]
           ,[PensionableServiceYears]
           ,[ActualEmploymentBasedServiceYears]
           ,[PensionableServiceDifferenceReasons]
           ,[BasicPayValuePensionableRemuneration]
           ,[PensionableAllowancesPensionableRemuneration]
           ,[HowMuchAbatementIfAny]
           ,[CreateDate]
           ,[ModifyDate])

SELECT [VersionId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
      ,[DateOfBirth]
      ,[GenderId]
      ,[CivilStatusId]
      ,[PensionSchemeId]
      ,[TypeOfBeneficiaryId]
      ,[PensionCommencementDate]
      ,[AnnualPensionValue]
      ,[AnnualSupplementaryPension]
      ,[BasisPensionCommencedId]
      ,[PensionableServiceYears]
      ,[ActualEmploymentBasedServiceYears]
      ,[PensionableServiceDifferenceReasons]
      ,[BasicPayValuePensionableRemuneration]
      ,[PensionableAllowancesPensionableRemuneration]
      ,[HowMuchAbatementIfAny]
      ,[CreateDate]
      ,[ModifyDate]
FROM [dbo].[Payments] a
WHERE [PensionCommencementDate] > '31 dec 2018'
AND PPSN NOT in (SELECT sub.PPSN FROM PaymentsDeleted sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId])

DELETE FROM [dbo].[Payments] 
WHERE [PensionCommencementDate] > '31 dec 2018'

--444