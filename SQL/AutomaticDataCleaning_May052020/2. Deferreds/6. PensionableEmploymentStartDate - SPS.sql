USE AccruedLiability

DELETE FROM [CleaningLogTemporary]

 INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Deferreds', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, 'PensionableEmploymentStartDate', PensionableEmploymentStartDate, NULL
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionableEmploymentStartDate < ('01 jan 2013')
AND PensionSchemeId = 4
--222

UPDATE a
SET [UpdatedValue] = CAST('01 jan 2013' AS DATETIME)
FROM [CleaningLogTemporary] a
WHERE [UpdatedValue] IS NULL

UPDATE a
SET a.PensionableEmploymentStartDate = CAST(u.[UpdatedValue] AS datetime)
FROM Deferreds a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionableEmploymentStartDate = u.DateOfEntryIntoScheme
AND a.PensionableEmploymentStartDate = CAST(u.[OriginalValue] AS datetime)


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]
