
USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[GenderGetAll] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 25/07/2019
-- Description:	Get the mapping lookup data
-- =============================================
CREATE PROCEDURE [dbo].[GenderGetAll] 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, MappingGender FROM [dbo].MappingGender
END
GO

GRANT EXECUTE ON [dbo].[GenderGetAll] TO budgetviewer AS [dbo]
GO


