USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [Security].[Roles]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[Roles](
		[RoleId] [int] IDENTITY(1,1) NOT NULL,
		RoleName NVARCHAR(250) NULL,
		DetailAccess BIT NOT NULL,
		ModifyDate DATETIME NOT NULL,
CONSTRAINT [PK_Roles_RoleId] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF__Roles_ModifyDate]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Roles] ADD  DEFAULT ((GETDATE())) FOR ModifyDate
END

INSERT INTO [Security].Roles(RoleName, DetailAccess, ModifyDate)
SELECT 'Administrator', 1, GETDATE() UNION ALL
SELECT 'Pension Administrator', 1 , GETDATE() UNION ALL
SELECT 'Pension Viewer' , 1, GETDATE() UNION ALL
SELECT 'Policy Viewer' , 0, GETDATE()

SELECT * FROM [Security].Roles

