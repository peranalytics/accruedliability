USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM BeneficiaryType


IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension ch')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'pension ch'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pen crc cner')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pen crc cner'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pen crc iser')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pen crc iser'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pen crc pi')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pen crc pi'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pens crc pvd')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pens crc pvd'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pens preserv')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pens preserv'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension cner')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pension cner'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension crc')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pension crc'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension exg')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pension exg'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension iser')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pension iser'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension main')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pension main'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension ns')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pension ns'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension pi')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pension pi'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension ver')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pension ver'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pension sp')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 'pension sp'
END
