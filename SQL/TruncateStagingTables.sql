USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[TruncateStagingTables]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Deletes the staging tables
-- =============================================
CREATE PROCEDURE [dbo].[TruncateStagingTables] 
	
AS

BEGIN
	DELETE FROM StagingBudgetData
END
GO

GRANT EXECUTE ON [dbo].[TruncateStagingTables] TO [budgetviewer] AS [dbo]
GO


