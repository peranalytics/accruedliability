USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [Report].[RelevantAuthorityGetAll]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Get the Relevant Authories
-- =============================================
CREATE PROCEDURE [Report].[RelevantAuthorityGetAll] 
	
AS

BEGIN
	
	SELECT 
		[RelevantAuthorityId]
		,[RelevantAuthorityName]
		,ra.[ModifyDate]
		,ra.[CreateDate]
		,ra.[ModifiedBy]
		,ROW_NUMBER() OVER(ORDER BY RelevantAuthorityName) AS rowNumber
	FROM [dbo].[RelevantAuthority] ra WITH(NOLOCK)
	
END
GO

GRANT EXECUTE ON [Report].[RelevantAuthorityGetAll] TO [AccruedLiabilityviewer] AS [dbo]
GO


