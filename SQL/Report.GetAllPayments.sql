USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS Report.GetAllPayments
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load deferreds to the table
-- =============================================
CREATE PROCEDURE Report.GetAllPayments
	@UserId INT,
	@Year INT,
	@Sectors NVARCHAR(max) = '-1'
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT CAST(Value AS INT) AS SectorId
	INTO #SectorList FROM STRING_SPLIT(@Sectors, ',')  
	WHERE RTRIM(value) <> '';


	DECLARE @DetailAccess INT
	SELECT @DetailAccess = DetailAccess FROM Security.Roles r INNER JOIN Security.UserRoles ur ON ur.RoleId = r.RoleId WHERE ur.UserId = @UserId
	
	SELECT [RelevantAuthorityId]
		  ,[RelevantAuthorityName]
		  ,[YearId]
		  ,(CASE WHEN @DetailAccess = 1 THEN PPSN ELSE '--------' END) AS PPSN
		  ,[DateOfBirth]
		  ,[Gender]
		  ,[CivilStatus]
		  ,[PensionScheme]
		  ,[BeneficiaryType]
		  ,[PensionCommencementDate]
		  ,[AnnualPensionValue]
		  ,[AnnualSupplementaryPension]
		  ,[PensionBasis]
		  ,[PensionableServiceYears]
--		  ,[ActualEmploymentBasedServiceYears]
--		  ,[PensionableServiceDifferenceReasons]
		  ,[BasicPayValuePensionableRemuneration]
		  ,[PensionableAllowancesPensionableRemuneration]
--		  ,[HowMuchAbatementIfAny]
	  FROM [dbo].[PaymentsFlatFile]
	 WHERE (@Sectors = '-1') OR  SectorId IN (SELECT sub.SectorId FROm #SectorList sub)
			 AND (@Year = YearId)



	DROP TABLE #SectorList


END
GO

GRANT EXECUTE ON Report.GetAllPayments TO AccruedLiabilityViewer 
