
USE AccruedLiability
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FileLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[FileLog](
	[VersionId] INT NOT NULL,
	[UserId] INT NOT NULL,
	CreateDate DATETIME DEFAULT GETDATE()
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys o WHERE o.name = 'FK_FileLog_User')
BEGIN
    ALTER TABLE [Security].[FileLog] WITH CHECK ADD CONSTRAINT [FK_FileLog_User] FOREIGN KEY(UserId) REFERENCES  [Security].[Users] ([UserId])
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys o WHERE o.name = 'FK_FileLog_Metadata')
BEGIN
    ALTER TABLE [Security].[FileLog] WITH CHECK ADD CONSTRAINT [FK_FileLog_FileHistory] FOREIGN KEY(VersionId) REFERENCES  FileHistory (VersionId)
END

