USE AccruedLiability


IF NOT EXISTS (SELECT 1 FROM PensionScheme WHERE PensionScheme = 'Ministerial_Judicial')
BEGIN
INSERT INTO PensionScheme (Id, PensionScheme)
SELECT 10, 'Ministerial_Judicial'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'Ministerial_Judicial')
BEGIN
INSERT INTO MappingPensionScheme (Id, MappingPensionScheme)
SELECT 10, 'Ministerial_Judicial'
END


UPDATE PensionScheme
SET PensionScheme = 'Post 1995'
WHERE PensionScheme = 'Pre 2004'

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'Post 1995')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'Post 1995' END


UPDATE PensionBasis
SET PensionBasis = 'Early Retirement'
WHERE PensionBasis = 'Cost Neutral Early Retirement'

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'Early Retirement')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'Early Retirement' END


UPDATE BeneficiaryType
SET BeneficiaryType = 'PAO Beneficiary'
WHERE BeneficiaryType = 'PAO'

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'PAO Beneficiary')BEGIN INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)SELECT 3, 'PAO Beneficiary' END




IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class d (pre ''89) unco-ordinated')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'class d (pre ''89) unco-ordinated' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'co-habitant ')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 11, 'co-habitant ' END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'j/s retirement initiative')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'j/s retirement initiative' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'abolition of office')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'abolition of office' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pen crc pi')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'pen crc pi' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pens crc pvd')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'pens crc pvd' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pension ch')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 6, 'pension ch' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pension crc')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'pension crc' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pension exg')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'pension exg' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pension main')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'pension main' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pension ns')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'pension ns' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pension pi')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'pension pi' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pension sp')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 6, 'pension sp' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pension ver')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'pension ver' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '1900-01-01')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '1900-01-01' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ex gratia')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'ex gratia' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2014-06-01 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2014-06-01 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2016-08-01 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2016-08-01 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2015-11-15 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2015-11-15 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2017-09-25 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2017-09-25 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2013-08-31 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2013-08-31 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2015-10-01 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2015-10-01 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2016-07-19 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2016-07-19 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2014-02-01 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2014-02-01 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2014-12-13 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2014-12-13 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2015-12-01 00:00:00')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '2015-12-01 00:00:00' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'gratuity')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'gratuity' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver1987')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'ver1987' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver 1997?')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'ver 1997?' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver 2009')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'ver 2009' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver 2008')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'ver 2008' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver 2004')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'ver 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ver 1992')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'ver 1992' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'presvd')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'presvd' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'actred')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'actred' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'webess')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'webess' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'preserved ben.')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'preserved ben.' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'fast accrual scheme 3% annual increase')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'fast accrual scheme 3% annual increase' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'dismissal')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'dismissal' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'nurses pers')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'nurses pers' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'defcib')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'defcib' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'illpot')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 2, 'illpot' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'illhealth')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 2, 'illhealth' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'spouse beneficiary')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 6, 'spouse beneficiary' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'child beneficiary')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 6, 'child beneficiary' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'retirement cost neutral')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'retirement cost neutral' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'incentivised scheme early retire')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 4, 'incentivised scheme early retire' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'death')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 6, 'death' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '-')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, '-' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'actuarially reduced')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'actuarially reduced' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'refund')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'refund' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'age grounds')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'age grounds' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retirment option')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'early retirment option' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retirment scheme')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'early retirment scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'age grrounds')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'age grrounds' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pilot early retirement scheme')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'pilot early retirement scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'k148/181')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'k148/181' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'job sharing iniative')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'job sharing iniative' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'neutral early retirement')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'neutral early retirement' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'normal pensionable age')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'normal pensionable age' END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'co-habitant ')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 11, 'co-habitant ' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'not disclosed')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 10, 'not disclosed' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 's - single')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 0, 's - single' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'p - separated')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 6, 'p - separated' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'm - married')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 1, 'm - married' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'o - other')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 10, 'o - other' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'e')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 10, 'e' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'c')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 10, 'c' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'marries')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 1, 'marries' END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'general operatives (co-or 6.5%)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'general operatives (co-or 6.5%)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nhass all')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'nhass all' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nhass medical')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'nhass medical' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'heritage council staff superannuation scheme ')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'heritage council staff superannuation scheme ' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'f�s closed scheme-former anco')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'f�s closed scheme-former anco' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hse 1956 scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'hse 1956 scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'f�s - former national manpower')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'f�s - former national manpower' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class d (pre ''89) unco-ordinated')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'class d (pre' '89) unco-ordinated' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class d (pre 95) un-coordinated')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'class d (pre 95) un-coordinated' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'co-ordinated (new entrants)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'co-ordinated (new entrants)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'co-ordinated (ongoing)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'co-ordinated (ongoing)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'co-ordinated 6.5%(full)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'co-ordinated 6.5%(full)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hia superann scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'hia superann scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'royal irish academy model scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'royal irish academy model scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'royal irish academy new scheme mems npa65')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'royal irish academy new scheme mems npa65' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 2002')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'pre 2002' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'vhss')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'vhss' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'rhd scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'rhd scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'd1 officers: main scheme (spouses & children)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'd1 officers: main scheme (spouses & children)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'co-ordinated officers')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'co-ordinated officers' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new entrants from april 04')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'new entrants from april 04' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class a (co-ordinated employees)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'class a (co-ordinated employees)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new entrant pension scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'new entrant pension scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class a co-ordinated (officers)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'class a co-ordinated (officers)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class d (un-coordinated officers)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'class d (un-coordinated officers)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1956 act non officer')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, '1956 act non officer' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'vhss - officers')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'vhss - officers' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'vhss - non officers')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'vhss - non officers' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'vhss - officer')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'vhss - officer' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pension 5% (old scheme)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'pension 5% (old scheme)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pension 6.5%(old scheme)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pension 6.5%(old scheme)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'vhss -  non officers ')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'vhss -  non officers ' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single public sector pension 2013')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 4, 'single public sector pension 2013' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pro rata  integration a class officers')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pro rata  integration a class officers' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pro rata  integration a class employees')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pro rata  integration a class employees' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single pension scheme 2013 (prison civilian)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 4, 'single pension scheme 2013 (prison civilian)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'unestablished civil service pension scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 5, 'unestablished civil service pension scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'civil service pension scheme post 6 apr 95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'civil service pension scheme post 6 apr 95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'civil service pension scheme  pre 6-apr-95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'civil service pension scheme  pre 6-apr-95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single pension scheme 2013 (prison officers)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 4, 'single pension scheme 2013 (prison officers)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'integration a class ee''s (nes pro rata)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'integration a class ee''s (nes pro rata)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'integration a class off (ne pro rata)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'integration a class off (ne pro rata)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pension scheme a class & spouses & children contributory.')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pension scheme a class & spouses & children contributory.' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'was in nhass')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'was in nhass' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'was in nhass main')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'was in nhass main' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'bord bia supperannuation scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'bord bia supperannuation scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hia superannuation scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'hia superannuation scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'royal irish academy old scheme mems npa60')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'royal irish academy old scheme mems npa60' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1998 scheme pt ii ch.3 (1977) (new entrant)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, '1998 scheme pt ii ch.3 (1977) (new entrant)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1998 scheme pt ii ch.3 (1977) (class a officer paying s&c)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, '1998 scheme pt ii ch.3 (1977) (class a officer paying s&c)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1998 scheme pt ii ch.3 (1977) (no w&o)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, '1998 scheme pt ii ch.3 (1977) (no w&o)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1986 revision scheme (non new entrant)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, '1986 revision scheme (non new entrant)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1986 revision scheme (no w&o)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, '1986 revision scheme (no w&o)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1998 scheme pt ii ch.3 (1977) (paying w&o)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, '1998 scheme pt ii ch.3 (1977) (paying w&o)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'ex-gratia ')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'ex-gratia ' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'ex gratia')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'ex gratia' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'non officer')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'non officer' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1956')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, '1956' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'non officer pre 95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'non officer pre 95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'officer pre 95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'officer pre 95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'officer pre 2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'officer pre 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'ntda')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'ntda' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'rta')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'rta' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'cert')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'cert' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'childs pension -rta')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'childs pension -rta' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'ntda ')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'ntda ' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'spouse')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'spouse' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'spouse ntda')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'spouse ntda' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'model scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'model scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 2013')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 4, 'post 2013' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'spouse scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'spouse scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'childrens')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'childrens' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'group 179 established spouses')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'group 179 established spouses' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'group 177 established pensions')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'group 177 established pensions' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'unest.contrib.spouses pension')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 5, 'unest.contrib.spouses pension' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'spouse (pen)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'spouse (pen)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'group 178 unestablished pension')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 5, 'group 178 unestablished pension' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'garda pensioners')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'garda pensioners' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'gda widows-pen')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'gda widows-pen' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'gda comm-pens')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'gda comm-pens' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'f�s closed scheme-former anco ')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'f�s closed scheme-former anco ' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '-')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, '-' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'ex-gratia')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'ex-gratia' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'ex gratia ')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'ex gratia ' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hia superannuation scheme - new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'hia superannuation scheme - new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'ministerial/judicial')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 10, 'ministerial/judicial' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pips pension payment')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pips pension payment' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'the heritage council superannuation scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'the heritage council superannuation scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'soldier')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'soldier' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'officer')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'officer' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nurse')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'nurse' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pao')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pao' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'chaplain')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'chaplain' END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'non-member')BEGIN INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)SELECT 2, 'non-member' END
IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'childerns')BEGIN INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)SELECT 4, 'childerns' END
IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'ex gratia')BEGIN INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)SELECT 5, 'ex gratia' END
