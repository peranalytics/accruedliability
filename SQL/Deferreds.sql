USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [Deferreds]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Deferreds]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[Deferreds](
	[VersionId] [int] NOT NULL,
	[RelevantAuthorityId] [int] NOT NULL,
	[YearId] [int] NOT NULL,
	[PPSN] VARCHAR(20) NOT NULL,
	[DateOfBirth] DATETIME  NULL,
	GenderId INT NOT NULL,
	CivilStatusId INT  NULL,
	PensionSchemeId INT NOT NULL,
	PreservedPensionAge INT NOT NULL,
	PrsiClass NVARCHAR(100) NOT NULL,
	FTE DECIMAL(10,2) NOT NULL,
	PensionableEmploymentStartDate DATETIME NOT NULL,
	PensionableEmploymentLeaveDate DATETIME NOT NULL,
	PensionableRemunerationFuturePensionBasedOn DECIMAL(10,2) NOT NULL,
	PensionableServiceFuturePensionBasedOn DECIMAL(10,2) NULL,
	CreateDate DATETIME DEFAULT GETDATE() NOT NULL,
	ModifyDate DATETIME DEFAULT GETDATE() NOT NULL
	)
END
GO

