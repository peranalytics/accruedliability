USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MappingBeneficiaryType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MappingBeneficiaryType](
	[Id] [int] NOT NULL,
	[MappingBeneficiaryType] [nvarchar](100) NOT NULL
) 
END
GO

DELETE FROM [dbo].[MappingBeneficiaryType]
INSERT INTO [dbo].[MappingBeneficiaryType]
SELECT * FROM BeneficiaryType


SELECT * FROM MappingBeneficiaryType


