use AccruedLiability

ALTER TABLE Actives
ADD MandatoryRetirementAge INT NULL;

ALTER TABLE Actives
ADD AnnualBasicPensionablePayFTE DECIMAL(10,2) NULL;

ALTER TABLE StagingActives
ADD MandatoryRetirementAge INT NULL;

ALTER TABLE StagingActives
ADD AnnualBasicPensionablePayFTE DECIMAL(10,2) NULL;

ALTER TABLE ActivesFlatFile
ADD MandatoryRetirementAge INT NULL;

ALTER TABLE ActivesFlatFile
ADD AnnualBasicPensionablePayFTE DECIMAL(10,2) NULL;