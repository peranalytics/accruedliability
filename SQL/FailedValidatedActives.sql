USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--DROP TABLE [StagingActives]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FailedValidatedActives]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[FailedValidatedActives](
	[VersionId] [int] NOT NULL,
	[RelevantAuthorityId] [int] NOT NULL,
	[YearId] [int] NOT NULL,
	[PPSN] VARCHAR(20) NOT NULL,
	[DateOfBirth] DATETIME NOT NULL,
	Gender NVARCHAR(100) NOT NULL,
	CivilStatus NVARCHAR(100) NOT NULL,
	PensionScheme NVARCHAR(250) NOT NULL,
	[DateOfEntryIntoScheme] DATETIME NOT NULL,
	MinimumNormalRetirementAge INT NOT NULL,
	PrsiClass NVARCHAR(100) NOT NULL,
	AnnualPensionablePayFTE FLOAT NOT NULL,
	LengthOfService FLOAT NOT NULL,
	FTE FLOAT NOT NULL,
	Grade NVARCHAR(250) NOT NULL,
	ScalePoint NVARCHAR(250) NOT NULL,
	IncrementDate DATETIME NOT NULL
	)
END
GO

