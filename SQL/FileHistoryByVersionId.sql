USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS FileHistoryByVersionId
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	get meta data by version
-- =============================================
CREATE PROCEDURE FileHistoryByVersionId
	@VersionId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
	   [VersionId]
      ,[Year]
      ,ra.RelevantAuthorityId AS RelevantAuthorityId
	  ,RelevantAuthorityName
      ,[FileName]
      ,[UploadedBy]
      ,[FileTypeId]
      ,[RecordCount]
      ,md.[ModifyDate]
      ,md.[CreateDate]
      ,[Scheme]
      ,[Success]
      ,[EntryUpdateDate]
      ,[LocalFileName]
	FROM FileHistory md WITH(NOLOCK)
	INNER JOIN RelevantAuthority ra
	ON ra.RelevantAuthorityId = md.RelevantAUthorityId
	WHERE VersionId = @VersionId
	ORDER BY Scheme, Year

END
GO

GRANT EXECUTE ON FileHistoryByVersionId TO [AccruedLiabilityviewer] 
