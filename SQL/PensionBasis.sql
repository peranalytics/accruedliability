USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PensionBasis]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PensionBasis](
	[Id] [int] NOT NULL,
	[PensionBasis] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_PensionBasis_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

DELETE FROM [dbo].PensionBasis
INSERT INTO [dbo].PensionBasis
SELECT 1, 'Normal' UNION ALL
SELECT 2, 'Ill-health Retirement' UNION ALL
SELECT 3, 'Cost Neutral Early Retirement' UNION ALL
SELECT 4, 'Incentivised Retirement scheme'


SELECT * FROM PensionBasis