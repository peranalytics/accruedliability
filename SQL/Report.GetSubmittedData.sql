USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS Report.GetSubmittedData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load Bodies who submitted data for each scheme
-- =============================================
CREATE PROCEDURE Report.GetSubmittedData
@Year INT
	
AS
BEGIN
	
	SET NOCOUNT ON;
	--Payments
	--Actives
	--Deferreds

	SELECT
		s.SectorName,
		ra.RelevantAuthorityId,
		ra.RelevantAuthorityName, 'Payments' as Scheme
	
			
	FROM RelevantAuthority ra
	INNER JOIN RelevantAuthoritySectors ras ON ra.RelevantAuthorityId = ras.RelevantAuthorityId
	INNER JOIN Sectors s ON ras.SectorId = s.SectorId
	 WHERE ra.RelevantAuthorityId IN 
	(SELECT fh.RelevantAuthorityId from FileHistory fh WHERE Scheme = 'Payments' AND fh.Year = @Year) 
	
	UNION ALL
		SELECT s.SectorName,ra.RelevantAuthorityId, ra.RelevantAuthorityName, 'Actives' FROM RelevantAuthority ra 
		INNER JOIN RelevantAuthoritySectors ras ON ra.RelevantAuthorityId = ras.RelevantAuthorityId
		INNER JOIN Sectors s ON ras.SectorId = s.SectorId	
		WHERE ra.RelevantAuthorityId IN 
	(SELECT fh.RelevantAuthorityId from FileHistory fh WHERE Scheme = 'Actives' AND fh.Year = @Year)
	UNION ALL
		SELECT s.SectorName,ra.RelevantAuthorityId, ra.RelevantAuthorityName, 'Deferreds' FROM RelevantAuthority ra 
		INNER JOIN RelevantAuthoritySectors ras ON ra.RelevantAuthorityId = ras.RelevantAuthorityId
		INNER JOIN Sectors s ON ras.SectorId = s.SectorId
	    WHERE ra.RelevantAuthorityId IN 
	(SELECT fh.RelevantAuthorityId from FileHistory fh WHERE Scheme = 'Deferreds' AND fh.Year = @Year)
	
	
END
GO

GRANT EXECUTE ON Report.GetSubmittedData TO AccruedLiabilityViewer 
