USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS [dbo].[GetConfig]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Get all config values
-- =============================================
CREATE PROCEDURE [dbo].[GetConfig]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		ConfigName,
		ConfigValue
	FROM dbo.Config
END
GO

GRANT EXECUTE ON [dbo].[GetConfig] TO accruedliabilityviewer 
