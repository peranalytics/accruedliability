
USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileHistorySummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileHistorySummary](
	[YrId] [int] NULL,
	[RelevantAuthorityId] [int] NULL,
	[RelevantAuthorityName] [nvarchar](255) NULL,
	[Scheme] [nvarchar](50) NULL,
	[FileName] [nvarchar](255) NULL,
	[RecordCount] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Success] [bit] NULL,
	[UploadedBy] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO


