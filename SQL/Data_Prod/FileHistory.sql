USE [AccruedLiability]
GO

--select * from FileHistory

--delete from Security.FileLog
--delete from FileHistory

SET IDENTITY_INSERT dbo.FileHistory ON;

USE [AccruedLiability]
GO

INSERT INTO [dbo].[FileHistory]
           (
		   [VersionId]
		   ,[DataStrategyId]
           ,[RelevantAUthorityId]
           ,[FileName]
           ,[Year]
           ,[UploadedBy]
           ,[TransactionTypeId]
           ,[FileTypeId]
           ,[RecordCount]
           ,[Validated]
           ,[ModifyDate]
           ,[CreateDate]
           ,[Scheme]
           ,[Success]
           ,[EntryUpdateDate]
           ,[LocalFileName])
SELECT 57,124,250,'01082019_n_t_p_f_pensions_in_payment.xls.csv',2019,'dataanalytics@per.gov.ie',1,2,4,1,GETDATE(),GETDATE(),'Payments',0,GETDATE(),'NULL' UNION ALL
SELECT 58,140,170,'06082019_office_of_public_works_pensions_in_payment_14.csv',2019,'dataanalytics@per.gov.ie',1,2,23,1,GETDATE(),GETDATE(),'Payments',0,GETDATE(),'NULL' UNION ALL
SELECT 60,119,47,'bai_actives_datarequest_4.csv',2019,'dataanalytics@per.gov.ie',1,2,45,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_60_RA_47_Year_2019_baiactivesdatarequest4.csv' UNION ALL
SELECT 61,124,250,'01082019_n_t_p_f_pensions_in_payment.xls.csv',2019,'dataanalytics@per.gov.ie',1,2,4,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_61_RA_250_Year_2019_01082019ntpfpensionsinpaymentxls.csv' UNION ALL
SELECT 62,140,170,'06082019_office_of_public_works_pensions_in_payment_14.csv',2019,'dataanalytics@per.gov.ie',1,2,23,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_62_RA_170_Year_2019_06082019officeofpublicworkspensionsinpayment14.csv' UNION ALL
SELECT 63,154,314,'07082019_st_angelas__college__pensions_in_payment.xls_2.csv',2019,'dataanalytics@per.gov.ie',1,2,57,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_63_RA_314_Year_2019_07082019stangelascollegepensionsinpaymentxls2.csv' UNION ALL
SELECT 65,161,314,'07082019_st_angelas_college_activeemployees_1.csv',2019,'dataanalytics@per.gov.ie',1,2,194,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_65_RA_314_Year_2019_07082019stangelascollegeactiveemployees1.csv' UNION ALL
SELECT 66,143,153,'08082019_gaisce_activeemployees.csv',2019,'dataanalytics@per.gov.ie',1,2,12,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_66_RA_153_Year_2019_08082019gaisceactiveemployees.csv' UNION ALL
SELECT 67,120,47,'bai_pensions_in_payment_datarequest.csv',2019,'dataanalytics@per.gov.ie',1,2,2,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_67_RA_47_Year_2019_baipensionsinpaymentdatarequest.csv' UNION ALL
SELECT 68,149,153,'pensions_in_payment_datarequest_3.csv',2019,'dataanalytics@per.gov.ie',1,2,0,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_68_RA_153_Year_2019_pensionsinpaymentdatarequest3.csv' UNION ALL
SELECT 69,225,153,'15.08.2019Gaisce_Deferreds.csv',2019,'dataanalytics@per.gov.ie',1,2,0,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_69_RA_153_Year_2019_15082019gaiscedeferreds.csv' UNION ALL
SELECT 70,241,345,'14082019_Teaching_Council_345_.csv',2019,'dataanalytics@per.gov.ie',1,2,2,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_70_RA_345_Year_2019_14082019teachingcouncil345.csv' UNION ALL
SELECT 71,244,345,'14082019_teaching_council_345_pension_in_payment~1.csv',2019,'dataanalytics@per.gov.ie',1,2,3,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_71_RA_345_Year_2019_14082019teachingcouncil345pensioninpayment1.csv' UNION ALL
SELECT 72,245,47,'BAI_Deferreds_DataRequest.csv',2019,'dataanalytics@per.gov.ie',1,2,15,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_72_RA_47_Year_2019_baideferredsdatarequest.csv' UNION ALL
SELECT 73,247,250,'16082019_N_T_P_F_deferred_members.xls.csv',2019,'dataanalytics@per.gov.ie',1,2,30,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_73_RA_250_Year_2019_16082019ntpfdeferredmembersxls.csv' UNION ALL
SELECT 74,248,345,'16082019_Teaching_Council__Deferred__Members_.csv',2019,'dataanalytics@per.gov.ie',1,2,3,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_74_RA_345_Year_2019_16082019teachingcouncildeferredmembers.csv' UNION ALL
SELECT 75,250,345,'150819_teaching_council_pension_in_payment~1.csv',2019,'dataanalytics@per.gov.ie',1,2,3,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_75_RA_345_Year_2019_150819teachingcouncilpensioninpayment1.csv' UNION ALL
SELECT 76,254,345,'16082019_teachincouncil_active_employees_~2.csv',2019,'dataanalytics@per.gov.ie',1,2,36,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_76_RA_345_Year_2019_16082019teachincouncilactiveemployees2.csv' UNION ALL
SELECT 77,256,357,'01082019_psi_pensions_in_payment~1.csv',2019,'dataanalytics@per.gov.ie',1,2,3,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_77_RA_357_Year_2019_01082019psipensionsinpayment1.csv' UNION ALL
SELECT 78,268,250,'19082019_n_t_p_f_activeemployees~3.csv',2019,'dataanalytics@per.gov.ie',1,2,49,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_78_RA_250_Year_2019_19082019ntpfactiveemployees3.csv' UNION ALL
SELECT 79,269,353,'19082019_sport_ireland_activeemployees~5.csv',2019,'dataanalytics@per.gov.ie',1,2,40,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_79_RA_353_Year_2019_19082019sportirelandactiveemployees5.csv' UNION ALL
SELECT 80,289,20,'200819_national_concert_hall_activeemployees1~1.csv',2019,'dataanalytics@per.gov.ie',1,2,39,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_80_RA_20_Year_2019_200819nationalconcerthallactiveemployees11.csv' UNION ALL
SELECT 81,308,166,'20082019_hiqa_pensions_in_payments~2.csv',2019,'dataanalytics@per.gov.ie',1,2,18,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_81_RA_166_Year_2019_20082019hiqapensionsinpayments2.csv' UNION ALL
SELECT 82,332,20,'20082019_national_concert_hall_pensions_in_payment1~6.csv',2019,'dataanalytics@per.gov.ie',1,2,12,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_82_RA_20_Year_2019_20082019nationalconcerthallpensionsinpayment16.csv' UNION ALL
SELECT 83,333,47,'bai_pensions_in_payment_datarequest~1.csv',2019,'dataanalytics@per.gov.ie',1,2,2,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_83_RA_47_Year_2019_baipensionsinpaymentdatarequest1.csv' UNION ALL
SELECT 84,337,20,'200819_National_Concert_Hall_deferred_members.csv',2019,'dataanalytics@per.gov.ie',1,2,18,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_84_RA_20_Year_2019_200819nationalconcerthalldeferredmembers.csv' UNION ALL
SELECT 85,339,284,'19082019_pre_hospital_emergency_care_council_activeemployees~1.csv',2019,'dataanalytics@per.gov.ie',1,2,15,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_85_RA_284_Year_2019_19082019prehospitalemergencycarecouncilactiveemployees1.csv' UNION ALL
SELECT 86,381,290,'22082019_commission_for_railway_regulation_pensions_in_payment~2.csv',2019,'dataanalytics@per.gov.ie',1,2,1,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_86_RA_290_Year_2019_22082019commissionforrailwayregulationpensionsinpayment2.csv' UNION ALL
SELECT 87,402,290,'22082019_commission_for_railway_regulation_activeemployees~1.csv',2019,'dataanalytics@per.gov.ie',1,2,14,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_87_RA_290_Year_2019_22082019commissionforrailwayregulationactiveemployees1.csv' UNION ALL
SELECT 88,403,170,'23082019_office_of_public_works_deferred_members.xls~6.csv',2019,'dataanalytics@per.gov.ie',1,2,36,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_88_RA_170_Year_2019_23082019officeofpublicworksdeferredmembersxls6.csv' UNION ALL
SELECT 89,404,170,'23082019_office_of_public_works_deferred_members.xls~7.csv',2019,'dataanalytics@per.gov.ie',1,2,36,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_89_RA_170_Year_2019_23082019officeofpublicworksdeferredmembersxls7.csv' UNION ALL
SELECT 90,406,391,'27082019_national_university_of_ireland_activeemployees~1.csv',2019,'dataanalytics@per.gov.ie',1,2,18,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_90_RA_391_Year_2019_27082019nationaluniversityofirelandactiveemployees1.csv' UNION ALL
SELECT 91,414,250,'01082019_n_t_p_f_pensions_in_payment.xls~1.csv',2019,'dataanalytics@per.gov.ie',1,2,4,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_91_RA_250_Year_2019_01082019ntpfpensionsinpaymentxls1.csv' UNION ALL
SELECT 92,415,166,'27082019_HIQA_pensions_in_payments.csv',2019,'dataanalytics@per.gov.ie',1,2,18,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_92_RA_166_Year_2019_27082019hiqapensionsinpayments.csv' UNION ALL
SELECT 93,417,387,'27082019_Regulator_of_the_National_Lottery_ActiveEmployees.csv',2019,'dataanalytics@per.gov.ie',1,2,6,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_93_RA_387_Year_2019_27082019regulatorofthenationallotteryactiveemployees.csv' UNION ALL
SELECT 94,418,387,'27082019_Regulator_of_the_National_Lottery_Pensions_in_Payment.csv',2019,'dataanalytics@per.gov.ie',1,2,0,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_94_RA_387_Year_2019_27082019regulatorofthenationallotterypensionsinpayment.csv' UNION ALL
SELECT 95,419,162,'26082019_Grangegorman_Development_Agency_Pensions__in_Payment.csv',2019,'dataanalytics@per.gov.ie',1,2,1,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_95_RA_162_Year_2019_26082019grangegormandevelopmentagencypensionsinpayment.csv' UNION ALL
SELECT 96,421,162,'27082019_grangegorman_development_agency_activeemployees~1.csv',2019,'dataanalytics@per.gov.ie',1,2,19,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_96_RA_162_Year_2019_27082019grangegormandevelopmentagencyactiveemployees1.csv' UNION ALL
SELECT 97,422,162,'27082019_Grangegorman_Development_Agency_deferred__members.csv',2019,'dataanalytics@per.gov.ie',1,2,1,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_97_RA_162_Year_2019_27082019grangegormandevelopmentagencydeferredmembers.csv' UNION ALL
SELECT 98,423,122,'31072019_crawford_art_gallery__pensions_in_payment~1.csv',2019,'dataanalytics@per.gov.ie',1,2,0,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_98_RA_122_Year_2019_31072019crawfordartgallerypensionsinpayment1.csv' UNION ALL
SELECT 99,424,122,'27082019_Crawford_Art_Gallery_Deferreds_DataRequest.csv',2019,'dataanalytics@per.gov.ie',1,2,0,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_99_RA_122_Year_2019_27082019crawfordartgallerydeferredsdatarequest.csv' UNION ALL
SELECT 100,425,162,'26082019_grangegorman_development_agency_pensions__in_payment~1.csv',2019,'dataanalytics@per.gov.ie',1,2,1,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_100_RA_162_Year_2019_26082019grangegormandevelopmentagencypensionsinpayment1.csv' UNION ALL
SELECT 101,427,122,'27082019_Crawford_Art_Gallery__Actives_DataRequest.csv',2019,'dataanalytics@per.gov.ie',1,2,6,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_101_RA_122_Year_2019_27082019crawfordartgalleryactivesdatarequest.csv' UNION ALL
SELECT 102,426,391,'27082019_National_University_of_Ireland_Pensions_in_Payment.csv',2019,'dataanalytics@per.gov.ie',1,2,9,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_102_RA_391_Year_2019_27082019nationaluniversityofirelandpensionsinpayment.csv' UNION ALL
SELECT 103,429,391,'28082019_national_university_of_ireland_deferred_members~1.csv',2019,'dataanalytics@per.gov.ie',1,2,13,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_103_RA_391_Year_2019_28082019nationaluniversityofirelanddeferredmembers1.csv' UNION ALL
SELECT 104,430,391,'29082019_National_University_of_Ireland_Pensions_in_Payment.csv',2019,'dataanalytics@per.gov.ie',1,2,9,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_104_RA_391_Year_2019_29082019nationaluniversityofirelandpensionsinpayment.csv' UNION ALL
SELECT 105,431,391,'29082019_National_University_of_Ireland_deferred_members.csv',2019,'dataanalytics@per.gov.ie',1,2,13,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_105_RA_391_Year_2019_29082019nationaluniversityofirelanddeferredmembers.csv' UNION ALL
SELECT 106,433,17,'29082019_adoption_authority_of_ireland_activeemployees~1.csv',2019,'dataanalytics@per.gov.ie',1,2,28,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_106_RA_17_Year_2019_29082019adoptionauthorityofirelandactiveemployees1.csv' UNION ALL
SELECT 107,435,17,'29082019_adoption_authority_of_ireland_pensions_in_payment~1.csv',2019,'dataanalytics@per.gov.ie',1,2,1,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_107_RA_17_Year_2019_29082019adoptionauthorityofirelandpensionsinpayment1.csv' UNION ALL
SELECT 108,442,353,'30082019_Sport_Ireland_pensions_in_Payment.csv',2019,'dataanalytics@per.gov.ie',1,2,4,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_108_RA_353_Year_2019_30082019sportirelandpensionsinpayment.csv' UNION ALL
SELECT 109,443,353,'30082019_Sport_Ireland_deferred_members.csv',2019,'dataanalytics@per.gov.ie',1,2,27,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_109_RA_353_Year_2019_30082019sportirelanddeferredmembers.csv' UNION ALL
SELECT 110,444,353,'02092019_Sport_Ireland_pensions_in_Payment.csv',2019,'dataanalytics@per.gov.ie',1,2,4,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_110_RA_353_Year_2019_02092019sportirelandpensionsinpayment.csv' UNION ALL
SELECT 111,446,20,'20082019_national_concert_hall_pensions_in_payment2~1.csv',2019,'dataanalytics@per.gov.ie',1,2,12,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_111_RA_20_Year_2019_20082019nationalconcerthallpensionsinpayment21.csv' UNION ALL
SELECT 112,449,367,'06082019_university_of_limerick_activeemployees~2.csv',2019,'dataanalytics@per.gov.ie',1,2,1933,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_112_RA_367_Year_2019_06082019universityoflimerickactiveemployees2.csv' UNION ALL
SELECT 113,456,367,'21082019_university_of_limerick_deferred_members~6.csv',2019,'dataanalytics@per.gov.ie',1,2,416,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_113_RA_367_Year_2019_21082019universityoflimerickdeferredmembers6.csv' UNION ALL
SELECT 114,463,345,'030919_TEST_teaching_council__to_DPER_030919_Pensions_in_Payment_DataRequest.csv',2019,'dataanalytics@per.gov.ie',1,2,0,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_114_RA_345_Year_2019_030919testteachingcounciltodper030919pensionsinpaymentdatarequest.csv' UNION ALL
SELECT 115,467,345,'DPER_2019_Teaching_Coucil__Pensions_in_Payment_Data_Request.csv',2019,'dataanalytics@per.gov.ie',1,2,3,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_115_RA_345_Year_2019_dper2019teachingcoucilpensionsinpaymentdatarequest.csv' UNION ALL
SELECT 116,479,367,'06082019_university_of_limerick_pensions_in_payment~4.csv',2019,'dataanalytics@per.gov.ie',1,2,481,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_116_RA_367_Year_2019_06082019universityoflimerickpensionsinpayment4.csv' UNION ALL
SELECT 117,553,314,'04092019_st_angelas_defered_datarequest~4.csv',2019,'dataanalytics@per.gov.ie',1,2,88,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_117_RA_314_Year_2019_04092019stangelasdefereddatarequest4.csv' UNION ALL
SELECT 118,554,314,'04092019_st_angelas_defered_datarequest~5.csv',2019,'dataanalytics@per.gov.ie',1,2,88,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_118_RA_314_Year_2019_04092019stangelasdefereddatarequest5.csv' UNION ALL
SELECT 119,555,290,'05092019_Commission_for_Railway_Regulation_Pensions_in_Payment.csv',2019,'dataanalytics@per.gov.ie',1,2,1,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_119_RA_290_Year_2019_05092019commissionforrailwayregulationpensionsinpayment.csv' UNION ALL
SELECT 120,560,345,'05092019_teaching_councideferred_members_~1.csv',2019,'dataanalytics@per.gov.ie',1,2,5,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_120_RA_345_Year_2019_05092019teachingcouncideferredmembers1.csv' UNION ALL
SELECT 121,570,345,'05092019_teaching_council_active_members~5.csv',2019,'dataanalytics@per.gov.ie',1,2,46,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_121_RA_345_Year_2019_05092019teachingcouncilactivemembers5.csv' UNION ALL
SELECT 122,571,17,'05092019_Adoption_Authority_of_Ireland_Deferreds_DataRequest.csv',2019,'dataanalytics@per.gov.ie',1,2,4,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_122_RA_17_Year_2019_05092019adoptionauthorityofirelanddeferredsdatarequest.csv' UNION ALL
SELECT 123,573,170,'06092019_public_service_body_name_activeemployees~1.csv',2019,'dataanalytics@per.gov.ie',1,2,74,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_123_RA_170_Year_2019_06092019publicservicebodynameactiveemployees1.csv' UNION ALL
SELECT 124,578,138,'09092019_dun_laoghaire_rathdown_co_co_actives_datarequest~1.csv',2019,'dataanalytics@per.gov.ie',1,2,1011,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_124_RA_138_Year_2019_09092019dunlaoghairerathdowncocoactivesdatarequest1.csv' UNION ALL
SELECT 125,668,288,'16092019_Quality_and_Qualifications_Ireland_ActiveEmployees.csv',2019,'dataanalytics@per.gov.ie',1,2,81,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_125_RA_288_Year_2019_16092019qualityandqualificationsirelandactiveemployees.csv' UNION ALL
SELECT 126,671,174,'17092019_housing_finance_agency_activeemployees~2.csv',2019,'dataanalytics@per.gov.ie',1,2,14,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_126_RA_174_Year_2019_17092019housingfinanceagencyactiveemployees2.csv' UNION ALL
SELECT 127,697,17,'05092019_adoption_authority_of_ireland_deferreds_datarequest~1.csv',2019,'dataanalytics@per.gov.ie',1,2,4,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_127_RA_17_Year_2019_05092019adoptionauthorityofirelanddeferredsdatarequest1.csv' UNION ALL
SELECT 128,698,387,'27082019_Regulator_of_the_National_Lottery_deferred_members.csv',2019,'dataanalytics@per.gov.ie',1,2,1,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_128_RA_387_Year_2019_27082019regulatorofthenationallotterydeferredmembers.csv' UNION ALL
SELECT 129,706,166,'20092019_hiqa_activeemployees~2.csv',2019,'dataanalytics@per.gov.ie',1,2,235,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_129_RA_166_Year_2019_20092019hiqaactiveemployees2.csv' UNION ALL
SELECT 130,710,500,'24_08_19_tourism_ireland_pensions_in_payment~5.csv',2019,'dataanalytics@per.gov.ie',1,2,34,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_130_RA_500_Year_2019_240819tourismirelandpensionsinpayment5.csv' UNION ALL
SELECT 131,716,500,'24_08_19_tourism_ireland_pensions_in_payment~6.csv',2019,'dataanalytics@per.gov.ie',1,2,34,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_131_RA_500_Year_2019_240819tourismirelandpensionsinpayment6.csv' UNION ALL
SELECT 132,722,174,'19092019_Housing_Finance_Agency_deferred_members.csv',2019,'dataanalytics@per.gov.ie',1,2,8,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_132_RA_174_Year_2019_19092019housingfinanceagencydeferredmembers.csv' UNION ALL
SELECT 133,724,8,'copy_of_deferreds_datarequest_(005)~12.csv',2019,'dataanalytics@per.gov.ie',1,2,64,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_133_RA_8_Year_2019_copyofdeferredsdatarequest00512.csv' UNION ALL
SELECT 134,727,357,'23092019_psi_activeemployees~2.csv',2019,'dataanalytics@per.gov.ie',1,2,39,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_134_RA_357_Year_2019_23092019psiactiveemployees2.csv' UNION ALL
SELECT 135,729,357,'23092019_psi_deferred_members~1.csv',2019,'dataanalytics@per.gov.ie',1,2,21,1,GETDATE(),GETDATE(),'Deferreds',1,GETDATE(),'Version_135_RA_357_Year_2019_23092019psideferredmembers1.csv' UNION ALL
SELECT 136,730,367,'06082019_University_of_Limerick_Pensions_in_Payment_Updated.csv',2019,'dataanalytics@per.gov.ie',1,2,481,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_136_RA_367_Year_2019_06082019universityoflimerickpensionsinpaymentupdated.csv' UNION ALL
SELECT 137,731,367,'06082019_University_of_Limerick_ActiveEmployees_Updated_Password.csv',2019,'dataanalytics@per.gov.ie',1,2,1874,1,GETDATE(),GETDATE(),'Actives',1,GETDATE(),'Version_137_RA_367_Year_2019_06082019universityoflimerickactiveemployeesupdatedpassword.csv' UNION ALL
SELECT 138,735,94,'24092019_cork_county_council_pensions_in_payment~3.csv',2019,'dataanalytics@per.gov.ie',1,2,1408,1,GETDATE(),GETDATE(),'Payments',1,GETDATE(),'Version_138_RA_94_Year_2019_24092019corkcountycouncilpensionsinpayment3.csv' 

SET IDENTITY_INSERT dbo.FileHistory OFF

EXEC FileHistorySummaryCreate

SELECT * FROM FileHistorySummary where not FileName is null
SELECT * FROM FileHistory