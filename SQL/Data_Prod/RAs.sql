use AccruedLiability

delete From RelevantAuthority where RelevantAuthorityId >=3101

SET IDENTITY_INSERT dbo.RelevantAuthority ON



INSERT INTO [dbo].[RelevantAuthority]
           (RelevantAuthorityId,
		   [RelevantAuthorityName]
           ,[ModifyDate]
           ,[CreateDate]
           ,[ModifiedBy])
SELECT 500, 'Tourism Ireland', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 501,	'Waterways Ireland', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 502	,'Foras na Gaeilge', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 503	,'Intertrade Ireland', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 504	,'Foyle, Carlingford and Irish Lights Commission', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 505	,'Special European Union Programmes Body', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 506	,'Food Safety Promotion Board', GETDATE(), GETDATE(), 1 




SET IDENTITY_INSERT dbo.RelevantAuthority OFF

select * from [RelevantAuthority]