
USE [AccruedLiability]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileTransformHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileTransformHistory](
	[VersionId] [int]  NOT NULL,
	[FileTransformStatusId] [INT] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileTransformHistory_VersionId]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileTransformHistory]'))
ALTER TABLE [dbo].[FileTransformHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileTransformHistory_VersionId] FOREIGN KEY(VersionId)
REFERENCES [dbo].[FileHistory] (VersionId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileTransformHistory_FileTransformStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileTransformHistory]'))
ALTER TABLE [dbo].[FileTransformHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileTransformHistory_FileTransformStatus] FOREIGN KEY([FileTransformStatusId])
REFERENCES [dbo].FileTransformStatus (Id)
GO

SELECT * FROM [FileTransformHistory]