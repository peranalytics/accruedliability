USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS LoadDeferreds
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load deferreds to the table
-- =============================================
CREATE PROCEDURE LoadDeferreds
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE d
	FROM Deferreds d
	INNER JOIN StagingDeferreds sd
	ON d.RelevantAuthorityId = sd.RelevantAuthorityId
	AND d.YearId = sd.YearId


INSERT INTO [dbo].[Deferreds]
           ([VersionId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[GenderId]
           ,[CivilStatusId]
           ,[PensionSchemeId]
           ,[PreservedPensionAge]
           ,[PrsiClass]
           ,[FTE]
           ,[PensionableEmploymentStartDate]
           ,[PensionableEmploymentLeaveDate]
           ,[PensionableRemunerationFuturePensionBasedOn]
           ,[PensionableServiceFuturePensionBasedOn]
		   ,CreateDate
		   ,ModifyDate)
	SELECT [VersionId]
		  ,[RelevantAuthorityId]
		  ,[YearId]
		  ,[PPSN]
		  ,[DateOfBirth]
		  ,mg.Id
		  ,msc.Id
		  ,mps.Id
		  ,[PreservedPensionAge]
		  ,[PrsiClass]
		  ,[FTE]
		  ,[PensionableEmploymentStartDate]
		  ,[PensionableEmploymentLeaveDate]
		  ,[PensionableRemunerationFuturePensionBasedOn]
		  ,[PensionableServiceFuturePensionBasedOn]
			, GETDATE()
			,GETDATE()
  FROM [dbo].StagingDeferreds sd
  INNER JOIN MappingGender mg
  ON mg.MappingGender = sd.Gender
  INNER JOIN MappingCivilStatus msc
  ON msc.MappingCivilStatus = sd.CivilStatus
  INNER JOIN MappingPensionScheme mps
  ON mps.MappingPensionScheme = sd.PensionScheme


END
GO

GRANT EXECUTE ON LoadDeferreds TO AccruedLiabilityViewer 
