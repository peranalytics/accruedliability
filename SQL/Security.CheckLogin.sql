USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [Security].[CheckLogin]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Check Log in
-- =============================================
CREATE PROCEDURE [Security].[CheckLogin]
	@UserName VARCHAR(100)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		UserId, 
		PasswordHash
	FROM Security.Users u
	WHERE u.UserName = @UserName
	AND deleted = 0
	
END
GO

GRANT EXECUTE ON [Security].[CheckLogin] TO [budgetviewer] AS [dbo]
GO


