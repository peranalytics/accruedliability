USE [AccruedLiability]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [dbo].[MappingCivilStatus]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MappingCivilStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MappingCivilStatus](
	[Id] [int] NOT NULL,
	[MappingCivilStatus] [nvarchar](50) NOT NULL
)
END
GO

DELETE FROM [dbo].[MappingCivilStatus]
INSERT INTO [dbo].[MappingCivilStatus]
SELECT * FROM SPS..[CivilStatus]

SELECT * FROM [MappingCivilStatus]