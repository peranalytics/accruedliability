USE AccruedLiability


DECLARE @YearId INT = 2023
/* CHECK FOR YEAR - COMPLETE */

INSERT INTO [dbo].[PaymentsDeleted]
           ([VersionId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[GenderId]
           ,[CivilStatusId]
           ,[PensionSchemeId]
           ,[TypeOfBeneficiaryId]
           ,[PensionCommencementDate]
           ,[AnnualPensionValue]
           ,[AnnualSupplementaryPension]
           ,[BasisPensionCommencedId]
           ,[PensionableServiceYears]
           ,[ActualEmploymentBasedServiceYears]
           ,[PensionableServiceDifferenceReasons]
           ,[BasicPayValuePensionableRemuneration]
           ,[PensionableAllowancesPensionableRemuneration]
           ,[HowMuchAbatementIfAny]
           ,[CreateDate]
           ,[ModifyDate])

SELECT [VersionId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
      ,[DateOfBirth]
      ,[GenderId]
      ,[CivilStatusId]
      ,[PensionSchemeId]
      ,[TypeOfBeneficiaryId]
      ,[PensionCommencementDate]
      ,[AnnualPensionValue]
      ,[AnnualSupplementaryPension]
      ,[BasisPensionCommencedId]
      ,[PensionableServiceYears]
      ,[ActualEmploymentBasedServiceYears]
      ,[PensionableServiceDifferenceReasons]
      ,[BasicPayValuePensionableRemuneration]
      ,[PensionableAllowancesPensionableRemuneration]
      ,[HowMuchAbatementIfAny]
      ,[CreateDate]
      ,[ModifyDate]
FROM [dbo].[Payments] a
WHERE [PensionCommencementDate] > '31 dec 2021'
AND PPSN NOT in (SELECT sub.PPSN FROM PaymentsDeleted sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId])
AND YearId = 2023

DELETE FROM [dbo].[Payments] 
WHERE [PensionCommencementDate] > '31 dec 2021'
AND YearId = @YearId

--444