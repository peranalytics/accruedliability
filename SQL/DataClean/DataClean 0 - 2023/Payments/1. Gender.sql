USE AccruedLiability

--GENDER
DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023
/* CHECK FOR YEAR - COMPLETE */



INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Payments', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate, 'Gender', GenderId, NULL
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE GenderId = 2
AND a.YearId = @YearId


UPDATE a
SET [UpdatedValue] = CAST(VAlue AS INT)
FROM [CleaningLogTemporary] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE dcs.Scheme = 'Payments' AND dcs.Property = 'Gender'
AND [UpdatedValue] IS NULL
AND [OriginalValue] = 2


--UPDATING LIVE FILE
UPDATE a
SET a.GenderId = CAST(u.[UpdatedValue] AS INT)
FROM Payments a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND (a.DateOfBirth IS NULL OR a.DateOfBirth = u.DateOfBirth)
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.GenderId = CAST(u.[OriginalValue] AS INT)
AND a.YearId = u.YearId
AND a.YearId = @YearId



INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]




