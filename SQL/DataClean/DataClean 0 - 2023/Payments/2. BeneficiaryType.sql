USE AccruedLiability

--GENDER
DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023
/* CHECK FOR YEAR - COMPLETE */

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Payments', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate, 'TypeOfBeneficiary', TypeOfBeneficiaryId, NULL
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE TypeOfBeneficiaryId = 5
AND a.YearId = @YearId



UPDATE a
SET [UpdatedValue] = 1 -- MEMBER
FROM [CleaningLogTemporary] a
WHERE [UpdatedValue] IS NULL
AND DATEDIFF(YEAR, DateOfBirth, '31 dec 2021') >= 22

UPDATE a
SET [UpdatedValue] = 4 -- Children
FROM [CleaningLogTemporary] a
WHERE [UpdatedValue] IS NULL


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Payments', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate, 'TypeOfBeneficiary', TypeOfBeneficiaryId, NULL
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE TypeOfBeneficiaryId IN (1,2)
AND DATEDIFF(YEAR, DateOfBirth, '31 dec 2021') < 22
AND a.YearId = @YearId


UPDATE a
SET [UpdatedValue] = 4 -- Children
FROM [CleaningLogTemporary] a
WHERE [UpdatedValue] IS NULL
AND DATEDIFF(YEAR, DateOfBirth, '31 dec 2021') < 22

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Payments', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionCommencementDate, 'TypeOfBeneficiary', TypeOfBeneficiaryId, NULL
FROM Payments a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE TypeOfBeneficiaryId IN (4)
AND DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') >= 22
AND a.YearId = @YearId
--388

UPDATE a
SET [UpdatedValue] = 2 -- Spouse
FROM [CleaningLogTemporary] a
WHERE [UpdatedValue] IS NULL
AND DATEDIFF(YEAR, DateOfBirth, '31 dec 2018') >= 22


--UPDATING LIVE FILE
UPDATE a
SET a.TypeOfBeneficiaryId = CAST(u.[UpdatedValue] AS INT)
FROM Payments a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.TypeOfBeneficiaryId = CAST(u.[OriginalValue] AS INT)
AND a.YearId = u.YearId
AND a.YearId = @YearId
--2721


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]




