USE AccruedLiability
GO
--NRAs
DECLARE @YearId INT = 2023

INSERT INTO [dbo].[ActivesDeleted]
           ([VersionId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[GenderId]
           ,[CivilStatusId]
           ,[PensionSchemeId]
           ,[DateOfEntryIntoScheme]
           ,[MinimumNormalRetirementAge]
           ,[PrsiClass]
           ,[AnnualPensionablePayFTE]
           ,[LengthOfService]
           ,[FTE]
           ,[Grade]
           ,[ScalePoint]
           ,[IncrementDate]
           ,[CreateDate]
           ,[ModifyDate])

SELECT [VersionId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
      ,[DateOfBirth]
      ,[GenderId]
      ,[CivilStatusId]
      ,[PensionSchemeId]
      ,[DateOfEntryIntoScheme]
      ,[MinimumNormalRetirementAge]
      ,[PrsiClass]
      ,[AnnualPensionablePayFTE]
      ,[LengthOfService]
      ,[FTE]
      ,[Grade]
      ,[ScalePoint]
      ,[IncrementDate]
      ,[CreateDate]
      ,[ModifyDate]
  FROM [dbo].[Actives] a
WHERE [DateOfEntryIntoScheme] > '31 dec 2021'
AND YearId = @YearId
AND PPSN NOT in (SELECT sub.PPSN FROM ActivesDeleted sub WHERE sub.[RelevantAuthorityId] = a.[RelevantAuthorityId])

DELETE FROM [Actives]
WHERE [DateOfEntryIntoScheme] > '31 dec 2021'
AND YearId = @YearId


