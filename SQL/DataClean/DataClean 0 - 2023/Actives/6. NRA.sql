USE AccruedLiability


/* CHECK FOR YEAR - COMPLETE */


--NRAs
DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'MinimumNormalRetirementAge', MinimumNormalRetirementAge, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE (MinimumNormalRetirementAge IS NULL OR MinimumNormalRetirementAge = 0)
AND a.YearId = @YearId

UPDATE u
SET [UpdatedValue] = 60
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.YearId = u.YearId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (1,2)
AND [UpdatedValue] IS NULL
AND a.YearId = @YearId
--2


UPDATE u
SET [UpdatedValue] = 65
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.YearId = u.YearId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (3,5)
AND [UpdatedValue] IS NULL
AND a.YearId = @YearId

UPDATE u
SET [UpdatedValue] = 66
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.YearId = u.YearId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (4)
AND [UpdatedValue] IS NULL
AND a.YearId = @YearId
-- SPS


--UPDATING LIVE FILE
UPDATE a
SET a.MinimumNormalRetirementAge = CAST(u.[UpdatedValue] AS INT)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.MinimumNormalRetirementAge = CAST(u.[OriginalValue] AS INT)
AND a.YearId = u.YearId
WHERE a.YearId = @YearId
 

INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]



