USE AccruedLiability
GO
/* CHECK FOR YEAR - COMPLETE */

DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'DateOfEntryIntoScheme', DateOfEntryIntoScheme, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionSchemeId = 4 and DateOfEntryIntoScheme < '01/01/2013'
AND a.YearId = @YearId


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'LengthOfService', LengthOfService, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionSchemeId = 4 and DateOfEntryIntoScheme < '01/01/2013'
AND a.YearId = @YearId

UPDATE a
SET [UpdatedValue] = '01 jan 2013'
FROM [CleaningLogTemporary] a
WHERE [UpdatedValue] IS NULL
AND Property = 'DateOfEntryIntoScheme'

UPDATE a
SET [UpdatedValue] = '9'
FROM [CleaningLogTemporary] a
WHERE [UpdatedValue] IS NULL
AND Property = 'LengthOfService'

--UPDATING LIVE FILE
UPDATE a
SET a.LengthOfService = CAST(u.[UpdatedValue] AS DECIMAL(10,2))
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.LengthOfService = CAST(u.[OriginalValue] AS DECIMAL(10,2))
AND a.YearId = u.YearId
AND Property = 'LengthOfService'
WHERE a.YearId = @YearId

UPDATE a
SET a.DateOfEntryIntoScheme = CAST(u.[UpdatedValue] AS datetime)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.DateOfEntryIntoScheme = CAST(u.[OriginalValue] AS datetime)
AND a.YearId = u.YearId
AND Property = 'DateOfEntryIntoScheme'
WHERE a.YearId = @YearId


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]


