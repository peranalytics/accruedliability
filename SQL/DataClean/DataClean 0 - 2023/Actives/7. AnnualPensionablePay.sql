USE AccruedLiability

/* CHECK FOR YEAR - COMPLETE */

DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'AnnualPensionablePayFTE', AnnualPensionablePayFTE, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE ([AnnualPensionablePayFTE] IS NULL OR [AnnualPensionablePayFTE] <=0)
AND a.YearId = @YearId
--0 Empty Pay

UPDATE a
SET [UpdatedValue] = CAST(VAlue AS DECIMAL(10,2))
FROM [CleaningLogTemporary] a
INNER JOIN [DataCleaningValuesBySector] dcs
ON a.sectorId = dcs.SectorId
WHERE dcs.Scheme = 'Actives' AND dcs.Property = 'AnnualPensionablePayFTE'
AND [UpdatedValue] IS NULL

--UPDATING LIVE FILE
UPDATE a
SET a.AnnualPensionablePayFTE = CAST(u.[UpdatedValue] AS DECIMAL(10,2))
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.AnnualPensionablePayFTE = CAST(u.[OriginalValue] AS DECIMAL(10,2))
AND a.YearId = u.YearId
WHERE a.YearId = @YearId


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]

SELECT * FROM [CleaningLogFull]



