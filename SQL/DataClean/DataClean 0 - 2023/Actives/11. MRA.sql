USE AccruedLiability


/* CHECK FOR YEAR - COMPLETE */


--NRAs
DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'MandatoryRetirementAge', ISNULL(MandatoryRetirementAge,100), 100
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE (MandatoryRetirementAge IS NULL OR MandatoryRetirementAge = 0 OR MandatoryRetirementAge >=100)
AND a.YearId = @YearId


UPDATE u
SET [UpdatedValue] = 70
FROM [CleaningLogTemporary] u
INNER JOIN Actives a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.YearId = u.YearId
WHERE ([OriginalValue] IS NULL OR [OriginalValue] = '0')
AND [PensionSchemeId] IN (1,2,4)
AND [UpdatedValue] IS NULL
AND a.YearId = @YearId

--UPDATING LIVE FILE
UPDATE a
SET a.MandatoryRetirementAge = CAST(u.[UpdatedValue] AS INT)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND (a.MandatoryRetirementAge IS NULL OR a.MandatoryRetirementAge = CAST(u.[OriginalValue] AS INT))
AND a.YearId = u.YearId
WHERE a.YearId = @YearId
 

INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]



