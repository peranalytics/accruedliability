USE AccruedLiability
GO

/* CHECK FOR YEAR - COMPLETE */


DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Actives', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, [DateOfEntryIntoScheme], 'MinimumNormalRetirementAge', MinimumNormalRetirementAge, NULL
FROM Actives a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE DATEDIFF(YEAR, DateOfBirth, '31 dec 2021') > MinimumNormalRetirementAge
AND a.YearId = @YearId


UPDATE [CleaningLogTemporary]
SET [UpdatedValue] = DATEDIFF(YEAR, DateOfBirth, '31 dec 2022')
WHERE [UpdatedValue] IS NULL

--UPDATING LIVE FILE
UPDATE a
SET a.MinimumNormalRetirementAge = CAST(u.[UpdatedValue] AS INT)
FROM Actives a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.MinimumNormalRetirementAge = CAST(u.[OriginalValue] AS INT)
AND a.YearId = u.YearId
WHERE a.YearId = @YearId


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]




