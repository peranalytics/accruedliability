USE AccruedLiability

/* CHECK FOR YEAR - COMPLETE */


DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023


 INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Deferreds', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, 'DateofBirth', DateOfBirth, NULL
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionSchemeId IN (1,2)
AND DATEDIFF(Year, DateOfBirth, '31 dec 2021') >= 60
AND YearId = @YearId




UPDATE u
SET [UpdatedValue] = CAST(dc.value AS DATETIME)
FROM CleaningLogTemporary u
INNER JOIN Deferreds a
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionableEmploymentStartDate = u.DateOfEntryIntoScheme
AND a.YearId = u.YearId
INNER JOIN DataCleaningValuesBySector dc
ON  CAST(a.PensionSchemeId AS nvarchar(100)) = dc.Property
AND u.SectorId = dc.SectorId
WHERE dc.Scheme = 'Deferreds'
AND UpdatedValue IS NULL
AND a.YearId = @YearId


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Deferreds', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, 'DateofBirth', DateOfBirth, NULL
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionSchemeId IN (3)
AND DATEDIFF(Year, DateOfBirth, '31 dec 2021') >= 65
AND YearId = @YearId

--2124

UPDATE u
SET [UpdatedValue] = CAST(dc.value AS DATETIME)
FROM CleaningLogTemporary u
INNER JOIN Deferreds a
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionableEmploymentStartDate = u.DateOfEntryIntoScheme
AND a.YearId = u.YearId
INNER JOIN DataCleaningValuesBySector dc
ON  CAST(a.PensionSchemeId AS nvarchar(100)) = dc.Property
AND u.SectorId = dc.SectorId
WHERE dc.Scheme = 'Deferreds'
AND UpdatedValue IS NULL
AND a.YearId = @YearId

INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Deferreds', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, 'DateofBirth', DateOfBirth, NULL
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE PensionSchemeId IN (4)
AND DATEDIFF(Year, DateOfBirth, '31 dec 2021') >= 66
AND a.YearId = @YearId


UPDATE u
SET [UpdatedValue] = CAST(dc.value AS DATETIME)
FROM CleaningLogTemporary u
INNER JOIN Deferreds a
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionableEmploymentStartDate = u.DateOfEntryIntoScheme
AND a.YearId = u.YearId
INNER JOIN DataCleaningValuesBySector dc
ON  CAST(a.PensionSchemeId AS nvarchar(100)) = dc.Property
AND u.SectorId = dc.SectorId
WHERE dc.Scheme = 'Deferreds'
AND UpdatedValue IS NULL
AND a.YearId = @YearId

UPDATE CleaningLogTemporary 
SET [UpdatedValue] = '31 dec 2001'
WHERE DATEDIFF(Year, CAST([UpdatedValue] AS datetime), [DateOfEntryIntoScheme]) < 20



UPDATE a
SET a.DateOfBirth = CAST(u.[UpdatedValue] AS datetime)
FROM Deferreds a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionableEmploymentStartDate = u.DateOfEntryIntoScheme
AND a.DateOfBirth = CAST(u.[OriginalValue] AS datetime)
AND a.YearId = u.YearId
AND a.YearId = @YearId


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]
