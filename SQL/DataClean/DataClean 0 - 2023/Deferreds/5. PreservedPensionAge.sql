USE AccruedLiability

/* CHECK FOR YEAR - COMPLETE */

DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023


INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Deferreds', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, 'PreservedPensionAge', PreservedPensionAge, NULL
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE (PreservedPensionAge IS NULL OR PreservedPensionAge <= 0)
AND YearId = @YearId


UPDATE u
SET [UpdatedValue] = 60
FROM [CleaningLogTemporary] u
INNER JOIN Deferreds a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR CAST([OriginalValue] AS INT) <= 0)
AND [PensionSchemeId] IN (1,2)
AND [UpdatedValue] IS NULL
--229


UPDATE u
SET [UpdatedValue] = 65
FROM [CleaningLogTemporary] u
INNER JOIN Deferreds a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR CAST([OriginalValue] AS INT) <= 0)
AND [PensionSchemeId] IN (3,5)
AND [UpdatedValue] IS NULL
--0

UPDATE u
SET [UpdatedValue] = 66
FROM [CleaningLogTemporary] u
INNER JOIN Deferreds a 
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
WHERE ([OriginalValue] IS NULL OR CAST([OriginalValue] AS INT) <= 0)
AND [PensionSchemeId] IN (4)
AND [UpdatedValue] IS NULL
--0


-- UPDATING live
UPDATE a
SET a.PreservedPensionAge = CAST(u.[UpdatedValue] AS INT)
FROM Deferreds a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth = u.DateOfBirth
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PreservedPensionAge = CAST(u.[OriginalValue] AS INT)
AND a.YearId = u.YearId
AND a.YearId = @YearId


INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]





