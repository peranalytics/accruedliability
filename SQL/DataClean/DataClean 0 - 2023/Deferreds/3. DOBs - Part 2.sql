USE AccruedLiability

/* CHECK FOR YEAR - COMPLETE */


DELETE FROM [CleaningLogTemporary]
DECLARE @YearId INT = 2023


 INSERT INTO [dbo].[CleaningLogTemporary]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
		   ,[DateOfEntryIntoScheme]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT 'Deferreds', VersionId, SectorId, ras.RelevantAuthorityId, YearId, PPSN, DateOfBirth, PensionableEmploymentStartDate, 'DateofBirth', '01/01/1900', NULL
FROM Deferreds a
INNER JOIN RelevantAuthoritySectors ras
ON a.RelevantAuthorityId = ras.RelevantAuthorityId
WHERE DateOfBirth IS NULL
AND YearId = @YearId




UPDATE u
SET [UpdatedValue] = CAST(dc.value AS DATETIME)
FROM CleaningLogTemporary u
INNER JOIN Deferreds a
ON a.PPSN = u.PPSN
AND a.DateOfBirth IS NULL
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionableEmploymentStartDate = u.DateOfEntryIntoScheme
AND a.YearId = u.YearId
INNER JOIN DataCleaningValuesBySector dc
ON  CAST(a.PensionSchemeId AS nvarchar(100)) = dc.Property
AND u.SectorId = dc.SectorId
WHERE dc.Scheme = 'Deferreds'
AND UpdatedValue IS NULL
AND a.YearId = @YearId


UPDATE a
SET a.DateOfBirth = CAST(u.[UpdatedValue] AS datetime)
FROM Deferreds a
INNER JOIN [CleaningLogTemporary] u
ON a.PPSN = u.PPSN
AND a.DateOfBirth IS NULL
AND a.VersionId = u.VersionId
AND a.RelevantAuthorityId = u.RelevantAuthorityId
AND a.PensionableEmploymentStartDate = u.DateOfEntryIntoScheme
AND a.YearId = u.YearId
AND a.YearId = @YearId

INSERT INTO [dbo].[CleaningLogFull]
           ([Scheme]
           ,[VersionId]
           ,[SectorId]
           ,[RelevantAuthorityId]
           ,[YearId]
           ,[PPSN]
		   ,[DateOfBirth]
           ,[Property]
           ,[OriginalValue]
           ,[UpdatedValue])
SELECT [Scheme]
      ,[VersionId]
      ,[SectorId]
      ,[RelevantAuthorityId]
      ,[YearId]
      ,[PPSN]
	  ,[DateOfBirth]
      ,[Property]
      ,[OriginalValue]
      ,[UpdatedValue]
FROM [dbo].[CleaningLogTemporary]
