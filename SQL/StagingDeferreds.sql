USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [StagingDeferreds]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingDeferreds]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[StagingDeferreds](
	[VersionId] [int] NOT NULL,
	[RelevantAuthorityId] [int] NOT NULL,
	[YearId] [int] NOT NULL,
	[PPSN] VARCHAR(20) NOT NULL,
	[DateOfBirth] DATETIME  NULL,
	Gender NVARCHAR(100) NOT NULL,
	CivilStatus NVARCHAR(100)  NULL,
	PensionScheme NVARCHAR(250) NOT NULL,
	PreservedPensionAge INT NOT NULL,
	PrsiClass NVARCHAR(100) NOT NULL,
	FTE DECIMAL(10,2) NOT NULL,
	PensionableEmploymentStartDate DATETIME NOT NULL,
	PensionableEmploymentLeaveDate DATETIME NOT NULL,
	PensionableRemunerationFuturePensionBasedOn DECIMAL(10,2) NOT NULL,
	PensionableServiceFuturePensionBasedOn DECIMAL(10,2) NULL
	)
END
GO

