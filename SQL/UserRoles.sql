USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [Security].[UserRoles]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE[Security].[UserRoles](
		[UserId] [int] NOT NULL,
		[RoleId] INT NOT NULL,
		ModifyDate DATETIME NOT NULL,
CONSTRAINT [PK_UserRoles_SubsectoryId] PRIMARY KEY CLUSTERED 
(
	[UserId], [RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[Security].[UserRoles]'))
ALTER TABLE [Security].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [Security].[Roles] (RoleId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserRoles_UserId]') AND parent_object_id = OBJECT_ID(N'[Security].[UserRoles]'))
ALTER TABLE [Security].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_UserId] FOREIGN KEY([UserId])
REFERENCES [Security].[Users] (UserId)
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF__UserRoles_ModifyDate]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserRoles] ADD  DEFAULT ((GETDATE())) FOR ModifyDate
END

INSERT INTO [Security].UserRoles(UserId, RoleId, ModifyDate)
SELECT UserId, 2, GETDATE() FROM [Security].Users

UPDATE [Security].UserRoles
SET roleId = 1
WHERE UserId IN 
	(SELECT sub.UserId FROM Security.Users sub WHERE UserName IN 
		(
		'audrey@gmail.com',
		'audrey.pender@per.gov.ie',
		'tracy.mchugh@per.gov.ie',
		'niamh.byrne@per.gov.ie',
		'padraig.odwyer@per.gov.ie'
		)
	)
