USe AccruedLiability

ALTER TABLE StagingPayments DROP COLUMN IF EXISTS ActualEmploymentBasedServiceYears;
ALTER TABLE StagingPayments DROP COLUMN IF EXISTS PensionableServiceDifferenceReasons;
ALTER TABLE StagingPayments DROP COLUMN IF EXISTS HowMuchAbatementIfAny;