USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM CivilStatus
SELECT * FROM MappingGender
SELECT * FROM PensionScheme


SELECT * FROM BeneficiaryType
SELECT * FROM PensionBasis

IF NOT EXISTS (SELECT 1 FROM Gender WHERE Gender = 'Unknown')
BEGIN
	INSERT INTO Gender(Id, Gender)
	SELECT 2, 'Unknown'
END

IF NOT EXISTS (SELECT 1 FROM PensionScheme WHERE PensionScheme = 'Unknown')
BEGIN
	INSERT INTO PensionScheme(Id, PensionScheme)
	SELECT 6, 'Unknown'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'Unknown')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 6, 'Unknown'
END


IF NOT EXISTS (SELECT 1 FROM BeneficiaryType WHERE BeneficiaryType = 'Unknown')
BEGIN
	INSERT INTO BeneficiaryType(Id, BeneficiaryType)
	SELECT 5, 'Unknown'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'Unknown')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 5, 'Unknown'
END

IF NOT EXISTS (SELECT 1 FROM PensionBasis WHERE PensionBasis = 'Unknown')
BEGIN
	INSERT INTO PensionBasis(Id, PensionBasis)
	SELECT 5, 'Unknown'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'Unknown')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 5, 'Unknown'
END

IF NOT EXISTS (SELECT 1 FROM PensionBasis WHERE PensionBasis = 'S&C')
BEGIN
	INSERT INTO PensionBasis(Id, PensionBasis)
	SELECT 6, 'S&C'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'S&C')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'S&C'
END

IF NOT EXISTS (SELECT 1 FROM MappingGender WHERE MappingGender = 'm')
BEGIN
	INSERT INTO MappingGender(Id, MappingGender)
	SELECT 0, 'm'
END

IF NOT EXISTS (SELECT 1 FROM MappingGender WHERE MappingGender = 'f')
BEGIN
	INSERT INTO MappingGender(Id, MappingGender)
	SELECT 1, 'f'
END

IF NOT EXISTS (SELECT 1 FROM MappingGender WHERE MappingGender = 'Unknown')
BEGIN
	INSERT INTO MappingGender(Id, MappingGender)
	SELECT 2, 'Unknown'
END

SELECT * FROM MappingGender

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'co habit')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 11, 'co habit'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'cohabitating')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 11, 'cohabitating'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'widow')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 8, 'widow'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'widower')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 8, 'widower'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 's')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 0, 's'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'm')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 1, 'm'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'seperated')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 6, 'seperated'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'not available')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, 'not available'
END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = '')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 10, ''
END


IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'civil partnership')
BEGIN
	INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)
	SELECT 2, 'civil partnership'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'Single Scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'Single Scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single public service pension scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single public service pension scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'singlescheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'singlescheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'spsps')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'spsps'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new public sector pension 2013')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'new public sector pension 2013'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'non -established')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 5, 'non -established'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'non established')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 5, 'non established'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1995; non-established')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 5, 'pre 1995; non-established'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'superannuation')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 6, 'superannuation'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'none')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 6, 'none'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'lgss')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 6, 'lgss'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'qqi main supperannuation scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 6, 'qqi main supperannuation scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'north/south ps')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 6, 'north/south ps'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'superannuation scheme 1982 (no s&c)')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 1, 'superannuation scheme 1982 (no s&c)'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'superannuation scheme 1982 & s&c scheme 1986')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 1, 'superannuation scheme 1982 & s&c scheme 1986'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'model employee superannuation scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'model employee superannuation scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'model employee scheme funded')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'model employee scheme funded'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'co-ordinated scheme funded')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'co-ordinated scheme funded'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'co-ordinated scheme post 95')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 2, 'co-ordinated scheme post 95'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'widow')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 'widow'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'widower')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 'widower'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'spouse')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 'spouse'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'surviving civil partner')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 'surviving civil partner'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'child dep')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'child dep'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'son')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'son'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'daughter')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'daughter'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'child dependant')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'child dependant'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'student')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'student'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'dependant')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 4, 'dependant'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 's&c')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 's&c'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'survivor')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 2, 'survivor'
END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'pensioner')
BEGIN
	INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)
	SELECT 1, 'pensioner'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retirement/irs')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'early retirement/irs'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'iser')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 4, 'iser'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retirement/cner')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'early retirement/cner'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'cner')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'cner'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'cner - 94.8712%')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 3, 'cner - 94.8712%'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ill health retirement')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'ill health retirement'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ill-health')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'ill-health'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ill health')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'ill health'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'ill- health')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 2, 'ill- health'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '0')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 5, '0'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'death in service')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'death in service'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'spouse''s pension payable on death')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'spouse''s pension payable on death'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'death of member')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'death of member'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'widow')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'widow'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'child dep')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'child dep'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'spouse')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'spouse'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'widower')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'widower'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'child dependant')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'child dependant'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'pao')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 6, 'pao'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'voluntary'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'age limit')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'age limit'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'expiry of contract')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'expiry of contract'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'preserved benefit')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'preserved benefit'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'preserved benefits')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, 'preserved benefits'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = '2013-12-31')
BEGIN
	INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)
	SELECT 1, '2013-12-31'
END


--single public service pension scheme
--singlescheme
--non -established
select * from MappingPensionScheme
--spsps

--Non-Established