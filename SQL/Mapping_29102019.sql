uSE AccruedLiability

IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'unkown')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'unkown' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'age')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'age' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'early retirement scheme')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'early retirement scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'age + surviving spouse')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'age + surviving spouse' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'surviving spouse')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 6, 'surviving spouse' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'incentivised retirement')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 4, 'incentivised retirement' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'voluntary redundancy/early retirement')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'voluntary redundancy/early retirement' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'death following retirement')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'death following retirement' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'death following ill health retirement')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 2, 'death following ill health retirement' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'death following payment of preserved benefitst')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'death following payment of preserved benefitst' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'cost neutral')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 3, 'cost neutral' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'normal/ preserved')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 1, 'normal/ preserved' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionBasis WHERE MappingPensionBasis = 'none')BEGIN INSERT INTO MappingPensionBasis(Id, MappingPensionBasis)SELECT 5, 'none' END

IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'chaplain')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 0, 'chaplain' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'none specified')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 10, 'none specified' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'single with dependents')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 0, 'single with dependents' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'b')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 10, 'b' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'married/confirmed')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 1, 'married/confirmed' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'co-habitant ')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 11, 'co-habitant ' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'remarried')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 1, 'remarried' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'widow of member')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 8, 'widow of member' END
IF NOT EXISTS (SELECT 1 FROM MappingCivilStatus WHERE MappingCivilStatus = 'widower of member')BEGIN INSERT INTO MappingCivilStatus(Id, MappingCivilStatus)SELECT 8, 'widower of member' END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'estab new entrants post 2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'estab new entrants post 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'established new entrants post 2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'established new entrants post 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'f�s post-2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'f�s post-2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hse post 2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'hse post 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hse post-2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'hse post-2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'model scheme - post 2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'model scheme - post 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'unestab new entrants post 2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'unestab new entrants post 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'unestablished new entrants post 2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'unestablished new entrants post 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'established pre 1995')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'established pre 1995' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'established pre-95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'established pre-95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'f�s pre-95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'f�s pre-95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'former nrb pre 95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'former nrb pre 95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hse pre 95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'hse pre 95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hse pre-95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'hse pre-95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1995 - preserved')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'pre 1995 - preserved' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1995 -preserved member')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'pre 1995 -preserved member' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'established post 1995')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'established post 1995' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'established post-95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'established post-95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'f�s post-95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'f�s post-95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hse post 95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'hse post 95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'hse post-95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'hse post-95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 1995')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'post 1995' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post1995')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'post1995' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'prison officer post-95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'prison officer post-95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'unestablished pre 2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'unestablished pre 2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'unestablished pre-2004')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'unestablished pre-2004' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single pension scheme post 2013')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 4, 'single pension scheme post 2013' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'was in spsps')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 4, 'was in spsps' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'no scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'no scheme' END

IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'ill-health')BEGIN INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)SELECT 5, 'ill-health' END
IF NOT EXISTS (SELECT 1 FROM MappingBeneficiaryType WHERE MappingBeneficiaryType = 'member + spouse')BEGIN INSERT INTO MappingBeneficiaryType(Id, MappingBeneficiaryType)SELECT 1, 'member + spouse' END

