USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS [Security].LogFileDownload
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:Logs user download of file 
-- =============================================
CREATE PROCEDURE [Security].LogFileDownload
	@VersionId INT,
	@UserId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	INSERT INTO [Security].[FileLog] ([VersionId], [UserId], [CreateDate])
	SELECT @VersionId, @UserId, GETDATE()
	
	
END
GO

GRANT EXECUTE ON [Security].LogFileDownload TO AccruedLiabilityviewer 
