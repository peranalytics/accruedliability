USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [MappingGender]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MappingGender]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MappingGender](
	[Id] [int] NOT NULL,
	[MappingGender] [nvarchar](50) NOT NULL
 )
END
GO



DELETE FROM [dbo].MappingGender
INSERT INTO [dbo].MappingGender
SELECT * FROM SPS..Gender

SELECT * FROM MappingGender