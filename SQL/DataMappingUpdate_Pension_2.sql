USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM PensionScheme
SELECT * FROM MappingPensionScheme

IF NOT EXISTS (SELECT 1 FROM PensionScheme WHERE PensionScheme = 'Alpha')
BEGIN
	INSERT INTO PensionScheme(Id, PensionScheme)
	SELECT 7, 'Alpha'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'Alpha')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 7, 'Alpha'
END

IF NOT EXISTS (SELECT 1 FROM PensionScheme WHERE PensionScheme = 'Reserved Rights')
BEGIN
	INSERT INTO PensionScheme(Id, PensionScheme)
	SELECT 8, 'Reserved Rights'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'Reserved Rights')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 8, 'Reserved Rights'
END

IF NOT EXISTS (SELECT 1 FROM PensionScheme WHERE PensionScheme = 'Core')
BEGIN
	INSERT INTO PensionScheme(Id, PensionScheme)
	SELECT 9, 'Core'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'Core')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 9, 'Core'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'alpha')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 7, 'alpha' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'core alpha roi')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 7, 'core alpha roi' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'north south scheme - alpha member')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 7, 'north south scheme - alpha member' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'core final salary roi')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 9, 'core final salary roi' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'fspo model scheme - new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'fspo model scheme - new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full ptno 05')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'full ptno 05' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full pto ''05')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'full pto ''05' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'model')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'model' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 2005')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'post 2005' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 2013 scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'pre 2013 scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'solas pro rata intergration (post04) new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 3, 'solas pro rata intergration (post04) new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = '1985 non officer')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, '1985 non officer' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'a class fire officer (non new entrant)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'a class fire officer (non new entrant)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'a class non officer (non new entrant)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'a class non officer (non new entrant)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'a class officer (non new entrant)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'a class officer (non new entrant)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class d')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'class d' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class d (pre ''89)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'class d (pre ''89)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'class d 1990 technician')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'class d 1990 technician' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'd class fire officer')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'd class fire officer' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'd class officer')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'd class officer' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'd1 rate prsi - fas scheme (supfas)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'd1 rate prsi - fas scheme (supfas)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'fspo model scheme - non new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'fspo model scheme - non new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'fspo model scheme - non-new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'fspo model scheme - non-new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full no 85')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'full no 85' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full nosc84')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'full nosc84' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full nosc86')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'full nosc86' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full off 95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'full off 95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'mod main 69')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'mod main 69' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'mod main 77')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'mod main 77' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'mod masc 69')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'mod masc 69' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'mod sacs 84')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'mod sacs 84' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'mod sacs 86')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'mod sacs 86' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nhass pre 1995')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'nhass pre 1995' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'non- intergrated')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'non- intergrated' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'non officer w&o')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'non officer w&o' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1955')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'pre 1955' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1994')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'pre 1994' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1996')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'pre 1996' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'revision scheme officers (pre 95 class d prsi)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'revision scheme officers (pre 95 class d prsi)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'solas open scheme a class but pre april95')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 1, 'solas open scheme a class but pre april95' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'revision scheme officers (post 95 class a prsi)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'revision scheme officers (post 95 class a prsi)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'solas open scheme - pro rata intergration pre04')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 2, 'solas open scheme - pro rata intergration pre04' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'reserved rights')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 8, 'reserved rights' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'reserved rights established')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 8, 'reserved rights established' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'reserved rights non established')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 8, 'reserved rights non established' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'a class fire fighter non officer')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'a class fire fighter non officer' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'a class non officer (new entrant)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'a class non officer (new entrant)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'a class officer (new entrant)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'a class officer (new entrant)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'a class officer spouses & children')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'a class officer spouses & children' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'bord bia superannuation scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'bord bia superannuation scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'college of education scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'college of education scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'commission for energy regulation pension scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'commission for energy regulation pension scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'co-ordinated employees')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'co-ordinated employees' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'director scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'director scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'established scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'established scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'european assembly (irish representatives) pensions scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'european assembly (irish representatives) pensions scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'full' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full gr n/a')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'full gr n/a' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full no sacs')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'full no sacs' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'full oono 96')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'full oono 96' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'integration a class ee''s (new entrants pro rata)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'integration a class ee''s (new entrants pro rata)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'integration a class off (new entrant pro rata)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'integration a class off (new entrant pro rata)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'intergrated')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'intergrated' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'mod sacs')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'mod sacs' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'modified')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'modified' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new entrant a class employee pro-rata integration')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'new entrant a class employee pro-rata integration' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new entrant a class officer pro rata integration')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'new entrant a class officer pro rata integration' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nhass')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'nhass' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'non-intergrated')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'non-intergrated' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'north south pension scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'north south pension scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'north south pension scheme final salary')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'north south pension scheme final salary' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'north south pension scheme reserved rights')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'north south pension scheme reserved rights' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'north south scheme - final salary')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'north south scheme - final salary' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'officers a1')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'officers a1' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 2009')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'post 2009' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 2014')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'post 2014' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 2014 - superannuation scheme & spouses & children contributory.')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'post 2014 - superannuation scheme & spouses & children contributory.' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 2009')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pre 2009' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 2014')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pre 2014' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre-existing pension scheme (pre-2013)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pre-existing pension scheme (pre-2013)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pro rata integration a class employees')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pro rata integration a class employees' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pro rata integration a class employees new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pro rata integration a class employees new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pro rata integration a class officers')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pro rata integration a class officers' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pro rata integration class a officers new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pro rata integration class a officers new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pro-rata integration a class employees')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pro-rata integration a class employees' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pro-rata integration a class officers')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'pro-rata integration a class officers' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'protected final salary')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'protected final salary' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'revision scheme employees (class a prsi)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'revision scheme employees (class a prsi)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 's&c scheme')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 's&c scheme' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'solas closed scheme (former anco)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'solas closed scheme (former anco)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'solas open scheme - pro-rata integration e class')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'solas open scheme - pro-rata integration e class' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'solas pro-rata integration a class new entrant')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'solas pro-rata integration a class new entrant' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'the competition & consumer protection staff superannuation scheme 2016 (s.i. no. 593 of 2016)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'the competition & consumer protection staff superannuation scheme 2016 (s.i. no. 593 of 2016)' END
IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'the competition and consumer protection staff superannuation scheme 2016 (s.i. no. 593 of 2016)')BEGIN INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)SELECT 6, 'the competition and consumer protection staff superannuation scheme 2016 (s.i. no. 593 of 2016)' END


