USE AccruedLiability
GO

/****** Object:  StoredProcedure [dbo].[FileHistorySummaryGet]    Script Date: 01/07/2019 13:21:57 ******/
DROP PROCEDURE IF EXISTS [dbo].[FileHistorySummaryGet]
GO

/****** Object:  StoredProcedure [dbo].[FileHistorySummaryGet]    Script Date: 01/07/2019 13:21:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Summary for all member Status
-- =============================================
CREATE PROCEDURE [dbo].[FileHistorySummaryGet]
	@StartYear INT = 2019,
	@RelevantAuthorityId INT = -1
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @StartYear = YEAR(GETDATE()) - 1
	
	CREATE TABLE #YearList
	(
		YrId INT
	)

	;WITH yearlist AS
	(
		SELECT  @StartYear AS YrId
		UNION ALL
		SELECT yl.YrId + 1 AS YrId
		FROM yearlist yl
		WHERE yl.YrId + 1 <= YEAR(GetDate()) + 7
	)
	INSERT INTO #YearList(YrId)
	SELECT YrId FROM yearlist

	; WITH FileHistoryMd AS (
		SELECT mds.YrId, mds.RelevantAuthorityId, RelevantAuthorityName, Scheme, FileName, RecordCount, mds.ModifyDate, Success, UploadedBy, RANK() over (partition by mds.RelevantAuthorityId, mds.yrId, mds.scheme order by mds.modifydate desc) AS rankedHistory
		FROM FileHistorySummary mds WITH(NOLOCK)
		INNER JOIN #YearList yl
		ON yl.YrId = mds.YrId
		WHERE
		(Success = 1 OR Success IS NULL)
		AND (@RelevantAuthorityId = -1 OR mds.RelevantAuthorityId = @RelevantAuthorityId)
	)

	SELECT YrId, RelevantAuthorityId, RelevantAuthorityName, Scheme, FileName, RecordCount, ModifyDate, Success, UploadedBy
	FROM FileHistoryMd
	WHERE rankedHistory = 1
	ORDER BY RelevantAuthorityId, Scheme, YrId DESC

END

GO

GRANT EXECUTE ON [dbo].[FileHistorySummaryGet] TO [AccruedLiabilityviewer] AS [dbo]
GO


