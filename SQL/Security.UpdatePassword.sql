USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [Security].[UpdatePassword]
GO


-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Registers the user
-- =============================================
CREATE PROCEDURE [Security].[UpdatePassword] 
	@UserId NVARCHAR(128),
	@HashedPassword NVARCHAR(MAX),
	@SecurityStamp NVARCHAR(MAX)
AS

BEGIN
	SET NOCOUNT ON;

	UPDATE [Security].[Users]
	SET [PasswordHash] = @HashedPassword,
	SecurityStamp = @SecurityStamp
	WHERE UserId = @UserId
END
GO

GRANT EXECUTE ON [Security].[UpdatePassword] TO accruedliabilityViewer AS [dbo]
GO


