USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS FileHistoryMostRecent
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Summary for all member Status
-- =============================================
CREATE PROCEDURE FileHistoryMostRecent
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT TOP 10
      YrId
      ,RelevantAuthorityId AS RelevantAuthorityId
	  ,RelevantAuthorityName
      ,[FileName]
      ,[UploadedBy]
      ,[RecordCount]
      ,fhs.[ModifyDate]
      ,[Scheme]
      ,[Success]
	FROM FileHistorySummary fhs WITH(NOLOCK)
	WHERE Success = 1
	ORDER BY fhs.ModifyDate DESC, Scheme, YrId

END
GO

GRANT EXECUTE ON FileHistoryMostRecent TO AccruedLiabilityViewer 
