USE AccruedLiability

-- DATA MAPPING UPDATE

SELECT * FROM PensionScheme
SELECT * FROM MappingPensionScheme


IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new public sector pension *2013*')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'new public sector pension *2013*'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single pension scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single pension scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single public pension scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single public pension scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single public service scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single public service scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single public sector pension scheme 2013')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single public sector pension scheme 2013'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single public sector pension scheme 2013')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single public sector pension scheme 2013'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single pension')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single pension'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single public service pen scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single public service pen scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'sps')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'sps'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'spsps 2013')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'spsps 2013'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'Single')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'Single'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'single public service pension scheme (2013)')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'single public service pension scheme (2013)'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'sps pension scheme wef 2013')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'sps pension scheme wef 2013'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'ciste pinsean')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 4, 'ciste pinsean'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 2004 employee')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'post 2004 employee'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 31/3/04 new entrant')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'post 31/3/04 new entrant'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post 2004 officer')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'post 2004 officer'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'post2004')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'post2004'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new entrants employees post april 04')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'new entrants employees post april 04'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'new entrants officers post april 04')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'new entrants officers post april 04'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nta superannuation scheme 2017 (post 2004 new entrant)')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 3, 'nta superannuation scheme 2017 (post 2004 new entrant)'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1995 officer')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 1, 'pre 1995 officer'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre1995')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 1, 'pre1995'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nhass pre 1995')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 1, 'nhass pre 1995'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1995?')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 1, 'pre 1995?'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1995 officer non member s&c')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 1, 'pre 1995 officer non member s&c'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 1995 -pension scheme a class & spouses & children contributory.')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 1, 'pre 1995 -pension scheme a class & spouses & children contributory.'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 2004 employee')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 2, 'pre 2004 employee'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 2004 officer')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 2, 'pre 2004 officer'
END


IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre2004')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 2, 'pre2004'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nhass pre 2004')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 2, 'nhass pre 2004'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre 2004 - pension scheme a class & spouses & children')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 2, 'pre 2004 - pension scheme a class & spouses & children'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'nta superannuation scheme 2017 (pre 2004 non-new entrant)')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 2, 'nta superannuation scheme 2017 (pre 2004 non-new entrant)'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'pre-04')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 2, 'pre-04'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'unestablished scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 5, 'unestablished scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'non-established scheme')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 5, 'non-established scheme'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'reserved rights non-established')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 5, 'reserved rights non-established'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'rr non established')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 5, 'rr non established'
END

IF NOT EXISTS (SELECT 1 FROM MappingPensionScheme WHERE MappingPensionScheme = 'not known')
BEGIN
	INSERT INTO MappingPensionScheme(Id, MappingPensionScheme)
	SELECT 6, 'not known'
END




