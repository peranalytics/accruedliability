USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [Report].[Reports]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reports]') AND type in (N'U'))
BEGIN
CREATE TABLE [Report].[Reports](
		[ReportId] [int] IDENTITY(1,1) NOT NULL,
		ReportName NVARCHAR(250) NOT NULL,
		ReportDescription NVARCHAR(MAX) NULL,
		ReportUrl NVARCHAR(250) NOT NULL,
		Deleted BIT NOT NULL,
		ModifyDate DATETIME NOT NULL,
CONSTRAINT [PK_Reports_RoleId] PRIMARY KEY CLUSTERED 
(
	[ReportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF__Reports_ModifyDate]') AND type = 'D')
BEGIN
ALTER TABLE [Report].[Reports] ADD  DEFAULT ((GETDATE())) FOR ModifyDate
END

INSERT INTO [Report].Reports(ReportName, ReportDescription, ReportUrl, Deleted, ModifyDate)
SELECT 'Active Members', 'All active members grouped by sector', '/AccruedLiability/Actives', 1, GETDATE() UNION ALL
SELECT 'Deferreds Pensions', 'All deferred members grouped by sector', '/AccruedLiability/Deferreds', 1, GETDATE() UNION ALL
SELECT 'Pensions in Payment', 'All members currently receiving a pension payment', '/AccruedLiability/Payments', 1, GETDATE()

SELECT * FROM [Report].Reports

