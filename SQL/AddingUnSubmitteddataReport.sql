--use AccruedLiability

--select * from Report.Reports

--INSERT INTO Report.Reports(ReportId, ReportDescription,ReportUrl,Deleted,ModifyDate),
--SELECT 4;


USE [AccruedLiability]
GO

INSERT INTO [Report].[Reports]
           ([ReportName]
           ,[ReportDescription]
           ,[ReportUrl]
           ,[Deleted]
           ,[ModifyDate])
     SELECT 
		 'UnSubmitted Data',
		 'Returns bodies who have not Submitted data for each File Type',
		 '/AccruedLiability/BodiesWithUnsubmittedData',
		 0,
		 GETDATE()

GO


INSERT INTO Security.UserReports
SELECT UserId, 4, GETDATE()
FROM Security.Users

