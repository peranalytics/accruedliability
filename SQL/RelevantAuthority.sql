USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE RelevantAuthority

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelevantAuthority]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[RelevantAuthority](
	[RelevantAuthorityId] [int] IDENTITY(1,1) NOT NULL,
	[RelevantAuthorityName] [nvarchar](255) NOT NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_RelevantAuthority_Id] PRIMARY KEY CLUSTERED 
(
	[RelevantAuthorityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RelevantAuthority_ModifiedBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[RelevantAuthority]'))
ALTER TABLE [dbo].[RelevantAuthority]  WITH CHECK ADD  CONSTRAINT [FK_RelevantAuthority_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [Security].[Users] (UserId)
GO


