USE [AccruedLiability]
GO

DROP TABLE IF EXISTS [Security].[ReportUsage]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ReportUsage]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[ReportUsage](
	[UsageId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ReportId] [int] NOT NULL,
	[ViewDate] [datetime] NULL,
 CONSTRAINT [PK_ReportUsage_Id] PRIMARY KEY CLUSTERED 
(
	[UsageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DF_ReportUsage_ViewDate]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[ReportUsage] ADD  DEFAULT (getdate()) FOR [ViewDate]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_ReportUsage_Report]') AND parent_object_id = OBJECT_ID(N'[Security].[ReportUsage]'))
ALTER TABLE [Security].[ReportUsage]  WITH CHECK ADD  CONSTRAINT [FK_ReportUsage_Report] FOREIGN KEY([ReportId])
REFERENCES [Report].[Reports] ([ReportId])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_ReportUsage_Report]') AND parent_object_id = OBJECT_ID(N'[Security].[ReportUsage]'))
ALTER TABLE [Security].[ReportUsage] CHECK CONSTRAINT [FK_ReportUsage_Report]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_ReportUsage_User]') AND parent_object_id = OBJECT_ID(N'[Security].[ReportUsage]'))
ALTER TABLE [Security].[ReportUsage]  WITH CHECK ADD  CONSTRAINT [FK_ReportUsage_User] FOREIGN KEY([UserId])
REFERENCES [Security].[Users] ([UserId])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_ReportUsage_User]') AND parent_object_id = OBJECT_ID(N'[Security].[ReportUsage]'))
ALTER TABLE [Security].[ReportUsage] CHECK CONSTRAINT [FK_ReportUsage_User]
GO


