USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [ActivesFlatFile]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivesFlatFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivesFlatFile](
	[RelevantAuthorityId] [int] NOT NULL,
	[RelevantAuthorityName] [nvarchar](255) NOT NULL,
	[SectorId] [int] NOT NULL,
	SectorName NVARCHAR(250) NOT NULL,
	[SubSectorId] [int] NOT NULL,
	SubSectorName NVARCHAR(250) NOT NULL,
	[YearId] [int] NOT NULL,
	[PPSN] [varchar](20) NOT NULL,
	[DateOfBirth] [datetime] NOT NULL,
	[Gender] [nvarchar](50) NOT NULL,
	[CivilStatus] [nvarchar](50) NOT NULL,
	[PensionScheme] [nvarchar](50) NOT NULL,
	[DateOfEntryIntoScheme] [datetime] NOT NULL,
	[MinimumNormalRetirementAge] [int] NOT NULL,
	[PrsiClass] [nvarchar](100) NOT NULL,
	[LengthOfService] [decimal](10, 2) NOT NULL,
	[FTE] [decimal](10, 2) NOT NULL,
	[Grade] [nvarchar](250) NOT NULL,
	[ScalePoint] [nvarchar](250) NOT NULL
) ON [PRIMARY]
END
GO

