USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BeneficiaryType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BeneficiaryType](
	[Id] [int] NOT NULL,
	[BeneficiaryType] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_BeneficiaryType_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

DELETE FROM [dbo].[BeneficiaryType]
INSERT INTO [dbo].[BeneficiaryType]
SELECT 1, 'Member' UNION ALL
SELECT 2, 'Spouse-Legal Partner ' UNION ALL
SELECT 3, 'PAO ' UNION ALL
SELECT 4, 'Children'

SELECT * FROM BeneficiaryType


