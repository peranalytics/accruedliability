USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS LoadPayments
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Load Payments to the table
-- =============================================
CREATE PROCEDURE LoadPayments
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE p
	FROM Payments p
	INNER JOIN StagingPayments sp
	ON p.RelevantAuthorityId = sp.RelevantAuthorityId
	AND p.YearId = sp.YearId

	INSERT INTO [dbo].[Payments]
			   ([VersionId]
			   ,[RelevantAuthorityId]
			   ,[YearId]
			   ,[PPSN]
			   ,[DateOfBirth]
			   ,[GenderId]
			   ,[CivilStatusId]
			   ,[PensionSchemeId]
			   ,[TypeOfBeneficiaryId]
			   ,[PensionCommencementDate]
			   ,[AnnualPensionValue]
			   ,[AnnualSupplementaryPension]
			   ,[BasisPensionCommencedId]
			   ,[PensionableServiceYears]
			   ,[ActualEmploymentBasedServiceYears]
			   ,[PensionableServiceDifferenceReasons]
			   ,[BasicPayValuePensionableRemuneration]
			   ,[PensionableAllowancesPensionableRemuneration]
			   ,[HowMuchAbatementIfAny]
			   ,[CreateDate]
			   ,[ModifyDate])
	SELECT [VersionId]
		  ,[RelevantAuthorityId]
		  ,[YearId]
		  ,[PPSN]
		  ,[DateOfBirth]
		  ,mg.Id
		  ,msc.Id
		  ,mps.Id
		  ,mbt.Id
		  ,[PensionCommencementDate]
		  ,[AnnualPensionValue]
		  ,[AnnualSupplementaryPension]
		  ,mpb.Id
		  ,[PensionableServiceYears]
		  ,[ActualEmploymentBasedServiceYears]
		  ,[PensionableServiceDifferenceReasons]
		  ,[BasicPayValuePensionableRemuneration]
		  ,[PensionableAllowancesPensionableRemuneration]
		  ,[HowMuchAbatementIfAny]
		  ,GETDATE()
		  ,GETDATE()
	  FROM [dbo].StagingPayments sp
	  INNER JOIN MappingGender mg
	  ON mg.MappingGender = sp.Gender
	  INNER JOIN MappingCivilStatus msc
	  ON msc.MappingCivilStatus = sp.CivilStatus
	  INNER JOIN MappingPensionScheme mps
	  ON mps.MappingPensionScheme = sp.PensionScheme
	  INNER JOIN MappingBeneficiaryType mbt
	  ON mbt.MappingBeneficiaryType = sp.TypeOfBeneficiary
	  INNER JOIN MappingPensionBasis mpb
	  ON mpb.MappingPensionBasis = sp.BasisPensionCommenced


END
GO

GRANT EXECUTE ON LoadPayments TO AccruedLiabilityViewer 
