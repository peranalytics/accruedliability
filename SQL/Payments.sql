USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [Payments]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Payments]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[Payments](
		[VersionId] [int] NOT NULL,
		[RelevantAuthorityId] [int] NOT NULL,
		[YearId] [int] NOT NULL,
		[PPSN] VARCHAR(20) NOT NULL,
		[DateOfBirth] DATETIME NULL,
		GenderId INT NOT NULL,
		CivilStatusId INT  NULL,
		PensionSchemeId INT NOT NULL,
		TypeOfBeneficiaryId INT NOT NULL,
		PensionCommencementDate DATETIME NOT NULL,
		AnnualPensionValue DECIMAL(10,2) NOT NULL,
		AnnualSupplementaryPension DECIMAL(10,2) NOT NULL,
		BasisPensionCommencedId INT NOT NULL,
		PensionableServiceYears DECIMAL(10,2) NULL,
		ActualEmploymentBasedServiceYears DECIMAL(10,2) NULL,
		PensionableServiceDifferenceReasons NVARCHAR(250) NULL,
		BasicPayValuePensionableRemuneration DECIMAL(10,2) NULL,
		PensionableAllowancesPensionableRemuneration DECIMAL(10,2) NULL,
		HowMuchAbatementIfAny NVARCHAR(250) NULL,
		CreateDate DATETIME DEFAULT GETDATE() NOT NULL,
		ModifyDate DATETIME DEFAULT GETDATE() NOT NULL
	)
END
GO

