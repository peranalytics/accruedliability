USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MappingPensionBasis]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MappingPensionBasis](
	[Id] [int] NOT NULL,
	[MappingPensionBasis] [nvarchar](50) NOT NULL
)
END
GO

DELETE FROM [dbo].MappingPensionBasis
INSERT INTO [dbo].MappingPensionBasis
SELECT * FROM PensionBasis


SELECT * FROM MappingPensionBasis