USE AccruedLiability 
GO

DROP PROCEDURE IF EXISTS FileHistoryByRelevantAuthority
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Summary for all member Status
-- =============================================
CREATE PROCEDURE FileHistoryByRelevantAuthority
	@RelevantAuthorityId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
	   [VersionId]
      ,[Year]
      ,RA.RelevantAuthorityId AS RelevantAuthorityId
	  ,RelevantAuthorityName
      ,[FileName]
      ,[UploadedBy]
      ,[FileTypeId]
      ,[RecordCount]
      ,md.[ModifyDate]
      ,md.[CreateDate]
      ,[Scheme]
      ,[Success]
      ,[EntryUpdateDate]
      ,[LocalFileName]
	FROM FileHistory md WITH(NOLOCK)
	INNER JOIN RelevantAuthority ra
	ON ra.RelevantAuthorityId = md.RelevantAUthorityId
	WHERE ra.RelevantAuthorityId = @RelevantAuthorityId
	AND Success = 1
	ORDER BY Scheme, Year

END
GO

GRANT EXECUTE ON FileHistoryByRelevantAuthority TO AccruedLiabilityViewer 
