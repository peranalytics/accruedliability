
USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[BenefitTypeGetAll] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 25/07/2019
-- Description:	Get the mapping lookup data
-- =============================================
CREATE PROCEDURE [dbo].[BenefitTypeGetAll] 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, MappingBenefitType FROM [dbo].MappingBenefitType
END
GO

GRANT EXECUTE ON [dbo].[BenefitTypeGetAll] TO budgetviewer AS [dbo]
GO


