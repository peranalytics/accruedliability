/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [AccruedLiability]
GO

DROP PROCEDURE IF EXISTS [dbo].[PopulateActivesFlatFiles]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PopulateActivesFlatFiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PopulateActivesFlatFiles] AS' 
END
GO

-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Populate flat files
-- =============================================
ALTER PROCEDURE [dbo].[PopulateActivesFlatFiles]
AS
BEGIN
	
	SET NOCOUNT ON;

	DELETE FROM [ActivesFlatFile]

	INSERT INTO [dbo].[ActivesFlatFile]
           ([RelevantAuthorityId]
           ,[RelevantAuthorityName]
		   ,[SectorId]
		   ,SectorName
		   ,[SubSectorId]
		   ,SubSectorName
           ,[YearId]
           ,[PPSN]
           ,[DateOfBirth]
           ,[Gender]
           ,[CivilStatus]
           ,[PensionScheme]
           ,[DateOfEntryIntoScheme]
           ,[MinimumNormalRetirementAge]
           ,[PrsiClass]
           ,[LengthOfService]
           ,[FTE]
           ,[Grade]
           ,[ScalePoint]
		   ,[AnnualPensionablePayFTE]
		   ,IncrementDate
		   ,MandatoryRetirementAge
		   ,AnnualBasicPensionablePayFTE)
    SELECT 
		a.RelevantAuthorityId,
		ra.RelevantAuthorityName,
		secs.SectorId,
		secs.SectorName,
		subsecs.SubsectorId,
		subsecs.SubsectorName,
		a.YearId,
		PPSN,
		DateOfBirth,
		Gender,
		CivilStatus,
		PensionScheme,
		DateOfEntryIntoScheme,
		MinimumNormalRetirementAge,
		PrsiClass,
		LengthOfService,
		FTE,
		Grade,
		ScalePoint,
		AnnualPensionablePayFTE,
		IncrementDate
		,MandatoryRetirementAge
		,AnnualBasicPensionablePayFTE
	FROM Actives a  WITH(NOLOCK)
	INNER JOIN RelevantAuthoritySectors ras WITH(NOLOCK)
	ON ras.RelevantAuthorityId = a.RelevantAuthorityId
	INNER JOIN RelevantAuthority ra WITH(NOLOCK)
	ON ra.RelevantAuthorityId = a.RelevantAuthorityId
	INNER JOIN Sectors secs WITH(NOLOCK)
	ON secs.SectorId = ras.SectorId
	INNER JOIN Subsectors subsecs WITH(NOLOCK)
	ON subsecs.SubsectorId = ras.SubsectorId
	AND subsecs.SectorId = ras.SectorId
	INNER JOIN Gender g WITH(NOLOCK) 
	ON a.GenderId = g.Id
	INNER JOIN CivilStatus cs WITH(NOLOCK) 
	ON a.CivilStatusId = cs.Id
	INNER JOIN PensionScheme p WITH(NOLOCK) 
	ON a.PensionSchemeId = p.Id

END
GO

GRANT EXECUTE ON [dbo].[PopulateActivesFlatFiles] TO [accruedLiabilityviewer] AS [dbo]
GO


