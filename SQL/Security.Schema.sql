USE AccruedLiability

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Security')
BEGIN
	EXEC('CREATE SCHEMA Security')
END