
USE AccruedLiability
GO

DROP PROCEDURE IF EXISTS [dbo].[PensionSchemeGetAll] 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 25/07/2019
-- Description:	Get the mapping lookup data
-- =============================================
CREATE PROCEDURE [dbo].[PensionSchemeGetAll] 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, MappingPensionScheme FROM [dbo].MappingPensionScheme
END
GO

GRANT EXECUTE ON [dbo].[PensionSchemeGetAll] TO budgetviewer AS [dbo]
GO


