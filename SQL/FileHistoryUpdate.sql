/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE AccruedLiability
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE IF EXISTS FileHistoryUpdate
GO

-- =============================================
-- Author:		Audrey
-- Create date: 06/02/2018
-- Description:	Add the meta data as successk
-- =============================================
CREATE PROCEDURE [dbo].[FileHistoryUpdate] 
		@VersionId INT,
		@LocalFileName NVARCHAR(255),
		@Success BIT
AS
BEGIN
	SET NOCOUNT ON;


	UPDATE FileHistory
	SET Success = @Success,
	LocalFileName = @LocalFileName
	WHERE VersionId = @VersionId

END
GO

GRANT EXECUTE ON [dbo].[FileHistoryUpdate] TO budgetviewer AS [dbo]
GO


