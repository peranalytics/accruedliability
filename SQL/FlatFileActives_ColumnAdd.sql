USE AccruedLiability

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnualPensionablePayFTE'AND Object_ID = Object_ID(N'ActivesFlatFile'))
BEGIN
Print 'Added'
    ALTER TABLE ActivesFlatFile ADD AnnualPensionablePayFTE DECIMAL(10,2)
END

GO

USE AccruedLiability

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IncrementDate'AND Object_ID = Object_ID(N'ActivesFlatFile'))
BEGIN
Print 'Added'
    ALTER TABLE ActivesFlatFile ADD IncrementDate DATETIME
END

GO

SELECT * FROM ActivesFlatFile

--3859